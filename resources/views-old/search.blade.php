@extends('master')

@section('content')
	<div class="row">
            <form method="post" action="search">
		<div class="col-md-3 col-sm-12 col-xs-12">
		<div class="inner-search">
                    <input type="text" name="doctor" value="{{ $request->doctor }}" id="doctor" placeholder="Doctor" onblur="checkDoctorId()">
                <input type="hidden" name="doctor_id" value="{{ $request->doctor_id }}" id="doctor_id" placeholder="Doctor">
		<a data-toggle="collapse" data-target="#filters" aria-expanded="false" aria-controls="filters" style="float: right;">View filters</a>

		<div style="clear: both;"></div>

	<div id="filters" class="collapse">
            <form method="post" action="search"></form>
                <input type="text" name="hospital" id="hospital" value="{{ $request->hospital }}" placeholder="Any Hospital" onblur="checkHospitalId()">
                <input type="hidden" name="hospital_id" id="hospital_id" value="{{ $request->hospital_id }}" placeholder="Any Hospital">
                <input type="text" name="specialization" id="spec" value="{{ $request->specialization }}" placeholder="Any Specialization" onblur="checkSpecializationId()">
                <input type="hidden" name="specialization_id" id="spec_id" value="{{ $request->specialization_id }}" placeholder="Any Specialization">
                <input type='text' name="date" id='date' value="{{ $request->date }}" placeholder="Any Date" style="bottom: 0;" />
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
	</div>

		<button type="submit">Search</button>
          </form>      
		<script type="text/javascript">
			$(document).ready(function() {

  
$('[data-toggle="collapse"]').click(function() {
  $(this).toggleClass( "active" );
  if ($(this).hasClass("active")) {
    $(this).text("Hide filters");
  } else {
    $(this).text("View filters");
  }
});
  
  
// document ready  
});

		</script>
    <script type="text/javascript">
      $(document).ready (function(){
            $(function () {
                $('#date').datetimepicker(
                  {
                    format:'DD/MM/YYYY'
                  });
                //$("#date").val("code");
            });
          });
        </script>                
	</div>	
		</div>
		<div class="col-md-9 col-sm-12 col-xs-12">
		<div class="heading">Select Doctor</div>
                <?php foreach($hospitals as $hospital){?>
                <?php
                $hospital_available=false;
                foreach ($data as $d) {
                        if (isset($d['hospitals'][$hospital['value']])) {
                            $hospital_available = true;
                            break;
                        }
                    }
                    ?> 
                <?php if($hospital_available){?>
		<h3><?php echo $hospital['label'];?></h3>
                <?php } ?>
                <?php foreach($data as $d){ ?>
                <?php if(isset($d['hospitals'][$hospital['value']])){?>
		<div class="col-md-6 col-sm-6 col-xs-12 doctors-block">
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
			<td width="18%"><img src="<?php echo $d['doc_image'];?>" width="100%" alt="<?php echo $d['doctor_name'];?>"></td>	
			<td>
			<strong><?php echo $d['doctor_name'];?></strong>
			<span><?php echo $d['specialization'];?></span> 	
			</td>
			<td width="20%">
			<a href="{{ url('/sessions/'.$d['doctor_code'].'/'.$hospital['value'].'/'.$d['specialization_id'].'/'.serialize($d['hospitals'][$hospital['value']]['source']).'/'.$d['hospitals'][$hospital['value']]['direct'].'') }}"><button>Channel</button></a>
			</td>
			</tr>	
		</table>	
		</div>
                <?php } ?>
                <?php } ?>
                <div style="clear: both"></div>
                <?php } ?>
		
                <?php if(empty($data)){?>
                No record(s) found.
                <?php } ?>
		</div>
	</div>	
@stop