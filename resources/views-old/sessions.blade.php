@extends('master')

@section('content')
	<div class="row">
            <?php if(!empty($data)){?>
            <?php foreach($data as $d){ ?>
		<div class="col-md-4 col-sm-12 col-xs-12">
		<div class="doctor-profile-block">
			<img src="<?php echo $d['doc_image'];?>" width="" alt="placeholder+image">
			<h4><?php echo $d['doctor_name'];?></h4>
			<span><?php echo $d['specialization_name'];?></span>
                        <?php if(!empty($d['special_notes'])){?>
			<span style="margin-top: 20px;">Special Notes: </span>
			<?php echo $d['special_notes'];?>
                        <?php } ?>
		</div>	
		</div>
            
		<div class="col-md-8 col-sm-12 col-xs-12">
		<div class="heading"><?php echo $d['hospital_name'];?></div>
                <?php foreach($d['sessions'] as $key=>$sessions){?>
                <?php
//                print_r($sessions);
                $class='';
                $link='#';
                $price='';
                if($sessions['status']=='Full'){
                    $class='session-full';
                }elseif($sessions['status']=='Holiday'){
                    $class='session-full';
                }elseif($sessions['status']=='Not Available'){
                    $class='session-full';
                }elseif($sessions['status']=='Available'){
                    $link=url('/booking/'.urlencode($key));
                    $price='LKR '.number_format($sessions['booking_info'][0]['amount_local'],2);
                }
                ?>
		<div class="session-block <?php echo $class;?>">
		<table width="100%" border="0" cellspacing="5" cellpadding="5">
		<tr>
		<td width="25%"><?php echo date('F d, Y',strtotime($sessions['show_date'].' '.$sessions['time'].':00')); ?><span><?php echo $sessions['day'];?> <?php echo date('h:i A',strtotime($sessions['show_date'].' '.$sessions['time'].':00')); ?></span></td>
		<td width="35%" style="text-align: center;">Active Appointments<label><?php echo $sessions['next_appointment_no'];?></label></td>
		<td width="25%" style="text-align: center;"><?php echo $sessions['status'];?></td>
		<td width="15%" style="text-align:center;">
                    <div class="m-price"><?php echo $price;?></div> <a href="<?php echo $link;?>"><button>Book</button></a>
		</td>	
		</tr>	
		</table>	
		</div>
                <?php } ?>

		</div>
            <?php } ?>
            <?php }else {?>
            <div class="col-md-4 col-sm-12 col-xs-12">
               No session(s) found.
            </div>   
            <?php } ?>
	</div>		
@stop