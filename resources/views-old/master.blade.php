<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>{{$title}}</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{ asset('css/channlling_style.css') }}"> 
<script defer src="https://use.fontawesome.com/releases/v5.0.9/js/all.js" integrity="sha384-8iPTk2s/jMVj81dnzb/iFR2sdA7u06vHJyyLlAd4snFpCl/SnyUjRrbdJsw1pGIl"
    crossorigin="anonymous"></script> 

    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css"> -->

<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css"> -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css" />

    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>
  
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	
  
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

</head>
<body>

<div class="container-fluid">

<!-- header -->
<div class="row bottom-border header">
            <div class="col-md-6 col-sm-6 col-xs-4 hemas-logo">
                <a href='{{ url('/index') }}'><img src="{{ asset('images/hemas-logo.png') }}" alt="placeholder+image"></a><!-- <br/> -->
                <!-- <h4 style="color: #e4e4e4;">Hemas Hospitals Lab Portal</h4>  -->
            </div>  
            <div class="col-md-6 col-sm-6 col-xs-8 right-header">
            <div>
            <div class="ayubolife"><img src="{{ asset('images/ayubolife.png') }}" width="80%" height="" alt="ayubo life"></div>
            </div>
<!--            <a href="" style="color:#fff; background: transparent;" class="btn  btn-md" title="Logout"> Logout 
             <table width="100%" border="0">
            <tr>
             <td><i class="fas fa-sign-out-alt"></i></td>
             <td> <span>Logout</span></td>
            </tr>
            </table>
            </a>-->

      </div>
    </div>
<!-- end header -->


	<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12 inner-page-slider">
		<h2>Channel Your Doctor</h2>
	</div>
	</div>
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
    @yield('content')
</div>	

</body>
</html>
 <script>
     var doctors;
     var hospitals;
     var specilizations;
     
  $( function() {
     doctors = <?php print_r(json_encode($doctors)) ?>;
     hospitals = <?php print_r(json_encode($hospitals)) ?>;
     specilizations = <?php print_r(json_encode($specializations)) ?>;
    
     $("#doctor").autocomplete({  
         minLength: 3,
        source: doctors,
        select: function( event, ui ) {

            $( "#doctor_id" ).val(ui.item.value);
            $( "#doctor" ).val( ui.item.label);
            return false;
        },
       
    });
    
     $("#hospital").autocomplete({  
         minLength: 3,
        source: hospitals,
        select: function( event, ui ) {

            $( "#hospital_id" ).val(ui.item.value);
            $( "#hospital" ).val( ui.item.label);
            return false;
        },
       
    });
    
     $("#spec").autocomplete({  
         minLength: 3,
        source: specilizations,
        select: function( event, ui ) {

            $( "#spec_id" ).val(ui.item.value);
            $( "#spec" ).val( ui.item.label);
            return false;
        },
       
    });  
    
          checkHospitalId();
          checkDoctorId();
          checkSpecializationId();    

  });
  
            function checkDoctorId(){
            for(i=0;i<doctors.length;i++){
                if(doctors[i].label==$("#doctor" ).val() && doctors[i].value==$("#doctor_id" ).val()){
                    return ;
                }
            }
            $("#doctor" ).val('');
            $("#doctor_id" ).val('');
          }
          function checkHospitalId(){
            for(i=0;i<hospitals.length;i++){
                if(hospitals[i].label==$("#hospital" ).val() && hospitals[i].value==$("#hospital_id" ).val()){
                    return ;
                }
            }
            $("#hospital" ).val('');
            $("#hospital_id" ).val('');
          }
          function checkSpecializationId(){
            for(i=0;i<specilizations.length;i++){
                if(specilizations[i].label==$("#spec" ).val() && specilizations[i].value==$("#spec_id" ).val()){
                    return ;
                }
            }
            $("#spec" ).val('');
            $("#spec_id" ).val('');
          }  
  </script>