@extends('master')

@section('content')
        <?php if(!empty($data)){?>
        <?php foreach($data as $d){ ?>
	<div class="row">
		<div class="col-md-4 col-sm-12 col-xs-12">
		<div class="doctor-profile-block">
			<img src="<?php echo $d['doc_image'];?>" width="" alt="<?php echo $d['doc_image'];?>">
			<h4><?php echo $d['doctor_name'];?></h4>
			<span><?php echo $d['specialization_name'];?></span>
                        <?php if(!empty($d['special_notes'])){?>
			<span style="margin-top: 20px;">Special Notes: </span>
			<?php echo $d['special_notes'];?>
                        <?php } ?>
		</div>	
		</div>

		<div class="col-md-8 col-sm-12 col-xs-12">
		<div class="heading"><?php echo $d['hospital_name'];?></div>
                <?php
                $class='';
                $price='';
                if($booking['status']=='Full'){
                    $class='session-full';
                }elseif($booking['status']=='Holiday'){
                    $class='holiday';
                }elseif($booking['status']=='Available'){
                    $price='LKR '.number_format($booking['booking_info'][0]['amount_local'],2);
                }
                ?>
		<div class="session-block">
		<table width="100%" border="0" cellspacing="5" cellpadding="5">
		<tr>
		<td width="25%"><?php echo date('F d, Y',strtotime($booking['show_date'].' '.$booking['time'].':00')); ?><span><?php echo $booking['day'];?> <?php echo date('h:i A',strtotime($booking['show_date'].' '.$booking['time'].':00')); ?></span></td>
                <td width="35%" style="text-align: center;">Active Appointments<span style="color: #ff8513;"><?php echo sprintf("%02d",$booking['next_appointment_no']);?></span></td>
                <td width="25%" style="text-align: center;"><?php echo $booking['status'];?> <br /> <span style="color: #ff8513;"><?php echo date('h:i A',strtotime($booking['show_date'].' '.$booking['time'].':00')); ?></span> </td>
		<td width="15%"><span style="color: #ff8513; font-weight: 500; text-align: center;"><?php echo $price;?></span></td>	
		</tr>	
		</table>	
		</div>
                <form method="post" action="{{ url('/booking') }}">
		<div class="booking-form">
			<table width="100%" border="0" cellspacing="5" cellpadding="5">
				<tr>
				<td width="30%"><h4>Name</h4></td>
				<td width="70%">
                                    <select name="title">
				<option value="Mr">Mr.</option>
				<option value="Mrs">Mrs.</option>
				<option value="Mast">Mast.</option>
				<option value="Miss">Miss</option>
				<option value="Dr">Dr.</option>
				<option value="Dr (Ms.)">Dr (Ms.)</option>
				<option value="Dr (Mrs.)">Dr (Mrs.)</option>
				<option value="Prof">Prof.</option>
				<option value="Prof (Ms.)">Prof (Ms.)</option>
				<option value="Prof (Mrs.)">Prof (Mrs.)</option>
				<option value="Rev">Rev.</option>
				<option value="Baby">Baby</option>
				<option value="Sis">Sis.</option>
				<option value="Ms">Ms.</option>	
				</select>	
				</td>
			    </tr>
				<tr>
				<td>.</td>
                                <td><input type="text" name="name" placeholder="Name" required=""></td>	
				</tr>
				<tr>
				<td><h4>Phone</h4></td>
				<td><input type="text" name="phone" placeholder="Phone" required=""></td>	
				</tr>
				<tr>
				<td><h4>Nationality &nbsp; &nbsp;</h4></td>
				<td>
				<!-- radio -->
				<div class="form-inline required">
                    <div class="form-group has-feedback">
                        <label class="input-group">
                            <span class="input-group-addon">
                                <input type="radio" name="nationality" value="0" checked="" required=""/>
                            </span>
                            <div class="form-control form-control-static nationalty">
                                Local
                            </div>
                            <span class="glyphicon form-control-feedback "></span>
                        </label>
                    </div>
                    <div class="form-group has-feedback ">
                        <label class="input-group">
                            <span class="input-group-addon">
                                <input type="radio" name="nationality" value="1" />
                            </span>
                            <div class="form-control form-control-static nationalty">
                                Foreign 
                            </div>
                            <span class="glyphicon form-control-feedback "></span>
                        </label>
                    </div>
                </div>
				<!-- end radio -->	
				</td>	
				</tr>
				<tr>
				<td><h4>NIC or Passport Number</h4></td>
				<td><input type="text" name="nic" placeholder="NIC or Passport Number" required=""></td>	
				</tr>
				</tr>
				<!-- <tr>
				<td><h4>Passport</h4></td>
				<td><input type="text" name="" placeholder="Passport Number - Required if no NIC Number"></td>	
				</tr> -->
				<tr>
				<td><h4>Email</h4></td>
				<td><input type="email" name="" placeholder="Email"></td>	
				</tr>
				<tr>
				<td><input type="hidden" name="_token" value="{{ csrf_token() }}"></td>
                                <td><button type="submit">Pay Now</button></td>	
				</tr>	
			</table>
		</div>	
                </form>    
		</div>
	</div>
            <?php } ?>
            <?php }else {?>
            <div class="col-md-4 col-sm-12 col-xs-12">
            Unspecified error please try again.
            </div>
            <?php } ?>
	<br>
	<br>	
@stop