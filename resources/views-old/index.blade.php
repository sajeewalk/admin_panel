<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>{{$title}}</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{ asset('css/channlling_style.css') }}"> 
<script defer src="https://use.fontawesome.com/releases/v5.0.9/js/all.js" integrity="sha384-8iPTk2s/jMVj81dnzb/iFR2sdA7u06vHJyyLlAd4snFpCl/SnyUjRrbdJsw1pGIl"
    crossorigin="anonymous"></script> 

    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css"> -->

<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css"> -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css" />

    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>
  
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	
  
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
      var doctors;
      var hospitals;
      var specilizations;
  $( function() {
     doctors = <?php print_r(json_encode($doctors)); ?>;
     hospitals = <?php print_r(json_encode($hospitals)); ?>;
     specilizations = <?php print_r(json_encode($specializations)); ?>;
    
     $("#doctor").autocomplete({  
         minLength: 3,
        source: doctors,
        select: function( event, ui ) {

            $( "#doctor_id" ).val(ui.item.value);
            $( "#doctor" ).val( ui.item.label);
            return false;
        },
       
    });
    
     $("#hospital").autocomplete({  
         minLength: 3,
        source: hospitals,
        select: function( event, ui ) {

            $( "#hospital_id" ).val(ui.item.value);
            $( "#hospital" ).val( ui.item.label);
            return false;
        },
       
    });
    
     $("#spec").autocomplete({  
         minLength: 3,
        source: specilizations,
        select: function( event, ui ) {

            $( "#spec_id" ).val(ui.item.value);
            $( "#spec" ).val( ui.item.label);
            return false;
        },
       
    });    

  });
  </script>

</head>
<body style="background: url(images/bg.jpg) center center fixed; background-size: cover;">

<div class="container-fluid">

<!-- header -->
<div class="row bottom-border header">
            <div class="col-md-6 col-sm-6 col-xs-4 hemas-logo">
                <a href='{{ url('/index') }}'><img src="{{ asset('images/hemas-logo.png') }}" alt="placeholder+image"></a><!-- <br/> -->
                <!-- <h4 style="color: #e4e4e4;">Hemas Hospitals Lab Portal</h4>  -->
            </div>  
            <div class="col-md-6 col-sm-6 col-xs-8 right-header">
            <div>
            <div class="ayubolife"><img src="{{ asset('images/ayubolife.png') }}" width="80%" height="" alt="ayubo life"></div>
            </div>
            <a href="" style="color:#fff; background: transparent;" class="btn  btn-md" title="Logout"><!-- Logout -->
<!--             <table width="100%" border="0">
            <tr>
             <td><i class="fas fa-sign-out-alt"></i></td>
             <td> <span>Logout</span></td>
            </tr>
            </table>-->
            </a>
            <!-- timeline -->
            <!-- <a href="#" data-toggle="modal" data-target="#timeline" id="timeline-link" style="color:#fff; background: transparent;" class="btn  btn-md" title="Timeline">
            <table width="100%" border="0">
            <tr>
             <td><i class="fas fa-bars"></i></td>
             <td> <span>Timeline</span></td>
            </tr>
            </table>
            </a> -->
            <!-- end timeline -->
            <!-- Store -->
            <!-- <a href="dashboard/redirect_store" target="blank" style="color:#fff; background: transparent;" class="btn  btn-md" title="Store">
            <table width="100%" border="0">
            <tr>
             <td><i class="fas fa-shopping-cart"></i></td> 
             <td><span>Store</span></td>
              </tr>
            </table>
            </a> -->
            <!-- End store -->
            <!-- add Patient -->
            <!-- <a href="#" data-toggle="modal" data-target="#add_new_account" style="color:#fff; background: transparent;" title="Add Patient">
              <table width="100%" border="0">
              <tr>
               <td style="font-size: 18px;"><i class="fas fa-users"></td> 
               <td><span>Add Patient</span></td>
                </tr>
              </table>
              </a> -->
            <!-- end add patient -->

           <!--  <div style="float: right;" class="welcome-user">
            <div id="outside" style="display: inline-block; margin-top: 2px;">
            <label style="float: right;"><img :src="apiDomain+this.$store.getters.getGlobalMembers[0].user_pic" width="100%" height="" alt="user"></label>
            </div>
            </div> -->
        <!-- end model -->
      </div>
    </div>
<!-- end header -->

	<div class="row">
	<div class="col-md-7 col-sm-12 col-xs-12"><h1>Channel <br />Your Doctor</h1></div>	
	<div class="col-md-5 col-sm-12 col-xs-12 form-top-margin">
	<div class="front-search">
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <form method="post" action="search">
                <input type="text" name="doctor" id="doctor" placeholder="Doctor" onblur="checkDoctorId()">
                <input type="hidden" name="doctor_id" id="doctor_id">
                <input type="text" name="hospital" id="hospital" placeholder="Any Hospital" onblur="checkHospitalId()">
                <input type="hidden" name="hospital_id" id="hospital_id">
                <input type="text" name="specialization" id="spec" placeholder="Any Specialization" onblur="checkSpecializationId()">
                <input type="hidden" name="specialization_id" id="spec_id" >
                <input type='text' name="date" id='date' placeholder="Any Date" style="bottom: 0;" />
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <button type="submit">Search</button>
            </form>  
	</div>	
	</div>	
	</div>	
</div>	
    <script type="text/javascript">
      $(document).ready (function(){
          checkHospitalId();
          checkDoctorId();
          checkSpecializationId();
          
            $(function () {
                $('#date').datetimepicker(
                  {
                    format:'DD/MM/YYYY'
                  });
                //$("#date").val("code");
            });
          });
          
          function checkDoctorId(){
            for(i=0;i<doctors.length;i++){
                if(doctors[i].label==$("#doctor" ).val() && doctors[i].value==$("#doctor_id" ).val()){
                    return ;
                }
            }
            $("#doctor" ).val('');
            $("#doctor_id" ).val('');
          }
          function checkHospitalId(){
            for(i=0;i<hospitals.length;i++){
                if(hospitals[i].label==$("#hospital" ).val() && hospitals[i].value==$("#hospital_id" ).val()){
                    return ;
                }
            }
            $("#hospital" ).val('');
            $("#hospital_id" ).val('');
          }
          function checkSpecializationId(){
            for(i=0;i<specilizations.length;i++){
                if(specilizations[i].label==$("#spec" ).val() && specilizations[i].value==$("#spec_id" ).val()){
                    return ;
                }
            }
            $("#spec" ).val('');
            $("#spec_id" ).val('');
          }          
        </script>
</body>
</html>