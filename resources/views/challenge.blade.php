    @extends('master')
    @section('content')
    <div class="row content">
    <div class="col-md-2 col-sm-3 col-xs-12 sidebar">
    <!-- <button onclick="add_share_images()">Share images</button> -->
    <button onclick="add_treasure()">Treasure Setting</button>
    <button onclick="add_gift()">Gift</button>
    <button id="billboard_btn">Billboards</button>
    <button onclick="add_dynamicquiz()">Add Quiz</button>
    <button onclick="billboar_vendar()">Billboard vendors</button>
    <button onclick="leaderboard()">Leaderboard</button>
    <!-- <button onclick="add_button()">Buttons</button>
    <button onclick="add_list()">List</button>
    <button onclick="add_gif()">GIF</button>
    <button onclick="add_video_full()">Video</button>
    <button onclick="add_price()">Price</button>
    <button onclick="add_table()">Table</button>
    <button onclick="add_authors()">Author</button>
    <button onclick="add_terms_new()">Tearms & Conditions</button> -->
    </div>
    <div class="col-md-10 col-sm-9 col-xs-12 templates" >
    <form method="post" action="{{ url('dynamic_challenge_submit') }}" id="template_form">
    <div id="response">
    @if (session()->has('success'))
    <div class="alert alert-success" id="popup_notification">
    <strong>{{ session('success') }}</strong>
    </div>
    @endif
    @if ($errors->any())
    <div class="alert alert-danger">
    <ul>
    @foreach ($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
    </ul>
    </div>
    @endif
    </div>
    <div id="work_area">

    <div class="main_block">
    <h4>Challenge Name</h4>
    <div class="block">
    Enter name
    <input type="text" name="name" class="text-field" required>
    </div>
    Select Challenge Path
    <select name=field[challenge] id="challenge_id" class="form-control">
    <option value="1">Lockdown Challenge</option>
    <option value="2">Kandy to colombo</option>
    </select>
    </div>


    <div class="main_block">
    <h4>Steps Points Value </h4>
    <div class="block">
    Enter Challenge points
    <input type="number" min="0" max="10"  id="n1" name="steps_points_value" class="text-field" message="you can give score 0 to +10 only" required>
    </div>
    </div>

    <div class="main_block">
    <h4>Welcome</h4>
    <div class="block">
    Title
    <input type="text" name="field[Title]" class="text-field" required>
    </div>
    <div class="block">
    Select Join Banner image from image library
    <input type="text" value="" name="field[image_banner]" id="image_url" onclick=open_dialog("image_url") class="text-field" required>
    </div>
    <div class="block">
    <textarea name="field[body]" id="mytextarea"></textarea>
    </div>
    </div>

    <div class="main_block">
    <h4>Share images</h4>
    <div class="block">
    Select share card ready image from image library
    <input type="text" name="field[pending]" id="ready_image" onclick=open_dialog("ready_image") class="text-field" required>
    </div>
    <div class="block">
    Select share card completed image from image library
    <input type="text" name="field[completed]" id="completed_image" onclick=open_dialog("completed_image") class="text-field" required>
    </div>
    <div class="block">
    Select share card location image from image library
    <input type="text" name="field[ongoing]" id="ongoing_image" onclick=open_dialog("ongoing_image","image") class="text-field" required>
    </div>

    </div>

    </div>
    <div class="footer_btn">
    <input type="hidden" name="type" id="type" value="">
    <input type="hidden" name="id" id="id" value="####">
    <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
    <!-- <button data-toggle="modal" data-target="#preview_template" type="button">Preview</button> -->
    <button type="submit">Save</button>
    </div>
    </form>
    </div>
    </div>

    <!-- popup -->
    <div class="modal fade" id="myModal">
    <div class="modal-dialog modal-xl modal-dialog-scrollable">
    <div class="modal-content">

    <!-- Modal Header -->
    <div class="modal-header">
    <h4 class="modal-title">Select Image</h4>
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    </div>

    <!-- Modal body -->
    <div class="modal-body">
    <div><input type="search" id="myInput1" name="myInput1" placeholder="Search"></div>
    <div id="myDIV" class="row">
    <div id="gallery-images"></div>
    </div>
    </div>

    <!-- Modal footer -->
    <div class="modal-footer">
    <button type="button" class="btn btn-secondary">Insert</button>
    </div>

    </div>
    </div>
    </div>
    <!-- end popup -->

    <script>
    var id = 0;
    var billid = 0;
    var counter = 1;
    var quizid = 0;
    var billboard_add =1;

    tinymce.init({
    selector: '#mytextarea'
    });

    $('#work_area').on('click', '.remove', function () {
    $(this).parent().parent().remove();
    counter++;
    });

    function billboar_vendar(){
    billboar_vendar_section ='<div class="main_block">\
    <h4>Billboard Vendors Setting</h4>\
    <div class="block">\
    Enter billboard vendors minimum\
    <input type="number" name="billboard_vendors_min" class="text-field">\
    </div>\
    <div id=field-' + counter + '-billboard-more ></div>\
    <div id=button-'+counter+'></div>\
    <a onclick=add_bilboard_more(this)>\
    <div class="block">\
    <img src="images/add.png" alt="placeholder+image">\
    <label>Add Billboard Advertisement</label>\
    </div>\
    </a>\
    <div class="close">\
    <span class="remove"><img src="images/delete.png" width="100%" alt="placeholder+image"></span>\
    </div>\
    </div>';
    $(work_area).append(billboar_vendar_section);
    $('#button-'+counter).val(counter);
    counter++;
    }


    function add_billboard_force(id) {
    billboard_add++;
    add_new = '<div class="button-remove">\
    <div class="main_block">\
    Billboard Advertisement\
    <select name="billboard[]" id="data_id_'+(billboard_add-1)+'" class="form-control" size="4">';
    @foreach($billbord as $keyfunction)
    add_new += '<option value={{$keyfunction->id}} id="test_'+(billboard_add-1)+'">{{str_replace(array("\r\n", "\n", "\r"), ' ', $keyfunction->name)}}</option>';
    @endforeach
    add_new += '</select>\
    </div>\
    <div class="block">\
    impressions\
    <input type="number" name="impressions[]" class="text-field">\
    </div>\
    <a onclick=add_button_remove(this)>\
    <div class="block" style="color:#f44747;float:right">\
    <label>Remove</label>\
    </div>\
    </a>\
    </div>';
    $('#field-' + id + '-billboard-more').append(add_new);

    }

    function add_button_remove(obj) {
    $(obj).closest('.button-remove').remove();
    }

    function add_bilboard_more(obj) {
    id=$(obj).prev("div").val();
    add_billboard_force(id);
    }

    function add_treasure() {
    treasure = '<div class="main_block">\
    <h4>Treasure Setting</h4>\
    <div class="block">\
    Enter Lost heading \
    <input type="text" name=field[lost_heading] class="text-field">\
    </div>\
    <div class="block">\
    Lost paragraph\
    <input type="text" name=field[lost_paragraph] class="text-field">\
    </div>\
    <div class="block">\
    Probability\
    <input type="number" name=field[probability] class="text-field">\
    </div>\
    <div class="block">\
    Raffle draw entries\
    <select name=field[raffle_draw_entries] class="form-control">\
    <option value="false">False</option>\
    <option value="true">True</option>\
    </select>\
    </div>\
    <div class="close">\
    <span class="remove"><img src="images/delete.png" width="100%" alt="placeholder+image"></span>\
    </div>\
    </div>';
    $('#work_area').append(treasure);
    counter++;
    }

    function leaderboard() {
    leaderboard = '<div class="main_block">\
    <h4>leaderboard Setting</h4>\
    <input type="text" value="banner" name=field[type] class="text-field" hidden>\
    <input type="text" value="" name=field[heading] class="text-field" hidden>\
    <input type="text" value="" name=field[text] class="text-field"hidden>\
    <input type="text" value="" name=field[icon] class="text-field"hidden>\
    <input type="text" value="leaderboard" name=field[leaderboard_action] class="text-field"hidden>\
    <input type="text" value="custom/include/images/challenge_images/v7/leaderboard.png" name=field[image_leaderboard] class="text-field"hidden>\
    <h3>Set Up the Leaderboard!</h3>\
    <div class="close">\
    <span class="remove"><img src="images/delete.png" width="100%" alt="placeholder+image"></span>\
    </div>\
    </div>';
    $('#work_area').append(leaderboard);
    counter++;
    }

    function add_billboards() {
    billid++
    billboard = '<div class="main_block">\
    <div class="options_outer_container" id="options_outer_container">\
    <div class="number">'+billid+'</div>\
    <br>\
    <h4>Billboards Setting</h4>\
    <div class="block">\
    collected image from image library\
    <input type="text" name="field[collected_img]" id="bill_image_'+(billid-1)+'" onclick=open_dialog("bill_image_'+(billid-1)+'") class="text-field">\
    </div>\
    <div class="block">\
    Meta\
    <input type="number" name=field[meta_billboard] class="text-field">\
    </div>\
    <div class="block">\
    Action\
    <select name=field[action_billboard] class="form-control">\
    <option value="native_post">Native Post</option>\
    </select>\
    </div>\
    </div>\
    </div>';
    $('#work_area').append(billboard);
    counter++;
    }

    function add_dynamicquiz() {
    quizid++
    quiz = '<div class="main_block">\
    <h4>Dynamic Questionnaire Setting</h4>\
    Select Questions\
    <select name="field[questionnaire][]"  class="form-control" size="6" multiple>';
    @foreach($questionnaire as $que)
    quiz += '<option value={{$que->id}} id="example">{{str_replace(array("\r\n", "\n", "\r"), ' ', $que->type)}}</option>';
    @endforeach
    quiz += '</select>\
    <div class="block">\
    points value\
    <input type="number" name="field[points_value]" class="text-field">\
    </div>\
    <div class="close">\
    <span class="remove"><img src="images/delete.png" width="100%" alt="placeholder+image"></span>\
    </div>\
    </div>';
    $('#work_area').append(quiz);
    counter++;
    }

    $(document).ready(function(){
    $('#billboard_btn').on("click",function(){
    $.ajax({
    type: 'get',
    url: '{{ url('/challenge/billboard') }}' + '/' + $("#challenge_id").val(),
    data: [],
    success: function (data) {
    for ($x = 1; $x <= data; $x++) {
    add_billboards();
    }
    }
    });
    });
    });

    function add_gift() {
    id++;
    heading = '<div class="main_block">\
    <div class="options_outer_container" id="options_outer_container">\
    <div class="number">'+id+'</div>\
    <br>\
    <h4>Gift Setting</h4>\
    <div class="block">\
    Enter Vendor Name \
    <input type="text" name="names[]" class="text-field">\
    </div>\
    <div class="block">\
    Enter Gift Name \
    <input type="text" name="product[]" class="text-field">\
    </div>\
    <div class="block">\
    Enter message \
    <textarea name="message[]" class="text-field"></textarea>\
    </div>\
    <div class="block">\
    Select gift logo image from image library\
    <input type="text" name="logo[]" id="logo_image_'+(id-1)+'" onclick=open_dialog("logo_image_'+(id-1)+'") class="text-field">\
    </div>\
    <div class="block">\
    Enter Total number of gift\
    <input type="number" name="count[]" class="text-field">\
    </div>\
    <div class="block">\
    Enter enable the gift aftre step\
    <input type="number" name="steps_limit[]" class="text-field">\
    </div>\
    <div class="block">\
    Enter gift per user\
    <input type="number" name="countEach[]"class="text-field">\
    </div>\
    <div class="block">\
    Select life point enable\
    <select name="life_points[]" class="form-control">\
    <option value="true">True</option>\
    <option value="false">False</option>\
    </select>\
    </div>\
    <div class="block">\
    Enter point value\
    <input type="text" name="point_value[]" class="text-field">\
    </div>\
    <div class="block">\
    Select gender\
    <select name="Gender[]" class="form-control">\
    <option value=""></option>\
    <option value="Male">Male</option>\
    <option value="Female">Female</option>\
    </select>\
    </div>\
    </div>\
    <div class="close">\
    <span class="remove"><img src="images/delete.png" width="100%" alt="placeholder+image"></span>\
    </div>\
    </div>';
    $('#work_area').append(heading);
    }


    </script>


    <script>


    $('#myModal').on('show.bs.modal', function () {
    load_gallery_images();
    });

    function load_gallery_images(folder){
    folder = typeof folder !== 'undefined' ? folder : '';
    url='{{ url('image_library/popup/') }}';
    if(folder!=''){
    url='{{ url('image_library/popup') }}'+'/'+folder;
    }
    $.ajax({
    url: url,
    type: 'get',
    data:{"image_type":image_type},
    success: function (data) {
    $('#gallery-images').html(data);
    //                    styleload();
    },
    complete: function () {

    },
    error: function (data) {

    },
    timeout: 120000,
    });
    }

    selected_obaject='';
    function open_dialog(ref,type){
    image_type=type;
    selected_obaject=ref;
    jQuery("#myModal").modal('show');
    }
    function insert_url(url){
    $("#"+selected_obaject).val(url);
    $("#"+selected_obaject+"-thumb").attr("src",url);
    $('#myModal').fadeOut();
    $('#myModal').modal('hide');
    }
    </script>
    @endsection
