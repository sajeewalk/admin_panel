@extends('master')

@section('content')
<div class="row content" style="background: rgba(255,255,255,0.7); padding: 30px 15px;">
    <div class="col-md-10 col-sm-10 col-xs-12">
        <h5>Experts</h5>
    </div>


    <div class="col-md-12 col-sm-12 col-xs-12">
        <table width="100%" id="example"  class="table table-striped" width="100%">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>User </th>
                    <th>Qualifications</th>
                    <th>SLMC</th>
                    <th>Specialization</th>
                    <th>Date created</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($data as $d) { ?>  
                    <?php
                    $specialitations = App\Repositories\OnboardRepository::get_specializations();
                    $spec = isset($specialitations[str_replace('^', '', $d->specialization_c)]) ? $specialitations[str_replace('^', '', $d->specialization_c)] : '';
                    ?>                
                    <tr>
                        <td>{{ $d->first_name }} {{ $d->last_name }}</td>
                        <td>{{$d->user_name}}</td>                       
                        <td>{{$d->qualification_and_destinatio_c}}</td>
                        <td>{{$d->slmc_no_c}}</td>
                        <td>{{ $spec}}</td>
                        <td>{{$d->date_entered}}</td> 
                    </tr>           
                <?php } ?>   
            </tbody>
        </table>

    </div>   
        


    <script type="text/javascript">
        $(document).ready(function () {
            $('#example').DataTable({
           "order": [[ 5, "desc" ]]
           });
        });
    </script>

</div>	






@stop
