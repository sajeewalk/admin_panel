@extends('master_promotion')

@section('content')
<style>
select {
    width: 100%;
    border: 1px solid #dbdbdb;
    border-radius: 5px;
    margin-top: 0px;
    margin-bottom: 5px;
    text-indent: 14px;
    height: 40px;
} 
#cover-spin {
    position:fixed;
    width:100%;
    left:0;right:0;top:0;bottom:0;
    background-color: rgba(255,255,255,0.7);
    z-index:9999;
    display:none;
}

@-webkit-keyframes spin {
	from {-webkit-transform:rotate(0deg);}
	to {-webkit-transform:rotate(360deg);}
}

@keyframes spin {
	from {transform:rotate(0deg);}
	to {transform:rotate(360deg);}
}

#cover-spin::after {
    content:'';
    display:block;
    position:absolute;
    left:48%;top:40%;
    width:40px;height:40px;
    border-style:solid;
    border-color:black;
    border-top-color:transparent;
    border-width: 4px;
    border-radius:50%;
    -webkit-animation: spin .8s linear infinite;
    animation: spin .8s linear infinite;
}
</style>
<div class="row content" style="padding: 30px 15px; margin-top: 100px;">
    <div id="cover-spin"></div>
<form method="post" action="{{ url('promotion/form').'?company='.$promotion->promotion }}" id="promotion_form">
    <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
    <div class="offset-md-3 col-md-6 col-sm-12 col-xs-12 create_program">
<div id="response">
            @if (session()->has('success'))
            <div class="alert alert-success" id="popup_notification">
                <strong>{{ session('success') }}</strong>
            </div>
            @endif
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif             
            </div>        
        <h5>Enter details</h5>


        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <label>Mother’s Name</label>
                <input type="text" name="mother_name"  required>
            </div> 
            <div class="col-md-12 col-sm-12 col-xs-12">
                <label>Mother’s Mobile Number</label>
                <input type="text" name="mother_mobile"  inputmode="numeric" required pattern="[0]{1}[7]{1}[0-9]{8}" title="Phone number starting 07 with 10 digits" maxlength="10">
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
                <label>Farlin Branch</label>
                <select name="branch" required>
                    <option></option>
                    <option>Farlin Bambalapitiya</option>
                    <option>Kids Land Kandana</option>
                    <option>Kids Land Negombo</option>
                    <option>Kids Land Borella</option>
                    <option>Kids Unlimited</option>
                    <option>Cool Planet Colombo</option>
                    <option>Cool Planet Wattala</option>
                    <option>Cool Planet Pellewatta</option>
                    <option>Cool Planet Kandy</option>
                    <option>Cool Planet Maharagama</option>
                    <option>Kids World Kottawa</option>
                    <option>Kids World Thalawathugoda</option>
                    <option>Wish Hospital</option>
                    <option>Nesh</option>
                    <option>Baby Dry</option>
                    <option>Other</option>
                </select>
            </div>   
            <div class="col-md-12 col-sm-12 col-xs-12">
                <label>Sales Agent Name</label>
                <input type="text" name="agent" required="">
            </div> 
            <div class="col-md-12 col-sm-12 col-xs-12">
                <label>Bill id</label>
                <input type="text" name="bill_id" required="">
            </div> 
            <div class="col-md-12 col-sm-12 col-xs-12">
                <label>Bill value</label>
                <input type="text" name="bill_value" required="">
            </div>             
            <div class="col-md-12 col-sm-12 col-xs-12">
                <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
                <button type="submit" id="sub-button" >Create</button>
            </div>            
        </div>
      
 

    </div>
</form>




   

</div>	
    <!-- popup -->
    <div class="modal fade" id="myModal">
    <div class="modal-dialog modal-xl modal-dialog-scrollable">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Select Image</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
        <div><input type="search" id="myInput1" name="myInput1" placeholder="Search"></div>
        <div id="myDIV" class="row">              
            <div id="gallery-images"></div>
        </div>
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary">Insert</button>
        </div>
        
      </div>
    </div>
  </div>
    <!-- end popup -->
<script>
 
$(document).ready(function(){
    $('#promotion_form').on('submit', function(e){
        $('#cover-spin').show(0);
        $("#sub-button").hide();
    });
});
   function open_dialog(ref,type){
       image_type=type;
       selected_obaject=ref;
       jQuery("#myModal").modal('show');         
    } 
       $('#myModal').on('show.bs.modal', function () {
            load_gallery_images();
        }); 
        function load_gallery_images(folder){ 
            folder = typeof folder !== 'undefined' ? folder : '';
            url='{{ url('image_library/popup/') }}';
            if(folder!=''){
                url='{{ url('image_library/popup') }}'+'/'+folder;
            }
             $.ajax({
                url: url,
                type: 'get',
                data:{"image_type":image_type},
                success: function (data) {
                    $('#gallery-images').html(data);
//                    styleload();
                },
                complete: function () {
                   
                },
                error: function (data) {

                },
                timeout: 120000,
            });           
        }
   function insert_url(url){
       $("#"+selected_obaject).val(url);
       $("#"+selected_obaject+"-thumb").attr("src",url);
       $('#myModal').fadeOut();
       $('#myModal').modal('hide');
    }        
</script>

@stop
