@extends('master')

@section('content')
<div class="row content" style="background: rgba(255,255,255,0.7); padding: 30px 15px;">
    <div class="col-md-10 col-sm-10 col-xs-12">
        <h5>Transactions</h5>
    </div>
      <div class="col-md-2 col-sm-2 col-xs-12">
<!--           <a href="" data-toggle="modal" data-target="#myModal1">
           <span>
           <img src="{{ url('images/user.png')}}" height="30px" alt="placeholder+image"> 
           <p>100</p>
           </span>
           </a>-->

<!--           <a href="{{ url('onboard/create')}}">
           <span>
           <img src="{{ url('images/man.png')}}" height="30px" alt="placeholder+image"> 
           <p>Create</p>   
           </span>
           </a>-->

       </div>

    <div class="col-md-12 col-sm-12 col-xs-12">
        <table width="100%" id="example" cellpadding="5" cellspacing="5" border="0" class="table table-striped">
            <thead>
                <tr>
                    <th>Order Id</th>
                    <th>related_type </th>
                    <th>related_id</th>
                    <th>amount</th>
                    <th>status_code</th>
                    <th>user_id</th>
                    <th>payment_method</th>
                    <th>created_at</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($data as $d) { ?>  
                    <?php
                    $user = App\User::where('id', '=', $d->user_id)->first();
                    ?>                
                    <tr>
                        <td>{{ $d->id }}</td>
                        <td>{{$d->related_type}}</td>                       
                        <td>{{$d->related_id}}</td>
                        <td>{{$d->amount}}</td>
                        <td>{{$d->status_code}}</td>
                        <td>@if($user) {{$user->user_name}} {{$user->first_name}} {{$user->last_name}} @endif</td>
                        <td>{{$d->payment_method}}</td> 
                        <td>{{$d->created_at}}</td> 
                    </tr>           
                <?php } ?>
            </tbody></table>           
                <!-- popup -->
            <div class="modal fade" id="assign_prescription">
                <div class="modal-dialog modal-dialog-scrollable">
                    <div class="modal-content">

                        <!-- Modal Header -->
                        <div class="modal-header">
                            <h4 class="modal-title">Assign prescription</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>

                        <!-- Modal body -->
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-6 col-md-6 col-xs-6">User </div>
                                <div class="col-md-6 col-md-6 col-xs-6"> 
                                    <select class="form-control" id="user-list" name="user-list">
                                        <?php
                                        $users = App\User::where('role', '=', 'pharmacy_operator')->get();
                                        ?>
                                        <?php foreach ($users as $user) { ?>
                                            <option value="{{$user->id}}">{{$user->first_name}} {{$user->last_name}}</option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 col-md-12 col-xs-12"><div id="d_msg_s" class="alert-primary"></div><div id="d_msg_e" class="alert-danger"></div></div>
                            </div>

                        </div>

                        <!-- Modal footer -->
                        <div class="modal-footer">
                            <input type="hidden" name="u_id" id="u_id" class="text-field">
                            <button type="button" class="btn btn-secondary assign_user" id="btnSubmit" onclick="assign_user()">Save</button>
                        </div>

                    </div>
                </div>
            </div>

            <div class="modal fade" id="history">
                <div class="modal-dialog modal-dialog-scrollable">
                    <div class="modal-content">

                        <!-- Modal Header -->
                        <div class="modal-header">
                            <h4 class="modal-title">Status Changes</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>

                        <!-- Modal body -->
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12 col-md-12 col-xs-12"> 
                                    <div id="status-history">Loading ...</div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 col-md-12 col-xs-12"><div id="d_msg_s" class="alert-primary"></div><div id="d_msg_e" class="alert-danger"></div></div>
                            </div>

                        </div>



                    </div>
                </div>
            </div>  

            <div class="modal fade" id="status_pop">
                <div class="modal-dialog modal-dialog-scrollable">
                    <div class="modal-content">

                        <!-- Modal Header -->
                        <div class="modal-header">
                            <h4 class="modal-title">Change status</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>

                        <!-- Modal body -->
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-6 col-md-6 col-xs-6">Status </div>
                                <div class="col-md-6 col-md-6 col-xs-6"> 
                                    <select class="form-control" id="status-list" name="status-list">
                                        <option>Processing</option>
                                        <option>Delivering</option>
                                        <option>Completed</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 col-md-12 col-xs-12"><div id="d_msg_p" class="alert-primary"></div><div id="d_msg_e" class="alert-danger"></div></div>
                            </div>

                        </div>

                        <!-- Modal footer -->
                        <div class="modal-footer">
                            <input type="hidden" name="u_id" id="u_id" class="text-field">
                            <button type="button" class="btn btn-secondary status-change" id="btnSubmit" onclick="change_status()">Save</button>
                        </div>

                    </div>
                </div>
            </div>            
            <!-- end popup --> 
            </tbody>
        </table>
    </div>   

    <script>
        prescription_id = '';
        $(document).on("click", ".assign_prescription", function () {
        prescription_id = $(this).data('id');
        $("#d_msg_s").html('');
        $("#btnSubmit").attr("disabled", false);
        });
        function assign_user() {
        var u_id = $("#user-list").val();
        $("#btnSubmit").attr("disabled", true);
        var user_id = $("#user-list").val();
        var id = $("#native_id").val();        
        $.ajax({
        url: '{{ url('pharmacy/assign') }}/' + prescription_id + '/' + u_id,
                type: 'get',
                data: {prescription: prescription_id, user: u_id},
                success: function (data) {
                $("#btnSubmit").attr("disabled", false);
                if (data.toString() == 1) {
                $("#d_msg_s").html('Prescription assigned to the user.');
//                window.setTimeout(function(){location.reload()}, 3000)
                } else {
                $("#d_msg_e").html('Errors occured.');
                $("#btnSubmit").attr("disabled", false);
                }
                //                    styleload();
                },
                complete: function () {
                    $("#assigned-"+prescription_id).html($("#user-list option:selected").text());
                    $("#status-"+prescription_id).html('Assigned');
                },
                error: function (data) {

                },
                timeout: 120000,
        });
        }

        $(document).on("click", ".history_change", function () {
        prescription_id = $(this).data('id');
        $.ajax({
        url: '{{ url('pharmacy/history') }}/' + prescription_id,
                type: 'get',
                success: function (data) {
                $("#btnSubmit").attr("disabled", false);
                $("#status-history").html(data);
                },
                complete: function () {

                },
                error: function (data) {

                },
                timeout: 120000,
        });
        });
        $(document).on("click", ".status_pop", function () {
        $("#d_msg_p").html('');    
        prescription_id = $(this).data('id');
        });
        function change_status() {
        var status = $("#status-list").val();
        $("#btnSubmit").attr("disabled", true);
        $.ajax({
        url: '{{ url('pharmacy/status') }}/' + prescription_id + '/' + status,
                type: 'get',
                success: function (data) {
                $("#btnSubmit").attr("disabled", false);
                if (data.toString() == 1) {
                $("#d_msg_p").html('Status changed.');
//                window.setTimeout(function(){location.reload()}, 3000)
                } else {
                $("#d_msg_p").html('Errors occured.');
                $("#btnSubmit").attr("disabled", false);
                }
                //                    styleload();
                },
                complete: function () {
                    $("#status-"+prescription_id).html($("#status-list option:selected").text());
                },
                error: function (data) {

                },
                timeout: 120000,
        });
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#example').DataTable({
           "order": [[ 0, "desc" ]]
           });
        });
    </script>

</div>	






@stop
