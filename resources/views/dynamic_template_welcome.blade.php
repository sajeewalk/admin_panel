
@extends('master')

@section('content')
<div class="row content">
    <div class="offset-md-4 col-md-2 col-sm-3 col-xs-12 home_card">
      <a href="{{ url('dynamic_template')}}"><div>
            <img src="{{ asset('images/dyamic_template.png') }}" width="" alt="placeholder+image">
            <h5> Create Dynamic<br />Template</h5>
        </div></a>
    </div>

    <div class="col-md-2 col-sm-3 col-xs-12 home_card">
        <a href="{{ url('nativepost_table')}}"><div>
            <img src="{{ asset('images/manage.png') }}" width="" alt="placeholder+image">
            <h5>All Dynamic<br />Template</h5>
        </div></a>
    </div>
</div>
@stop

<!-- <section id="section-feature" class="container">
 <div class="row">
     <ul>
       <li  class="col-md-4 col-sm-6 col-xs-12">
         <div class="card">
            <a href="{{ url('/home') }}">
               <div class="sf-icon">
                   <i class="fa fa-question-circle fa-5x"></i>
                   </a>
                     <a href="{{ url('/home') }}">
                   <h3>Questionnaire</h3>
                   </a>
               </div>
                  </div>
         </li>


         <li class="col-md-6 col-sm-6 col-xs-12">
               <div class="card">
                 <a  href="{{ url('/dashboard') }}">
               <div class="sf-icon">
                   <i class="fa fa-tachometer fa-5x"></i>
                   </a>
                   <a href="{{ url('/dashboard') }}">
                   <h3>Dash Board</h3>
                   </a>
                   </div>
               </div>
         </li>

     </ul>
 </div>
</section> -->
