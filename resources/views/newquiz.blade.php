@extends('master')

@section('content')



        <div class="row content">

        <div class="col-md-12 col-sm-12 col-xs-12 templates" >
        <form id="test" method="post" action="{{URL::to('/save_option')}}" enctype="multipart/form-data">

        <h3 class="">Questions Form</h3>
        <div id="response">
        <?php $exception = Session::get('exception'); ?>
        @if($exception)
        <p class="alert alert-success">
        {{$exception}}
        </p>
        <?php Session::put('exception',null); ?>

          @endif
          @if(count($errors)>0)
          <ul>
          @foreach($errors->all() as $error)
          <li class="alert alert-danger">{{$error}}</li>
          @endforeach
          </ul>
          @endif
          </div>
          {{csrf_field()}}
          <div class="container">
          <div class="row">
          <div class="col-md-6">
          <div class="options_outer_container" id="options_outer_container">

          <div class="main_block">
          <h4>Question Name</h4>
          <div class="block">
          Enter name
          <input type="text" name="question" class="text-field" value="">
          </div>
          </div>

          <input type="text" name="question_id"  value="{{$questionnaire_id}}" hidden>

          <div class="main_block">
          <h4>Type</h4>
          <select name="type" class="form-control">
          <option value="button">Button</option>
          <option value="image">Image</option>
          <option value="text">Text</option>
          <option value="date">Date</option>
          <option value="location">Location</option>
          </select>
          </div>

          <div class="main_block">
          <h4>Max Selection</h4>
          <select name="max_selection" class="form-control">
          <option value="0">0</option>
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
          <option value="6">6</option>
          <option value="6">6</option>
          </select>
          </div>
          </div>

            <div class="options_outer_container" id="options_outer_container">
            <div class="main_block">
            <h4>Hint Show</h4>
            <select name="hint_show" class="form-control">
            <option value="0"selected>Hide</option>
            <option value="1">Show</option>
            </select>
            </div>

            <div class="main_block">
            <h4>Hint Action</h4>
            <select name="hint_action" class="form-control">
            <option value=""></option>
            <option value="native_post">Native Post</option>
            </select>
            </div>

            <div class="main_block">
            <h4>Hint Meta</h4>
            <div class="block">
            Enter Meta ID
            <input type="text" name="hint_meta" value="">
            </div>
            </div>

            </div>
            </div>

          <div class="col-md-6">
          <div class="options_outer_container" id="options_outer_container">

          <div class="main_block">
          <h4>Quiz Image</h4>
          <div class="block">
          Select image from image library
          <input type="text" value="" name="image" id="image_url" onclick=open_dialog("image_url") class="text-field">
          </div>
          </div>

          <div class="main_block">
          <h4>Sex</h4>
          <select name="sex" class="custom-select">
          <option value="a">Both</option>
          <option value="f">Female</option>
          <option value="M">Male</option>
          </select>
          </div>

          <div class="main_block">
          <h4>Question Type</h4>
          <select name="question_type" class="custom-select">
          <option value="s">Single</option>
          <option value="M">Multiple</option>
          </select>
          </div>
          </div>



          <div class="block">
          <h4>Create Option +</h4>
          <button class="btn btn-info" id="savebtnn" type="button">+ Add</button>
          <br>
          <br>
          <hr>

          </div>

          <div id="outerdiv">

          </div>


          </div>
          </div>
          </div>
          <button type="submit" class="btn btn-success btn-block">Save</button>
          </div>
          </div>
          </form>
          </div>



    <!-- popup -->
    <div class="modal fade" id="myModal">
    <div class="modal-dialog modal-xl modal-dialog-scrollable">
    <div class="modal-content">

    <!-- Modal Header -->
    <div class="modal-header">
    <h4 class="modal-title">Select Image</h4>
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    </div>

    <!-- Modal body -->
    <div class="modal-body">
    <div id="gallery-images"></div>
    </div>

    <!-- Modal footer -->
    <div class="modal-footer">
    <button type="button" class="btn btn-secondary">Insert</button>
    </div>

    </div>
    </div>
    </div>
    <!-- end popup -->

</div>

<script>
var id=0;
$('#savebtnn').on('click',function(){
    addOptionsInputss();
    console.log('clicked');
});

function addOptionsInputss(){
  id++;
  var html_string = '<div class="options_outer_container" id="options_outer_container"><div class="close"><span class="remove"><img src="images/plus.png" width="100%"></span></div><div class="number">'+id+'</div><hr /><div class="main_block"><h4>Options Name</h4><div class="block">Enter Option Name<input type="text" name="option_name[]" class="text-field" value=""></div></div><div class="main_block" hidden><h4>Skip</h4><select name="skip['+(id-1)+'][]"class="form-control"size="4"multiple hidden>';
  @foreach($questions as $tests)
  html_string += '<option value={{$tests->id}} >{{str_replace(array("\r\n", "\n", "\r"), ' ', $tests->question)}}</option>';
  @endforeach
  html_string += '</select></div><div class="main_block"><h4>Correct</h4><select name="correct[]" class="custom-select"><option value="yes">yes</option><option value="no">no</option></select></div><div class="main_block"><h4>Button Image</h4><div class="block">Select image from image library<input type="text" value="" name="button_image[]" id="btton_url_'+(id-1)+'" onclick=open_dialog("btton_url_'+(id-1)+'") class="text-field"></div></div><div class="main_block"><h4>Answer Image</h4><div class="block">Select image from image library<input type="text" value="" name="answer_image[]" id="anwser_url_'+(id-1)+'" onclick=open_dialog("anwser_url_'+(id-1)+'") class="text-field"></div></div><div class="main_block"><h4>Output Image</h4><div class="block">Select image from image library<input type="text" value="" name="output_image[]" id="output_url_'+(id-1)+'" onclick=open_dialog("output_url_'+(id-1)+'") class="text-field"></div></div><div class="main_block"><h4>Output Title</h4><div class="block">Enter Output Title<input type="text" name="output_tile[]" class="text-field" value=""></div></div><div class="main_block"><h4>Output Icon Image</h4><div class="block">Select image from image library<input type="text" value="" name="output_icon[]" id="icon_url_'+(id-1)+'" onclick=open_dialog("icon_url_'+(id-1)+'") class="text-field"></div></div><div class="main_block"><h4>App Action Text</h4><div class="block">Enter App Action Text<textarea  class="form-control p-input" name="output_text[]" id="comment" rows="5" value=""></textarea></div></div>';

  $('#outerdiv').prepend(html_string);
}
</script>
<script>
var id=0;
$('#hintbtnn').on('click',function(){
    addOptionsInputs();
    console.log('clicked');
});

function addOptionsInputs(){


  var html_string = '';

  $('#outerdivv').prepend(html_string);

}

</script>

<script>

    $('#outerdiv').on('click', '.remove', function () {
        $(this).parent().parent().remove();
        counter++;
    });

       $('#myModal').on('show.bs.modal', function () {
            load_gallery_images();
        });



        function load_gallery_images(folder){
            folder = typeof folder !== 'undefined' ? folder : '';
            url='{{ url('image_library/popup/') }}';
            if(folder!=''){
                url='{{ url('image_library/popup') }}'+'/'+folder;
            }
             $.ajax({
                url: url,
                type: 'get',
                data:{"image_type":image_type},
                success: function (data) {
                    $('#gallery-images').html(data);
//                    styleload();
                },
                complete: function () {

                },
                error: function (data) {

                },
                timeout: 120000,
            });
        }
   selected_obaject='';
   function open_dialog(ref,type){
       image_type=type;
       selected_obaject=ref;
       jQuery("#myModal").modal('show');
    }
   function insert_url(url){
       $("#"+selected_obaject).val(url);
       $("#"+selected_obaject+"-thumb").attr("src",url);
       $('#myModal').fadeOut();
       $('#myModal').modal('hide');
    }
</script>
@stop
