@extends('master')

@section('content')
<div class="row content" style="background: rgba(255,255,255,0.7); padding: 30px 15px;">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <h5>Native Post Table</h5>
    </div>


<div class="col-md-12 col-sm-12 col-xs-12">
    <table width="100%" cellpadding="5" cellspacing="5" border="0" class="table table-striped">
        <thead>
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Date</th>
                <th>JSON</th>
                <th>Action</th>
                <th>Tile</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($data as $d){?>
            <tr>
                <td>{{$d->id}}</td>
                <td>{{$d->name}}</td>
                <td>{{$d->created_at}}</td>
                <td>{{$d->json}}</td>
                <td>
<!--                    <button class="btn btn-info">Preview</button> -->
                    <a href="{{ url('dynamic_template/edit/'.$d->id) }}" class="btn btn-success">Edit</a>
<!--                    <button class="btn btn-danger">Delete</button>-->
                </td>
                <td>
                     <a href="{{ url('dynamic_template/edit/'.$d->id) }}" class="btn btn-success tile_create" data-toggle="modal" data-target="#create_tile" data-id="{{$d->id}}">Create</a>
                </td>
            </tr>
            <?php } ?>
    <!-- popup -->
    <div class="modal fade" id="create_tile">
    <div class="modal-dialog modal-dialog-scrollable">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Create Tile</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
            <div class="row">
                <div class="col-md-6 col-md-6 col-xs-6">Mobile </div>
                <div class="col-md-6 col-md-6 col-xs-6"><input type="text" name="mobile" id="mobile" class="text-field"> </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-md-12 col-xs-12"><div id="d_msg_s" class="alert-primary"></div><div id="d_msg_e" class="alert-danger"></div></div>
            </div>

        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
            <input type="hidden" name="native_id" id="native_id" class="text-field">
            <button type="button" class="btn btn-secondary" id="btnSubmit" onclick="np_create_tile()">Save</button>
        </div>
        
      </div>
    </div>
  </div>
    <!-- end popup --> 
        </tbody>
    </table>
</div>   

<script>
//       $('#create_tile').on('show.bs.modal', function () {
//            
//        });
        $(document).on("click", ".tile_create", function () {
            var id=$(this).data('id');
            $("#native_id").val( id );
            var mobile=$("#mobile").val();
            $("#btnSubmit").attr("disabled", false);
            
        });
        function np_create_tile(){
            $("#d_msg_s").html('');
            $("#d_msg_e").html('');
            $("#btnSubmit").attr("disabled", true);
            var mobile=$("#mobile").val();
            var id=$("#native_id").val();
             $.ajax({
                url: '{{ url('dynamic_template/create_tile') }}',
                type: 'get',
                data:{mobile:mobile,id:id},
                success: function (data) {
                    if(data.toString()==1){
                        $("#d_msg_s").html('Tile created');
                    }else{
                        $("#d_msg_e").html('Invalid mobile or user name');
                        $("#btnSubmit").attr("disabled", false);
                    }
//                    styleload();
                },
                complete: function () {
                   
                },
                error: function (data) {

                },
                timeout: 120000,
            });             
        }
</script>
   

</div>	






@stop
