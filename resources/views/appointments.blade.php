@extends('master')

@section('content')
<div class="row content" style="background: rgba(255,255,255,0.7); padding: 30px 15px;">
    <div class="col-md-3 col-sm-3 col-xs-3 search">
        <form method="get" action="{{ url('appointments') }}" id="all_programs">
            <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
            <input type="search" name="q" value="" placeholder="Search Mobile">
        </form>
    </div>    
    <div class="col-md-12 col-sm-12 col-xs-12">
        <h5>Appointments</h5>
    </div>


    <div class="col-md-12 col-sm-12 col-xs-12">
        <table width="100%" cellpadding="5" cellspacing="5" border="0" class="table table-striped">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Created at </th>
                    <th>Appointment Date and Time</th>
                    <th>Dr Name</th>
                    <th>User</th>
                    <th>Mobile</th>
                    <th>Platform</th>
                    <th>Price</th>
                    <th>Payment Status</th>
                    <th>Appointment Status</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($data as $d) { ?>
                    <?php
                    $mobile = '';
                    $name = '';
                    $assigned = $d->assigned_user_id;
                    $users = \Illuminate\Support\Facades\DB::select("select *  from users where id='{$d->assigned_user_id}'");
                    foreach ($users as $user) {
                        $mobile = $user->user_name;
                        $name = $user->first_name . ' ' . $user->last_name;
                    }
                    
                    $doc_name='';
                    $doctors = \Illuminate\Support\Facades\DB::select("select *  from contacts where id='{$d->contact_id}'");
                    foreach ($doctors as $doctor) {
                        $doc_name = $doctor->first_name . ' ' . $doctor->last_name;
                    }
                    
                    $p='';
                    
                    $prices = \Illuminate\Support\Facades\DB::select("select *  from pmnt_notify where custom_2='{$d->id}'");
                    foreach ($prices as $price) {
                         $p=$price->amount;
                    }
                    ?>
                    <tr>
                        <td>{{ $d->name }}</td>
                        <td>{{$d->date_entered}}</td>
                        <td>{{ date("Y-m-d H:i:s", strtotime('+330 minutes', strtotime($d->t_from_c))) }}</td>
                        <td>{{$doc_name}}</td>
                        <td>{{$name}}</td>
                        <td>{{$mobile}}</td>
                        <td>{{$d->app_type_c}}</td>
                        <td>{{$p}}</td>
                        <td>{{$d->payment_status}}</td>
                        <td>{{$d->status_c}}</td>
                    </tr>           
                <?php } ?>
                <tr>
                    <td colspan="10">{{ $data->links() }}</td>
                </tr>             
                <!-- popup -->
            <div class="modal fade" id="assign_prescription">
                <div class="modal-dialog modal-dialog-scrollable">
                    <div class="modal-content">

                        <!-- Modal Header -->
                        <div class="modal-header">
                            <h4 class="modal-title">Assign prescription</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>

                        <!-- Modal body -->
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-6 col-md-6 col-xs-6">User </div>
                                <div class="col-md-6 col-md-6 col-xs-6"> 
                                    <select class="form-control" id="user-list" name="user-list">
                                        <?php
                                        $users = App\User::where('role', '=', 'pharmacy_operator')->get();
                                        ?>
                                        <?php foreach ($users as $user) { ?>
                                            <option value="{{$user->id}}">{{$user->first_name}} {{$user->last_name}}</option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 col-md-12 col-xs-12"><div id="d_msg_s" class="alert-primary"></div><div id="d_msg_e" class="alert-danger"></div></div>
                            </div>

                        </div>

                        <!-- Modal footer -->
                        <div class="modal-footer">
                            <input type="hidden" name="u_id" id="u_id" class="text-field">
                            <button type="button" class="btn btn-secondary assign_user" id="btnSubmit" onclick="assign_user()">Save</button>
                        </div>

                    </div>
                </div>
            </div>

            <div class="modal fade" id="history">
                <div class="modal-dialog modal-dialog-scrollable">
                    <div class="modal-content">

                        <!-- Modal Header -->
                        <div class="modal-header">
                            <h4 class="modal-title">Status Changes</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>

                        <!-- Modal body -->
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12 col-md-12 col-xs-12"> 
                                    <div id="status-history">Loading ...</div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 col-md-12 col-xs-12"><div id="d_msg_s" class="alert-primary"></div><div id="d_msg_e" class="alert-danger"></div></div>
                            </div>

                        </div>



                    </div>
                </div>
            </div>  

            <div class="modal fade" id="status_pop">
                <div class="modal-dialog modal-dialog-scrollable">
                    <div class="modal-content">

                        <!-- Modal Header -->
                        <div class="modal-header">
                            <h4 class="modal-title">Change status</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>

                        <!-- Modal body -->
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-6 col-md-6 col-xs-6">Status </div>
                                <div class="col-md-6 col-md-6 col-xs-6"> 
                                    <select class="form-control" id="status-list" name="status-list">
                                        <option>Processing</option>
                                        <option>Delivering</option>
                                        <option>Completed</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 col-md-12 col-xs-12"><div id="d_msg_p" class="alert-primary"></div><div id="d_msg_e" class="alert-danger"></div></div>
                            </div>

                        </div>

                        <!-- Modal footer -->
                        <div class="modal-footer">
                            <input type="hidden" name="u_id" id="u_id" class="text-field">
                            <button type="button" class="btn btn-secondary status-change" id="btnSubmit" onclick="change_status()">Save</button>
                        </div>

                    </div>
                </div>
            </div>            
            <!-- end popup --> 
            </tbody>
        </table>
    </div>   

    <script>
        prescription_id = '';
        $(document).on("click", ".assign_prescription", function () {
        prescription_id = $(this).data('id');
        $("#d_msg_s").html('');
        $("#btnSubmit").attr("disabled", false);
        });
        function assign_user() {
        var u_id = $("#user-list").val();
        $("#btnSubmit").attr("disabled", true);
        var user_id = $("#user-list").val();
        var id = $("#native_id").val();        
        $.ajax({
        url: '{{ url('pharmacy/assign') }}/' + prescription_id + '/' + u_id,
                type: 'get',
                data: {prescription: prescription_id, user: u_id},
                success: function (data) {
                $("#btnSubmit").attr("disabled", false);
                if (data.toString() == 1) {
                $("#d_msg_s").html('Prescription assigned to the user.');
//                window.setTimeout(function(){location.reload()}, 3000)
                } else {
                $("#d_msg_e").html('Errors occured.');
                $("#btnSubmit").attr("disabled", false);
                }
                //                    styleload();
                },
                complete: function () {
                    $("#assigned-"+prescription_id).html($("#user-list option:selected").text());
                    $("#status-"+prescription_id).html('Assigned');
                },
                error: function (data) {

                },
                timeout: 120000,
        });
        }

        $(document).on("click", ".history_change", function () {
        prescription_id = $(this).data('id');
        $.ajax({
        url: '{{ url('pharmacy/history') }}/' + prescription_id,
                type: 'get',
                success: function (data) {
                $("#btnSubmit").attr("disabled", false);
                $("#status-history").html(data);
                },
                complete: function () {

                },
                error: function (data) {

                },
                timeout: 120000,
        });
        });
        $(document).on("click", ".status_pop", function () {
        $("#d_msg_p").html('');    
        prescription_id = $(this).data('id');
        });
        function change_status() {
        var status = $("#status-list").val();
        $("#btnSubmit").attr("disabled", true);
        $.ajax({
        url: '{{ url('pharmacy/status') }}/' + prescription_id + '/' + status,
                type: 'get',
                success: function (data) {
                $("#btnSubmit").attr("disabled", false);
                if (data.toString() == 1) {
                $("#d_msg_p").html('Status changed.');
//                window.setTimeout(function(){location.reload()}, 3000)
                } else {
                $("#d_msg_p").html('Errors occured.');
                $("#btnSubmit").attr("disabled", false);
                }
                //                    styleload();
                },
                complete: function () {
                    $("#status-"+prescription_id).html($("#status-list option:selected").text());
                },
                error: function (data) {

                },
                timeout: 120000,
        });
        }
    </script>


</div>	






@stop
