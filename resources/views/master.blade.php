<!DOCTYPE html>
<html>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <head>
        <title>{{$title}}</title>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js" ></script>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

        <!--<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
        <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js" ></script>         -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="{{ asset('css/styles.css') }}">
        <!-- data table -->
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
<!-- <script src="https://code.jquery.com/jquery-3.3.1.js"></script> -->

         <script src="https://cdn.tiny.cloud/1/bdrilyrd1r0x3nzrgf0a9neuq4y6gqmn4hthlhhavv20ublg/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>


        <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
        <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
        <script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
<style type="text/css">
    div.dataTables_wrapper {
        width: 100%;
        margin: 0 auto;
    }
  </style>
        <!-- end data table -->
    </head>

    <body>

        <div class="container-fluid">

            <div class="row header">
                <div class="col-md-3 col-sm-3 col-xs-12 logo">
                    <?php
                    $role=auth()->user()->role;
                    ?>
                    <?php if($role=='pharmacy_admin' || $role=='pharmacy_operator'){?>
                        <a href="{{ url('/pharmacy')}}"><img src="{{ asset('images/logo.png') }}" width="" alt=""></a>
                    <?php }elseif($role=='appointment_hemas' || $role=='appointment_admin'){?>
                        <a href="{{ url('/appointments')}}"><img src="{{ asset('images/logo.png') }}" width="" alt=""></a>
                    <?php }elseif($role=='onboard_admin' || $role=='onboard_operator'){?>
                        <a href="{{ url('/onboard')}}"><img src="{{ asset('images/logo.png') }}" width="" alt=""></a>                        
                    <?php }else{?>
                        <a href="{{ url('/')}}"><img src="{{ asset('images/logo.png') }}" width="" alt=""></a>
                    <?php }?>
                </div>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    <div class="logout">
                        <a href="{{ url('logout') }}"><img src="{{ asset('images/logout.png') }}" alt=""></a>
                    </div>
                    <div class="user_details">
                        Welcome <span>{{ Auth::user()->first_name }}</span>
                    </div>
                    <div class="user">
                        <img src="https://livehappy.ayubo.life/resize.php?img=/datadrive/wellness_upload/upload/user_img/{{ Auth::user()->id }}" height="50px" alt="">
                    </div>
                </div>
            </div>
            @yield('content')
        </div>
        <script>
        $(document).ready(function(){
          $("#myInput1").on("keyup", function() {
            var value = $(this).val().toLowerCase();
            $('#myDIV .img-tile').filter(function() {
              $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
          });
        });
        </script>
    </body>
</html>
