@extends('master')

@section('content')
<div class="row content">

    <div class="col-md-2 col-sm-3 col-xs-12 sidebar">
        <button onclick="$('#heading').show_me()">Heading</button>
        <button onclick="$('#subheading').show_me()">Subheading</button>
        <button>Paragraph</button>
        <button>Images</button>
        <button>Buttons</button>
        <button>List</button>
        <button>GIF</button>
        <button>Video</button>
        <button>Price</button>
        <button>Table</button>
        <button>Author</button>
        <button>Tearms & Conditions</button>
    </div>

    <div class="col-md-10 col-sm-9 col-xs-12 templates" id="work_area">

        <div class="main_block" id="heading">
            <h4>Heading</h4>
            <div class="block">
                Enter heading 
                <input type="text" name="">
            </div>
            <div class="close">
                <img src="images/delete.png" width="100%" onclick="$('#heading').hide_me()">
            </div>
        </div>

        <div class="main_block">
            <h4>Subheading</h4>
            <div class="block">
                Enter subheading 
                <input type="text" name="">
            </div>
            <div class="close">
                <a href="">
                    <img src="images/delete.png" width="100%" alt="placeholder+image"></a>
            </div>
        </div>

        <div class="main_block">
            <h4>Paragraph</h4>
            <div class="block">
                Enter paragraph 
                <textarea></textarea>
            </div>
            <div class="close">
                <a href="">
                    <img src="images/delete.png" width="100%" alt="placeholder+image"></a>
            </div>
        </div>

        <div class="main_block">
            <h4>Image</h4>
            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12 block">
                    Image type
                    <!-- radio btn -->
                    <div class="btn-group btn-group-toggle" data-toggle="buttons">
                        <label class="btn btn-secondary active">
                            <input type="radio" name="options" id="option1" autocomplete="off" checked> Full Image (1:1)
                        </label>
                        <label class="btn btn-secondary">
                            <input type="radio" name="options" id="option2" autocomplete="off"> Post Image (3:4)
                        </label>
                    </div>
                    <!-- end radio btn -->
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12 block">
                    <!-- ... -->
                </div>
            </div>
            <div class="block">
                Select image from image library
                <input type="file" name="">
                <span><img src="images/user.jpg" width="100%" alt="placeholder+image"></span>
            </div>

            <div class="close">
                <a href="">
                    <img src="images/delete.png" width="100%" alt="placeholder+image"></a>
            </div>
        </div>

        <div class="main_block">
            <h4>Video</h4>
            <div class="block">
                Select Video
                <input type="file" name="">
                <!-- <span><img src="images/user.jpg" width="100%" alt="placeholder+image"></span> -->
            </div>

            <div class="block">
                Select Video thumbnail
                <input type="file" name="">
                <span><img src="images/user.jpg" width="100%" alt="placeholder+image"></span>
            </div>

            <div class="close">
                <a href="">
                    <img src="images/delete.png" width="100%" alt="placeholder+image"></a>
            </div>
        </div>


        <div class="main_block">
            <h4>GIF</h4>
            <div class="block">
                Select image from image library
                <input type="file" name="">
                <span><img src="images/user.jpg" width="100%" alt="placeholder+image"></span>
            </div>

            <div class="close">
                <a href="">
                    <img src="images/delete.png" width="100%" alt="placeholder+image"></a>
            </div>
        </div>


        <div class="main_block">
            <h4>Price</h4>
            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12 block">
                    Currency type 
                    <select>
                        <option>LKR</option>
                        <option>USD</option>
                        <option>TK</option>
                    </select>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12 block">
                    Amount 
                    <input type="text" name="">
                </div>
            </div>

            <div class="close">
                <a href="">
                    <img src="images/delete.png" width="100%" alt="placeholder+image"></a>
            </div>
        </div>


        <div class="main_block">
            <h4>Terms & Conditions</h4>
            <div class="block">
                Page url
                <input type="text" name="">
            </div>

            <a href="">
                <div class="block">
                    <img src="images/add.png" alt="placeholder+image">
                    <label>Add buttons</label>
                </div>
            </a>

            <div class="close">
                <a href="">
                    <img src="images/delete.png" width="100%" alt="placeholder+image"></a>
            </div>
        </div>

        <div class="main_block">
            <h4>Authors</h4>
            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12 block">
                    Name
                    <input type="text" name="">
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12 block">
                    Image
                    <input type="file" name="">
                    <span><img src="images/user.jpg" width="100%" alt="placeholder+image"></span>
                </div>
            </div>

            <div class="close">
                <a href="">
                    <img src="images/delete.png" width="100%" alt="placeholder+image"></a>
            </div>
        </div>


        <div class="main_block">
            <h4>Tables</h4>
            <div class="row">
                <div class="col-md-5 col-sm-4 col-xs-12 block">
                    Number of columns 
                    <select>
                        <option selected="">0</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                    </select>
                </div>
                <div class="col-md-5 col-sm-4 col-xs-12 block">
                    Number of rows
                    <select>
                        <option selected="">0</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                    </select>
                </div>
                <div class="col-md-2 col-sm-4 col-xs-12 block">
                    <button style="position: absolute; bottom: 7px;">Generate</button>
                </div>	
            </div>

            <div class="close">
                <a href="">
                    <img src="images/delete.png" width="100%" alt="placeholder+image"></a>
            </div>
        </div>


        <div class="main_block">
            <h4>Buttons</h4>
            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12 block">
                    Label
                    <input type="text" name="">
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12 block">
                    Action
                    <select>
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                    </select>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12 block">
                    Meta
                    <select>
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                    </select>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12 block">
                    Type
                    <input type="text" name="">
                </div>
            </div>

            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12 block">
                    Status
                    <!-- radio btn -->
                    <div class="btn-group btn-group-toggle" data-toggle="buttons">
                        <label class="btn btn-secondary active">
                            <input type="radio" name="options" id="option1" autocomplete="off" checked> Active
                        </label>
                        <label class="btn btn-secondary">
                            <input type="radio" name="options" id="option2" autocomplete="off"> Disable
                        </label>
                    </div>
                    <!-- end radio btn -->
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12 block">
                    <!-- ... -->
                </div>
            </div>

            <a href="">
                <div class="block">
                    <img src="images/add.png" alt="placeholder+image">
                    <label>Add new button</label>
                </div>
            </a>

            <div class="close">
                <a href="">
                    <img src="images/delete.png" width="100%" alt="placeholder+image"></a>
            </div>
        </div>


        <div class="main_block">
            <h4>Lists</h4>
            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12 block">
                    Status
                    <!-- radio btn -->
                    <div class="btn-group btn-group-toggle" data-toggle="buttons">
                        <label class="btn btn-secondary active">
                            <input type="radio" name="options" id="option1" autocomplete="off" checked> Ordered List
                        </label>
                        <label class="btn btn-secondary">
                            <input type="radio" name="options" id="option2" autocomplete="off"> 
                            Unordered List
                        </label>
                    </div>
                    <!-- end radio btn -->
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12 block">
                    <!-- ... -->
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12 block">
                    Label
                    <input type="text" name="">
                    <a href=""><strong>Remove</strong></a>
                </div>
            </div>

            <a href="">
                <div class="block">
                    <img src="images/add.png" alt="placeholder+image">
                    <label>Add new list item</label>
                </div>
            </a>

            <div class="close">
                <a href="">
                    <img src="images/delete.png" width="100%" alt="placeholder+image"></a>
            </div>
        </div>


        <div class="footer_btn">
            <button>Preview</button>
            <button>Save</button>
        </div>

    </div>

</div>	
<script>
    $(document).ready(function () {
        $(".main_block").hide();
    });

    (function ($) {
        $.fn.show_me = function () {
            alert(this[0].outerHTML);
            $( "#inner" ).append(this.html());
//            this.show();
            return this;
        };
        $.fn.hide_me = function () {
            this.hide();
            return this;
        };        
    })(jQuery);
</script>
@stop
