@extends('master')
@section('content')
<?php $exception = Session::get('exception'); ?>
@if($exception)
<p class="alert alert-success">
 {{$exception}}
</p>
 <?php Session::put('exception',null); ?>

@endif

@if(count($errors)>0)
<ul>
@foreach($errors->all() as $error)
<li class="alert alert-danger">{{$error}}</li>
@endforeach
</ul>
@endif

<div class="row content">
    <div class="col-md-12 create_quiz_btn">

      <a class="btn btn-warning" href="{{url('/postdata/'.$id)}}">+Create Question</a>


    </div>

</div>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12 create_quiz">
<table class="table table-striped">
  <thead>
    <tr>
      <th scope="col">Id</th>
      <th scope="col">Question Name</th>
      <th scope="col">Options</th>
      <th scope="col">Type</th>
      <th scope="col">Update</th>
    </tr>
  </thead>
  <tbody>
@foreach($questions->question_relationship as $question)
    <tr>
      <td>{{$question->id}}</td>
      <td>{{$question->question}}</td>
      <td>{{$question->options}}</td>
      <td>{{$question->type}}</td>
      <td width="10%"> 
        <!-- <a href="{{url('/updatequestions/'.$question->id)}}" class="btn btn-info">Update</a>
        <a href="#" class="btn btn-danger">Delete</a> -->
        <button class="btn-success" title="Edit" onclick="window.location.href = '{{url('/updatequestions/'.$question->id)}}';"><i class="fa fa-edit"></i></button>
        <button class="btn-danger" title="Delete"><i class="fa fa-trash-o"></i></button>  
      </td>
    </tr>
@endforeach
  </tbody>
</table>

</div>
</div>

@endsection
