@extends('master')

@section('content')
<div class="row content" style="background: rgba(255,255,255,0.7); padding: 30px 15px; margin-top: 100px;">
    <div class="col-md-12 col-sm-12 col-xs-12 search">
        <form method="get" action="{{ url('all_programs') }}" id="all_programs">
            <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
            <input type="search" name="q" value="<?php echo $q; ?>" placeholder="Search Programs">
        </form>
    </div>

    <div class="col-md-12 col-sm-12 col-xs-12">
        <h5>All Programs</h5>
    </div>


<div class="col-md-2 col-sm-3 col-xs-12 program_tile create_new">
    <a href="{{ url('create_program') }}">
    <div>
        <img src="images/plus.png" alt="placeholder+image">
        <h3>Create New</h3>
    </div>
    </a>
</div>  
@foreach($programs as $program)

<div class="col-md-2 col-sm-3 col-xs-12 program_tile past_programs">
    <a href="{{ url('program_page/'.$program->id) }}">
    <div>
        <h4>{{ $program->name }}</h4>
<!--        <span>150</span>-->
    </div>
        </a>
</div>

@endforeach

   

</div>	






@stop
