
@extends('master')

@section('content')
<div class="row content" style="padding: 30px 15px; margin-top: 80px;">

    <div class="col-md-12 col-sm-12 col-xs-12 program">
        
   <div class="row">
       <div class="col-md-10 col-sm-10 col-xs-12">
           <h5>{{$program->name}}</h5>
       </div>
       <div class="col-md-2 col-sm-2 col-xs-12">
<!--           <a href="" data-toggle="modal" data-target="#myModal1">
           <span>
           <img src="{{ url('images/user.png')}}" height="30px" alt="placeholder+image"> 
           <p>100</p>
           </span>
           </a>-->

           <a href="" data-toggle="modal" data-target="#create_tile">
           <span>
           <img src="{{ url('images/man.png')}}" height="30px" alt="placeholder+image"> 
           <p>Tile</p>   
           </span>
           </a>

           <a href="" data-toggle="modal" data-target="#myModal3">
           <span>
           <img src="{{ url('images/tag.png')}}" height="30px" alt="placeholder+image"> 
           <p>Tags</p>   
           </span>
           </a>
       </div>
   </div>

   <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
          Created by:
          <label>
              @if($program->created_by_id!='')
              <?php
              $user= App\User::where('id','=',$program->created_by_id)->first();
              ?>
              {{ $user->first_name }} {{ $user->last_name }} 
              @endif
          </label>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
          Created date:
          <label>{{ $program->created_at }}</label>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
          Published on:
          <label>2020/01/20</label>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
          Published by:
          <label>User</label>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
          Tags:
          <label>@foreach($program->tags as $tag) {{$tag->tag_name}}, @endforeach</label>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
          Status: <br />
          <div class="btn-group btn-group-toggle" data-toggle="buttons">
              @if($program->hide==0)
              <label class="btn btn-warning active">
                <input type="radio" name="options" id="option1" autocomplete="off" checked> Published
              </label>
              @endif
              @if($program->hide==1)
              <label class="btn btn-warning">
                <input type="radio" name="options" id="option2" autocomplete="off"> Unpublished
              </label>
              @endif
        </div>
        </div>
   </div> 

        
        <button type="button" onclick="add_new_row()">Add Content</button>
        

    </div>



<div class="col-md-12 col-sm-12 col-xs-12 program_table">
    <form method="post" action="{{ url("/program_page/".$program->id) }}">  
    <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">    
<!--<table width="100%" border="0" cellspacing="05" cellpadding="05" class="table table-fixed">
    <thead>
        <tr>
            <th>#</th>
            <th>program_id</th>
            <th>type</th>
            <th>sequence</th>
            <th>timeline_header</th>
            <th>timeline_text</th>
            <th>timeline_image_link</th>
            <th>timeline_video_link</th>
            <th>notification_header</th>
            <th>notification_text</th>
            <th>post_time</th>
            <th>relate_id</th>
            <th>relate_text</th>
            <th>tag_name_1</th>
            <th>tag_icon_1</th>
            <th>tag_name_2</th>
            <th>tag_icon_2</th>
            <th>tag_name_3</th>
            <th>points</th>
            <th>content_icon</th>
            <th>sub_heading</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        @foreach($sequence as $s)
        <tr>
            <td><input type="hidden" name="id[]" value="{{$s->id}}"></td>
            <td>{{ $s->program_id }}</td>
            <td>
                <select name="type[]">
                    <option @if($s->type=="challenge") selected @endif>challenge</option>
                    <option @if($s->type=="chat") selected @endif>chat</option>
                    <option @if($s->type=="image") selected @endif>image</option>
                    <option @if($s->type=="native_post") selected @endif>native_post</option>
                    <option @if($s->type=="notification") selected @endif>notification</option>
                    <option @if($s->type=="question") selected @endif>question</option>
                    <option @if($s->type=="video") selected @endif>video</option>
                    <option @if($s->type=="sms") selected @endif>sms</option>
                </select>    
            </td>
            <td><textarea type="text" name="sequence[]" >{{$s->sequence}}</textarea></td>
            <td><textarea type="text" name="timeline_header[]" >{{$s->timeline_header}}</textarea></td>
            <td><textarea type="text" name="timeline_text[]" >{{$s->timeline_text}}</textarea></td>
            <td><textarea type="text" name="timeline_image_link[]" >{{$s->timeline_image_link}}</textarea></td>
            <td><textarea type="text" name="timeline_video_link[]" >{{$s->timeline_video_link}}</textarea></td>
            <td><textarea type="text" name="notification_header[]" >{{$s->notification_header}}</textarea></td>
            <td><textarea type="text" name="notification_text[]" >{{$s->notification_text}}</textarea></td>
            <td><textarea type="text" name="post_time[]" >{{$s->post_time}}</textarea></td>
            <td><textarea type="text" name="relate_id[]" >{{$s->relate_id}}</textarea></td>
            <td><textarea type="text" name="relate_text[]" >{{$s->relate_text}}</textarea></td>
            <td><textarea type="text" name="tag_name_1[]" >{{$s->tag_name_1}}</textarea></td>
            <td><textarea type="text" name="tag_icon_1[]" >{{$s->tag_icon_1}}</textarea></td>
            <td><textarea type="text" name="tag_name_2[]" >{{$s->tag_name_2}}</textarea></td>
            <td><textarea type="text" name="tag_icon_2[]" >{{$s->tag_icon_2}}</textarea></td>
            <td><textarea type="text" name="tag_name_3[]" >{{$s->tag_name_3}}</textarea></td>
            <td><textarea type="text" name="points[]" >{{$s->points}}</textarea></td>
            <td><textarea type="text" name="content_icon[]" >{{$s->content_icon}}</textarea></td>
            <td><textarea type="text" name="sub_heading[]" >{{$s->sub_heading}}</textarea></td>
            <td>
                <button class="btn-success" title="Edit"><i class="fa fa-edit"></i></button>
                <button class="btn-info" title="View"><i class="fa fa-eye"></i></button>
                <button class="btn-danger" title="Delete"><i class="fa fa-trash-o"></i></button>    
            </td>
        </tr>
    <div id="new_row"></div>
        @endforeach
    </tbody>
</table>   -->

<!-- new data table -->
<table id="example" class="display nowrap table table-fixed" style="width:100%">
        <thead>
            <tr>
            <th>#</th>
            <th>program_id</th>
            <th>action</th>
            <th>meta</th>            
            <th>sequence</th>
            <th>timeline_header</th>
            <th>timeline_text</th>
            <th>timeline_image_link</th>
            <th>timeline_video_link</th>
            <th>notification_header</th>
            <th>notification_text</th>
            <th>post_time</th>
            <th>relate_text</th>
            <th>tag_name_1</th>
            <th>tag_icon_1</th>
            <th>tag_name_2</th>
            <th>tag_icon_2</th>
            <th>tag_name_3</th>
            <th>points</th>
            <th>content_icon</th>
            <th>sub_heading</th>
            <th></th>
            </tr>
        </thead>
        <tbody>
            <?php $i=0; ?>
            @foreach($sequence as $s)
            <tr>
            <td><input type="hidden" name="id[]" value="{{$s->id}}"></td>
            <td>{{ $s->program_id }}</td>
            <td>
                <select name="type[]" id="type{{$i}}" onchange="change_related_visibility({{$i}})">
                    <option @if($s->type=="") selected @endif></option>
                    <option @if($s->type=="challenge") selected @endif>challenge</option>
                    <option @if($s->type=="chat") selected @endif>chat</option>
                    <option @if($s->type=="image") selected @endif>image</option>
                    <option @if($s->type=="native_post") selected @endif>native_post</option>
                    <option @if($s->type=="notification") selected @endif>notification</option>
                    <option @if($s->type=="question") selected @endif>question</option>
                    <option @if($s->type=="video") selected @endif>video</option>
                    <option @if($s->type=="sms") selected @endif>sms</option>
                </select>    
            </td>
            <td><textarea type="text" name="relate_id[]" id="relate_id{{$i}}">{{$s->relate_id}}</textarea><div id="relate_select_text{{$i}}"></div></td>
            <td><textarea type="text" name="sequence[]" id="sequence{{$i}}" >{{$s->sequence}}</textarea></td>
            <td><textarea type="text" name="timeline_header[]" id="timeline_header{{$i}}">{{$s->timeline_header}}</textarea></td>
            <td><textarea type="text" name="timeline_text[]" id="timeline_text{{$i}}">{{$s->timeline_text}}</textarea></td>
            <td><textarea type="text" name="timeline_image_link[]" onclick=open_dialog("timeline_image_link{{$i}}","image") id="timeline_image_link{{$i}}">{{$s->timeline_image_link}}</textarea></td>
            <td><textarea type="text" name="timeline_video_link[]" id="timeline_video_link{{$i}}" onclick=open_dialog("timeline_video_link{{$i}}","video")>{{$s->timeline_video_link}}</textarea></td>
            <td><textarea type="text" name="notification_header[]"id="notification_header{{$i}}" >{{$s->notification_header}}</textarea></td>
            <td><textarea type="text" name="notification_text[]" id="notification_text{{$i}}">{{$s->notification_text}}</textarea></td>
            <td>
                <select name="post_time[]" id="post_time{{$i}}">
                    @for($t=0 ;$t<24;$t++)
                    @for($m=0 ;$m<60;$m+=15)
                    <option @if($s->post_time==sprintf("%02d",$t).':'.sprintf("%02d",$m).':00')selected @endif>{{sprintf("%02d",$t)}}:{{sprintf("%02d",$m)}}:00</option>
                    @endfor
                    @endfor
                </select>
            </td>            
            <td><textarea type="text" name="relate_text[]" id="relate_text{{$i}}">{{$s->relate_text}}</textarea></td>
            <td><textarea type="text" name="tag_name_1[]" id="tag_name_1{{$i}}">{{$s->tag_name_1}}</textarea></td>
            <td><textarea type="text" name="tag_icon_1[]" id="tag_icon_1{{$i}}" onclick=open_dialog("tag_icon_1{{$i}}","image")>{{$s->tag_icon_1}}</textarea></td>
            <td><textarea type="text" name="tag_name_2[]" id="tag_name_2{{$i}}">{{$s->tag_name_2}}</textarea></td>
            <td><textarea type="text" name="tag_icon_2[]" id="tag_icon_2{{$i}}" onclick=open_dialog("tag_icon_2{{$i}}","image")>{{$s->tag_icon_2}}</textarea></td>
            <td><textarea type="text" name="tag_name_3[]" id="tag_name_3{{$i}}">{{$s->tag_name_3}}</textarea></td>
            <td><textarea type="text" name="points[]" id="points{{$i}}">{{$s->points}}</textarea></td>
            <td><textarea type="text" name="content_icon[]" id="content_icon{{$i}}" onclick=open_dialog("content_icon{{$i}}","image")>{{$s->content_icon}}</textarea></td>
            <td><textarea type="text" name="sub_heading[]" id="sub_heading{{$i}}">{{$s->sub_heading}}</textarea></td>
            <td>
<!--                <button class="btn-success" title="Edit"><i class="fa fa-edit"></i></button>
                <button class="btn-info" title="View"><i class="fa fa-eye"></i></button>-->
                <button class="btn-danger" title="Delete" type="button" onclick="del_record({{$s->id}})"><i class="fa fa-trash-o"></i></button>    
            </td>
            </tr>
            <?php $i++; ?>
            @endforeach
        </tbody>
    </table>
<!-- end new data table -->

</div>

    <button type="button" onclick="add_new_row()">Add new</button>&nbsp;   
 <button type="submit">Save</button>   
 </form>  
</div>	

<!-- Communities modal -->
<!-- The Modal -->
  <div class="modal fade" id="myModal1">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Add Communities</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
          <input type="search" name="" placeholder="Search Community">

         
              <div class="col-md-12 col-sm-12 col-xs-12 tag_row">
                  Hemas Holdings
                  <span><i class="fa fa-close" style="font-size:18px"></i></span>
              </div>
              <div class="col-md-12 col-sm-12 col-xs-12 tag_row">
                  HDFC Bank
                  <span><i class="fa fa-close" style="font-size:18px"></i></span>
              </div>
              <div class="col-md-12 col-sm-12 col-xs-12 tag_row">
                  All Users
                  <span><i class="fa fa-close" style="font-size:18px"></i></span>
              </div>
        

        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
        
      </div>
    </div>
  </div>
<!-- End Communities modal  -->


<!-- Users modal -->
<!-- The Modal -->
  <div class="modal fade" id="myModal2">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Add Individual Users </h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
          <input type="search" name="" placeholder="Search Community">

         
              <div class="col-md-12 col-sm-12 col-xs-12 tag_row">
                  Subash - 0772440064
                  <span><i class="fa fa-close" style="font-size:18px"></i></span>
              </div>
              <div class="col-md-12 col-sm-12 col-xs-12 tag_row">
                  John Doe - 0772440064
                  <span><i class="fa fa-close" style="font-size:18px"></i></span>
              </div>
              <div class="col-md-12 col-sm-12 col-xs-12 tag_row">
                  Jain Doe - 0772440064
                  <span><i class="fa fa-close" style="font-size:18px"></i></span>
              </div>
        

        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
        
      </div>
    </div>
  </div>
<!-- End Users modal  -->



<!-- Tags modal -->
<!-- The Modal -->
  <div class="modal fade" id="myModal3">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Add Tags</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <form method="post" action="{{ url('/tags/save/'.$program->id) }}">
        <!-- Modal body -->
        <div class="modal-body">
            
            <?php
            $my_tags=[];
            foreach($program->tags as $tag){
                $my_tags[]=$tag->id;
            }                
            ?>
            <select name="tags[]" class="tags" multiple="multiple">
                @foreach($tags as $tag)  
                <option value="{{$tag->id}}" <?php if(in_array($tag->id, $my_tags)){?> selected<?php } ?>>{{$tag->tag_name}}</option>
                @endforeach
            </select>

         @foreach($program->tags as $tag)  
              <div class="col-md-12 col-sm-12 col-xs-12 tag_row">
                  {{$tag->tag_name}}
<!--                  <span><i class="fa fa-close" style="font-size:16px"></i></span>-->
              </div>
         @endforeach
        
         
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
            <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
          <button type="submit" class="btn btn-secondary">Save</button>
        </div>
        </form>
      </div>
    </div>
  </div>
    <!-- popup -->
    <div class="modal fade" id="create_tile">
    <div class="modal-dialog modal-dialog-scrollable">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Create Tile</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
            <div class="row">
                <div class="col-md-6 col-md-6 col-xs-6">Mobile </div>
                <div class="col-md-6 col-md-6 col-xs-6"><input type="text" name="mobile" id="mobile" class="text-field"> </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-md-12 col-xs-12"><div id="d_msg_s" class="alert-primary"></div><div id="d_msg_e" class="alert-danger"></div></div>
            </div>

        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
<!--            <input type="hidden" name="program_id" id="program_id" class="text-field">-->
            <button type="button" class="btn btn-secondary" id="btnSubmit" onclick="np_create_tile()">Save</button>
        </div>
        
      </div>
    </div>
  </div>
<!-- Tags modal  -->
    <!-- popup -->
    <div class="modal fade" id="myModal">
    <div class="modal-dialog modal-xl modal-dialog-scrollable">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Select Image</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">          
            <div id="gallery-images"></div>
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary">Insert</button>
        </div>
        
      </div>
    </div>
  </div>
    <!-- end popup -->
<script>
    
    function add_new_row(){
        location.href="{{ url('add_empty_row/'.$program->id) }}";
    }
</script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#example').DataTable( {
        "scrollX": true
    } );
} );

function change_related_visibility(id){
    var selected_type=$( "#type"+id ).val();
    if(selected_type=='challenge'){
//        $( "#post_time"+id ).hide();
        $( "#relate_select_text"+id).html('Enter challenge id');
    }else if(selected_type=='native_post'){
        select_native_post(id);
    }else if(selected_type=='question'){
        select_question(id);
    }else if(selected_type=='chat'){
        select_chat(id);
    }
}


       function select_native_post(id){          
             $.ajax({
                url: '{{ url('list/native_post') }}',
                type: 'get',
                success: function (data) {
                    data="<select class='native' onchange=change_val("+id+",this)>"+data+"</select>";
                    $('#relate_select_text'+id).html(data);
                },
                complete: function () {
                   $('.native').select2();
                },
                error: function (data) {
                },
                timeout: 120000,
            });           
        }
       function select_question(id){          
             $.ajax({
                url: '{{ url('list/select_question') }}',
                type: 'get',
                success: function (data) {
                    data="<select class='quiz' onchange=change_val("+id+",this)>"+data+"</select>";
                    $('#relate_select_text'+id).html(data);
                },
                complete: function () {
                   $('.quiz').select2();
                },
                error: function (data) {
                },
                timeout: 120000,
            });           
        }
       function select_chat(id){          
             $.ajax({
                url: '{{ url('list/chat') }}',
                type: 'get',
                success: function (data) {
                    data="<select class='chat' onchange=change_val("+id+",this)>"+data+"</select>";
                    $('#relate_select_text'+id).html(data);
                },
                complete: function () {
                   $('.chat').select2();
                },
                error: function (data) {
                },
                timeout: 120000,
            });           
        }        
function change_val(id,obj){
    var sel=$(obj).val();
    $('#relate_id'+id).text(sel);
}

   function open_dialog(ref,type){
       image_type=type;
       selected_obaject=ref;
       jQuery("#myModal").modal('show');         
    }
       $('#myModal').on('show.bs.modal', function () {
            load_gallery_images();
        }); 
         function load_gallery_images(folder){ 
            folder = typeof folder !== 'undefined' ? folder : '';
            url='{{ url('image_library/popup/') }}';
            if(folder!=''){
                url='{{ url('image_library/popup') }}'+'/'+folder;
            }
             $.ajax({
                url: url,
                type: 'get',
                data:{"image_type":image_type},
                success: function (data) {
                    $('#gallery-images').html(data);
//                    styleload();
                },
                complete: function () {
                   
                },
                error: function (data) {

                },
                timeout: 120000,
            });           
        }
        
   function insert_url(url){
       $("#"+selected_obaject).val(url);
       $("#"+selected_obaject+"-thumb").attr("src",url);
       $('#myModal').fadeOut();
       $('#myModal').modal('hide');
    }  
    
    function del_record(id){
        var sure=confirm('Delete record?');
        if(sure){
            location.href="{{ url('del_row/'.$program->id) }}/"+id;
        }
    }
    
$(document).ready(function() {
//    $('.chat').select2();
//    $('.native').select2();
//    $('.tags').select2();
}); 

        function np_create_tile(){
            $("#d_msg_s").html('');
            $("#d_msg_e").html('');
            $("#btnSubmit").attr("disabled", true);
            var mobile=$("#mobile").val();
            var id="{{$program->id}}";
             $.ajax({
                url: '{{ url('dynamic_template/create_tile_program') }}',
                type: 'get',
                data:{mobile:mobile,id:id},
                success: function (data) {
                    if(data.toString()==1){
                        $("#d_msg_s").html('Tile created');
                    }else{
                        $("#d_msg_e").html('Invalid mobile or user name');
                        $("#btnSubmit").attr("disabled", false);
                    }
//                    styleload();
                },
                complete: function () {
                   
                },
                error: function (data) {

                },
                timeout: 120000,
            });             
        }
</script> 

@stop
