@extends('master')

@section('content')
<div class="row content" style="background: rgba(255,255,255,0.7); padding: 30px 15px;">
    <div class="col-md-3 col-sm-3 col-xs-3 search">
        <form method="get" action="{{ url('pharmacy') }}" id="all_programs">
            <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
            <input type="search" name="q" value="" placeholder="Search Mobile">
        </form>
    </div>    
    <div class="col-md-12 col-sm-12 col-xs-12">
        <h5>Prescriptions received</h5>
    </div>


    <div class="col-md-12 col-sm-12 col-xs-12">
        <table width="100%" cellpadding="5" cellspacing="5" border="0" class="table table-striped">
            <thead>
                <tr>
                    <th>Date</th>
                    <th>Mobile</th>
                    <th>Name</th>
                    <th>Address</th>
                    <th>Payment Type</th>
                    <th>Prescription</th>
                    <th>Assigned</th>
                    <th>Status</th>
                    <th>History</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($data as $d) { ?>
                    <?php
                    $mobile = '';
                    $name = '';
                    $assigned = $d->assigned_user_id;
                    $users = \Illuminate\Support\Facades\DB::select("select *  from users where id='{$d->name}'");
                    foreach ($users as $user) {
                        $mobile = $user->user_name;
                        $name = $user->first_name . ' ' . $user->last_name;
                    }
                    $documents = \Illuminate\Support\Facades\DB::select("select *  from pc_external_prescription_documents_1_c where pc_external_prescription_documents_1pc_external_prescription_ida='{$d->id}'");
                    $doc_id='xyz';
                    foreach ($documents as $document) {
                        $doc_id = $document->pc_external_prescription_documents_1documents_idb;
                    }
                    $reviews = \Illuminate\Support\Facades\DB::select("select *  from documents where id='{$doc_id}'");
                    $rev_id='';
                    foreach ($reviews as $review) {
                        $rev_id = $review->document_revision_id;
                    }
                    $assigned_user = App\User::where('id', '=', $assigned)->first();
                    $t=date('Y-m-d H:i:s',strtotime('+330 minutes', strtotime($d->date_entered)));
                    ?>
                    <tr>
                        <td>{{ $t }}</td>
                        <td>{{$mobile}}</td>
                        <td>{{$name}}</td>
                        <td>{{$d->shipping_address_c}}</td>
                        <td>{{strtoupper($d->payment_type_c)}}</td>
                        <td>
                            @if(empty($d->description))
                                <a href="https://livehappy.ayubo.life/resize.php?img=/datadrive/wellness_upload/upload/{{$rev_id}}" target="_blank"><img width="100" src='https://livehappy.ayubo.life/resize.php?img=/datadrive/wellness_upload/upload/{{$rev_id}}'></a>
                            @else
                            <a href="{{$d->description}}" target="_blank"><img src='{{asset("images/prescription.jpg")}}'></a>
                            @endif
                        </td>
                        <td style="text-align: center"><span id='assigned-{{$d->id}}'>{{$assigned_user->first_name}} {{$assigned_user->last_name}}</span>
                            <br>
                            <?php if (auth()->user()->role == 'pharmacy_admin') { ?>
                                <button type="submit" class="btn btn-success assign_prescription" data-toggle="modal" data-target="#assign_prescription" data-id="{{$d->id}}">Change</button>
                            <?php } ?>
                        </td>
                        <td style="text-align: center"><span id='status-{{$d->id}}'>{{$d->status}}</span>
                            <br><button type="submit" class="btn btn-success status_pop" data-toggle="modal" data-target="#status_pop" data-id="{{$d->id}}">Change</button>
                        </td>
                        <td><br><button type="submit" class="btn btn-success history_change" data-toggle="modal" data-target="#history" data-id="{{$d->id}}">View</button></td>

                    </tr>           
                <?php } ?>
                <tr>
                    <td colspan="9">{{ $data->links() }}</td>
                </tr>             
                <!-- popup -->
            <div class="modal fade" id="assign_prescription">
                <div class="modal-dialog modal-dialog-scrollable">
                    <div class="modal-content">

                        <!-- Modal Header -->
                        <div class="modal-header">
                            <h4 class="modal-title">Assign prescription</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>

                        <!-- Modal body -->
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-6 col-md-6 col-xs-6">User </div>
                                <div class="col-md-6 col-md-6 col-xs-6"> 
                                    <select class="form-control" id="user-list" name="user-list">
                                        <?php
                                        $users = App\User::where('role', '=', 'pharmacy_operator')->get();
                                        ?>
                                        <?php foreach ($users as $user) { ?>
                                            <option value="{{$user->id}}">{{$user->first_name}} {{$user->last_name}}</option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 col-md-12 col-xs-12"><div id="d_msg_s" class="alert-primary"></div><div id="d_msg_e" class="alert-danger"></div></div>
                            </div>

                        </div>

                        <!-- Modal footer -->
                        <div class="modal-footer">
                            <input type="hidden" name="u_id" id="u_id" class="text-field">
                            <button type="button" class="btn btn-secondary assign_user" id="btnSubmit" onclick="assign_user()">Save</button>
                        </div>

                    </div>
                </div>
            </div>

            <div class="modal fade" id="history">
                <div class="modal-dialog modal-dialog-scrollable">
                    <div class="modal-content">

                        <!-- Modal Header -->
                        <div class="modal-header">
                            <h4 class="modal-title">Status Changes</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>

                        <!-- Modal body -->
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12 col-md-12 col-xs-12"> 
                                    <div id="status-history">Loading ...</div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 col-md-12 col-xs-12"><div id="d_msg_s" class="alert-primary"></div><div id="d_msg_e" class="alert-danger"></div></div>
                            </div>

                        </div>



                    </div>
                </div>
            </div>  

            <div class="modal fade" id="status_pop">
                <div class="modal-dialog modal-dialog-scrollable">
                    <div class="modal-content">

                        <!-- Modal Header -->
                        <div class="modal-header">
                            <h4 class="modal-title">Change status</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>

                        <!-- Modal body -->
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-6 col-md-6 col-xs-6">Status </div>
                                <div class="col-md-6 col-md-6 col-xs-6"> 
                                    <select class="form-control" id="status-list" name="status-list">
                                        <option>Processing</option>
                                        <option>Delivering</option>
                                        <option>Completed</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 col-md-12 col-xs-12"><div id="d_msg_p" class="alert-primary"></div><div id="d_msg_e" class="alert-danger"></div></div>
                            </div>

                        </div>

                        <!-- Modal footer -->
                        <div class="modal-footer">
                            <input type="hidden" name="u_id" id="u_id" class="text-field">
                            <button type="button" class="btn btn-secondary status-change" id="btnSubmit" onclick="change_status()">Save</button>
                        </div>

                    </div>
                </div>
            </div>            
            <!-- end popup --> 
            </tbody>
        </table>
    </div>   

    <script>
        prescription_id = '';
        $(document).on("click", ".assign_prescription", function () {
        prescription_id = $(this).data('id');
        $("#d_msg_s").html('');
        $("#btnSubmit").attr("disabled", false);
        });
        function assign_user() {
        var u_id = $("#user-list").val();
        $("#btnSubmit").attr("disabled", true);
        var user_id = $("#user-list").val();
        var id = $("#native_id").val();        
        $.ajax({
        url: '{{ url('pharmacy/assign') }}/' + prescription_id + '/' + u_id,
                type: 'get',
                data: {prescription: prescription_id, user: u_id},
                success: function (data) {
                $("#btnSubmit").attr("disabled", false);
                if (data.toString() == 1) {
                $("#d_msg_s").html('Prescription assigned to the user.');
//                window.setTimeout(function(){location.reload()}, 3000)
                } else {
                $("#d_msg_e").html('Errors occured.');
                $("#btnSubmit").attr("disabled", false);
                }
                //                    styleload();
                },
                complete: function () {
                    $("#assigned-"+prescription_id).html($("#user-list option:selected").text());
                    $("#status-"+prescription_id).html('Assigned');
                },
                error: function (data) {

                },
                timeout: 120000,
        });
        }

        $(document).on("click", ".history_change", function () {
        prescription_id = $(this).data('id');
        $.ajax({
        url: '{{ url('pharmacy/history') }}/' + prescription_id,
                type: 'get',
                success: function (data) {
                $("#btnSubmit").attr("disabled", false);
                $("#status-history").html(data);
                },
                complete: function () {

                },
                error: function (data) {

                },
                timeout: 120000,
        });
        });
        $(document).on("click", ".status_pop", function () {
        $("#d_msg_p").html('');    
        prescription_id = $(this).data('id');
        });
        function change_status() {
        var status = $("#status-list").val();
        $("#btnSubmit").attr("disabled", true);
        $.ajax({
        url: '{{ url('pharmacy/status') }}/' + prescription_id + '/' + status,
                type: 'get',
                success: function (data) {
                $("#btnSubmit").attr("disabled", false);
                if (data.toString() == 1) {
                $("#d_msg_p").html('Status changed.');
//                window.setTimeout(function(){location.reload()}, 3000)
                } else {
                $("#d_msg_p").html('Errors occured.');
                $("#btnSubmit").attr("disabled", false);
                }
                //                    styleload();
                },
                complete: function () {
                    $("#status-"+prescription_id).html($("#status-list option:selected").text());
                },
                error: function (data) {

                },
                timeout: 120000,
        });
        }
    </script>


</div>	






@stop
