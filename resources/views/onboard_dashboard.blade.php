
@extends('master')

@section('content')
<div class="row content">
    <div class="col-md-2 col-sm-3 col-xs-12 home_card">
      <a href="{{ url('onboard/create')}}"><div>
            <img src="{{ asset('images/dyamic_template.png') }}" width="" alt="placeholder+image">
            <h5> Expert<br />Create</h5>
        </div></a>
    </div>

    <div class="col-md-2 col-sm-3 col-xs-12 home_card">
        <a href="{{ url('onboard/list')}}"><div>
            <img src="{{ asset('images/manage.png') }}" width="" alt="placeholder+image">
            <h5>Expert<br />List</h5>
        </div></a>
    </div>
    
    <div class="col-md-2 col-sm-3 col-xs-12 home_card">
        <a href="{{ url('onboard/transactions')}}"><div>
            <img src="{{ asset('images/manage.png') }}" width="" alt="placeholder+image">
            <h5>Transactions<br />List</h5>
        </div></a>
    </div> 
    
    <div class="col-md-2 col-sm-3 col-xs-12 home_card">
        <a href="{{ url('onboard/payments')}}"><div>
            <img src="{{ asset('images/manage.png') }}" width="" alt="placeholder+image">
            <h5>Old<br />Payments</h5>
        </div></a>
    </div> 

    <div class="col-md-2 col-sm-3 col-xs-12 home_card">
        <a href="{{ url('onboard/sessions')}}"><div>
            <img src="{{ asset('images/manage.png') }}" width="" alt="placeholder+image">
            <h5>Session<br />Create</h5>
        </div></a>
    </div> 
    <div class="col-md-2 col-sm-3 col-xs-12 home_card">
        <a href="{{ url('onboard/price')}}"><div>
            <img src="{{ asset('images/manage.png') }}" width="" alt="placeholder+image">
            <h5>Price<br />Add</h5>
        </div></a>
    </div>       
</div>
@stop

<!-- <section id="section-feature" class="container">
 <div class="row">
     <ul>
       <li  class="col-md-4 col-sm-6 col-xs-12">
         <div class="card">
            <a href="{{ url('/home') }}">
               <div class="sf-icon">
                   <i class="fa fa-question-circle fa-5x"></i>
                   </a>
                     <a href="{{ url('/home') }}">
                   <h3>Questionnaire</h3>
                   </a>
               </div>
                  </div>
         </li>


         <li class="col-md-6 col-sm-6 col-xs-12">
               <div class="card">
                 <a  href="{{ url('/dashboard') }}">
               <div class="sf-icon">
                   <i class="fa fa-tachometer fa-5x"></i>
                   </a>
                   <a href="{{ url('/dashboard') }}">
                   <h3>Dash Board</h3>
                   </a>
                   </div>
               </div>
         </li>

     </ul>
 </div>
</section> -->
