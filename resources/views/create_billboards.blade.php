@extends('master')

@section('content')
<div class="row content">
<div class="col-md-12 col-sm-12 col-xs-12 templates" >
  <form method="post" action="{{ url('create_billboard_vendors') }}" id="template_form">
      <div id="response">
      @if (session()->has('success'))
      <div class="alert alert-success" id="popup_notification">
          <strong>{{ session('success') }}</strong>
      </div>
      @endif
      @if ($errors->any())
      <div class="alert alert-danger">
          <ul>
              @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div>
      @endif
      </div>


      <div class="main_block">
      <button class="btn-success" title="Edit" onclick="window.location.href = 'billboard_table';">Bilboard Table <i class="fa fa-edit"></i></button>
      <h4>Create Billboard Adds</h4>
      <div class="block">
      Enter name
      <input type="text" name="name" class="text-field" required>
      </div>

      <div class="block">
      Select image from image library
      <input type="text" value="" name="image" id="image_url" onclick=open_dialog("image_url") class="text-field">
      </div>

      <div class="block">
      Select logo from image library
      <input type="text" value="" name="logo" id="logo_url" onclick=open_dialog("logo_url") class="text-field">
      </div>

      <div class="block">
      Select Banner from image library
      <input type="text" value="" name="banner" id="banner_url" onclick=open_dialog("banner_url") class="text-field">
      </div>

      <div class="block">
      Select treasure Image from image library
      <input type="text" value="" name="image_treasure" id="treasure_url" onclick=open_dialog("treasure_url") class="text-field">
      </div>

      <div class="block">
      Select Action Type
      <select name="action" id="challenge_id" class="form-control" required>
      <option value="chanelling">Chanelling</option>
      <option value="Web">Web</option>
      </select>
      </div>

      <div class="block">
      Enter Meta
      <input type="text" name="link" class="text-field">
      </div>



      </div>
      <div class="footer_btn">
        <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
          <button type="submit">Create</button>
      </div>

    </form>
</div>
</div>

<!-- popup -->
    <div class="modal fade" id="myModal">
    <div class="modal-dialog modal-xl modal-dialog-scrollable">
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Select Image</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>

        <!-- Modal body -->
        <div class="modal-body">
        <div><input type="search" id="myInput1" name="myInput1" placeholder="Search"></div>
        <div id="myDIV" class="row">
            <div id="gallery-images"></div>
        </div>
        </div>

        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary">Insert</button>
        </div>

      </div>
    </div>
  </div>
    <!-- end popup -->
<script>
var counter = 1;

</script>


<script>


$('#myModal').on('show.bs.modal', function () {
         load_gallery_images();
     });

     function load_gallery_images(folder){
         folder = typeof folder !== 'undefined' ? folder : '';
         url='{{ url('image_library/popup/') }}';
         if(folder!=''){
             url='{{ url('image_library/popup') }}'+'/'+folder;
         }
          $.ajax({
             url: url,
             type: 'get',
             data:{"image_type":image_type},
             success: function (data) {
                 $('#gallery-images').html(data);
//                    styleload();
             },
             complete: function () {

             },
             error: function (data) {

             },
             timeout: 120000,
         });
     }

selected_obaject='';
function open_dialog(ref,type){
  image_type=type;
  selected_obaject=ref;
  jQuery("#myModal").modal('show');
}
function insert_url(url){
  $("#"+selected_obaject).val(url);
  $("#"+selected_obaject+"-thumb").attr("src",url);
  $('#myModal').fadeOut();
  $('#myModal').modal('hide');
}
</script>
@stop
