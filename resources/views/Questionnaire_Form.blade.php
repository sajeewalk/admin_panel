@extends('master')

@section('content')

        <div class="row content">

        <div class="col-md-12 col-sm-12 col-xs-12 templates" >
        <form class="forms-sample" method="post" action="{{URL::to('/save_data')}}" enctype="multipart/form-data">
        <h3 class="">Questionnaire Form</h3>
        <div id="response">
        <?php $exception = Session::get('exception'); ?>
        @if($exception)
        <p class="alert alert-success">
        {{$exception}}
        </p>
        <?php Session::put('exception',null); ?>

          @endif
          @if(count($errors)>0)
          <ul>
          @foreach($errors->all() as $error)
          <li class="alert alert-danger">{{$error}}</li>
          @endforeach
          </ul>
          @endif
          </div>
          {{csrf_field()}}
          <div class="container">
          <div class="row">
          <div class="col-md-4">
          <div class="main_block">
          <h4>Question Name</h4>
          <div class="block">
          Enter name
          <input type="text" name="name" class="text-field" required>
          </div>
          </div>

          <div class="main_block">
          <h4>Description</h4>
          <div class="block">
          Enter Description
          <input type="text" name="discription" class="text-field">
          </div>
          </div>


          <div class="main_block">
          <h4>Group Introduction</h4>
          <select name="gr_intro" class="form-control">
          <option value="1">Show</option>
          <option value="0">Hide</option>
          </select>
          <!-- <div class="bs-example">
          <div class="btn-group btn-group-toggle" data-toggle="buttons">
          <label class="btn btn-secondary active">
          <input type="radio" value="1" name="gr_intro" selected> Show
          </label>
          <label class="btn btn-secondary">
          <input type="radio" value="0" name="gr_intro"> Hide
          </label>
          </div>
          </div> -->
          </div>

          <div class="main_block">
          <h4>Introduction title</h4>
          <div class="block">
          Enter Introduction title
          <input type="text" class="form-control p-input" name="Introduction_title"  value="">
          </div>
          </div>

          <div class="main_block">
          <h4>Introduction Text</h4>
          <div class="block">
          Enter Introduction Text
          <textarea  class="form-control p-input" name="Introduction_Text"  id="comment" rows="5" value=""></textarea>
          </div>
          </div>


          </div>

          <div class="col-md-4">




          <div class="main_block">
          <h4>Intro Image</h4>
          <div class="block">
          Select image from image library
          <input type="text" name="intro_image" id="image_url" onclick=open_dialog("image_url") class="text-field">
          </div>
          </div>

          <div class="main_block">
          <h4>Intro Video</h4>
          <div class="block">
          Select Video
          <input type="text" name="intro_video" id="-video-full" onclick=open_dialog("-video-full","video") class="text-field">
          </div>
          </div>

          <div class="main_block">
          <h4>Group Ending</h4>
          <select name="group_ending" class="form-control">
          <option value="1">Show</option>
          <option value="0">Hide</option>
          </select>
          </div>

          <div class="main_block">
          <h4>Ending Heading</h4>
          <div class="block">
          Enter Ending Heading
          <input type="text" class="form-control p-input" value="" name="ending_heading">
          </div>
          </div>

          <div class="main_block">
          <h4>Ending Text</h4>
          <div class="block">
          Enter Ending Text
          <textarea  class="form-control p-input" name="ending_text"   id="comment" rows="5" value=""></textarea>
          </div>
          </div>

          </div>


          <div class="col-md-4">

          <div class="main_block">
          <h4>Ending Image</h4>
          <div class="block">
          Select image from image library
          <input type="text" name="ending_image" id="endingimage_url" onclick=open_dialog("endingimage_url") class="text-field">
          </div>
          </div>

          <div class="main_block">
          <h4>Back Enabled</h4>
          <select name="back_enabled" class="form-control">
          <option value="1">Yes</option>
          <option value="0">No</option>
          </select>
          </div>

          <div class="main_block">
          <h4>Action</h4>
          <div class="block">
          Enter Action
          <input type="text" class="form-control p-input" name="action"  value="">
          </div>
          </div>

          <div class="main_block">
          <h4>Meta ID</h4>
          <div class="block">
          Enter Meta ID
          <input type="text" class="form-control p-input" name="meta"  value="">
          </div>
          </div>

         <button type="submit" class="btn btn-success btn-block">Save</button>
          </div>


          </div>
          </div>


          </form>
          </div>


    <!-- popup -->
    <div class="modal fade" id="myModal">
    <div class="modal-dialog modal-xl modal-dialog-scrollable">
    <div class="modal-content">

    <!-- Modal Header -->
    <div class="modal-header">
    <h4 class="modal-title">Select Image</h4>
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    </div>

    <!-- Modal body -->
    <div class="modal-body">
    <div id="gallery-images"></div>
    </div>

    <!-- Modal footer -->
    <div class="modal-footer">
    <button type="button" class="btn btn-secondary">Insert</button>
    </div>

    </div>
    </div>
    </div>
    <!-- end popup -->

</div>

<script>


    $('#work_area').on('click', '.remove', function () {
        $(this).parent().parent().remove();
        counter++;
    });

       $('#myModal').on('show.bs.modal', function () {
            load_gallery_images();
        });



        function load_gallery_images(folder){
            folder = typeof folder !== 'undefined' ? folder : '';
            url='{{ url('image_library/popup/') }}';
            if(folder!=''){
                url='{{ url('image_library/popup') }}'+'/'+folder;
            }
             $.ajax({
                url: url,
                type: 'get',
                data:{"image_type":image_type},
                success: function (data) {
                    $('#gallery-images').html(data);
//                    styleload();
                },
                complete: function () {

                },
                error: function (data) {

                },
                timeout: 120000,
            });
        }
   selected_obaject='';
   function open_dialog(ref,type){
       image_type=type;
       selected_obaject=ref;
       jQuery("#myModal").modal('show');
    }
   function insert_url(url){
       $("#"+selected_obaject).val(url);
       $("#"+selected_obaject+"-thumb").attr("src",url);
       $('#myModal').fadeOut();
       $('#myModal').modal('hide');
    }
</script>



@stop
