@extends('master')

@section('content')
<div class="row content">
    <div class="col-md-2 col-sm-3 col-xs-12 home_card">
      <a href="{{ url('quiz_welcome')}}"><div>
            <img src="{{ asset('images/questions.png') }}" width="" alt="placeholder+image">
            <h5>Questionnaire <br />& Assessments</h5>
        </div></a>
    </div>

    <div class="col-md-2 col-sm-3 col-xs-12 home_card">
        <a href="{{ url('dynamic_template_welcome')}}"><div>
            <img src="{{ asset('images/dyamic_template.png') }}" width="" alt="placeholder+image">
            <h5>Dynamic<br />Templates</h5>
        </div></a>
    </div>
    <div class="col-md-2 col-sm-3 col-xs-12 home_card">
        <a href="{{ url('all_programs')}}"><div>
            <img src="{{ asset('images/dyamic_template.png') }}" width="" alt="placeholder+image">
            <h5>Create<br />Programs</h5>
        </div></a>
    </div>
    <div class="col-md-2 col-sm-3 col-xs-12 home_card">
        <a href="{{ url('image_library')}}"><div>
            <img src="{{ asset('images/media_gallery.png') }}" width="" alt="placeholder+image">
            <h5>Media<br />Gallery</h5>
        </div></a>
    </div>

    <div class="col-md-2 col-sm-3 col-xs-12 home_card">
      <a href="{{ url('challenge_dashboad')}}"><div>
            <img src="{{ asset('images/challenge.png') }}" width="" alt="placeholder+image">
            <h5>Step<br /> Challenge</h5>
        </div></a>
    </div>
<!--    <div class="col-md-2 col-sm-3 col-xs-12 home_card">
        <a href="{{ url('nativepost_table')}}"><div>
            <img src="{{ asset('images/manage.png') }}" width="" alt="placeholder+image">
            <h5>Manage<br />Native Post</h5>
        </div></a>
    </div>-->
<!--    <div class="col-md-2 col-sm-3 col-xs-12 home_card">
        <a href=" "><div>
            <img src="{{ asset('images/manage.png') }}" width="" alt="placeholder+image">
            <h5>Lorem <br />Ipsum</h5>
        </div></a>
    </div>-->
</div>
@stop
