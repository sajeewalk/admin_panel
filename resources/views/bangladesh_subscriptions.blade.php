@extends('master')

@section('content')
<div class="row content" style="background: rgba(255,255,255,0.7); padding: 30px 15px;">
    <div class="col-md-10 col-sm-10 col-xs-12">
        <h5>Subscriptions</h5>
    </div>

    <div class="col-md-12 col-sm-12 col-xs-12">
        <table width="100%" cellpadding="5" cellspacing="5" border="0" class="table table-striped">
            <thead>
                <tr>
                    <th>name</th>
                    <th>Age</th>
                    <th>user_gender_c</th>
                    <th>user_name</th>
                    <th>ban_device_id_c</th>
                    <th>date_entered</th>
                    <th>email_address</th>
                    <th>user_nic_c</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($data as $d) { ?>                 
                    <tr>
                        <td>{{ $d->first_name }} {{ $d->last_name }}</td>
                        <td>{{$d->user_dob_c}}</td>                       
                        <td>{{$d->user_gender_c}}</td>
                        <td>{{$d->user_name}}</td>
                        <td>{{$d->ban_device_id_c}}</td>
                        <td>{{$d->date_entered}}</td> 
                        <td>{{$d->email_address}}</td> 
                        <td>{{$d->user_nic_c}}</td> 
                    </tr>           
                <?php } ?>
                <tr>
                    <td colspan="10">{{ $data->links() }}</td>
                </tr>             
                <!-- popup -->
            <div class="modal fade" id="assign_prescription">
                <div class="modal-dialog modal-dialog-scrollable">
                    <div class="modal-content">

                        <!-- Modal Header -->
                        <div class="modal-header">
                            <h4 class="modal-title">Assign prescription</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>

                        <!-- Modal body -->
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-6 col-md-6 col-xs-6">User </div>
                                <div class="col-md-6 col-md-6 col-xs-6"> 
                                    <select class="form-control" id="user-list" name="user-list">
                                        <?php
                                        $users = App\User::where('role', '=', 'pharmacy_operator')->get();
                                        ?>
                                        <?php foreach ($users as $user) { ?>
                                            <option value="{{$user->id}}">{{$user->first_name}} {{$user->last_name}}</option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 col-md-12 col-xs-12"><div id="d_msg_s" class="alert-primary"></div><div id="d_msg_e" class="alert-danger"></div></div>
                            </div>

                        </div>

                        <!-- Modal footer -->
                        <div class="modal-footer">
                            <input type="hidden" name="u_id" id="u_id" class="text-field">
                            <button type="button" class="btn btn-secondary assign_user" id="btnSubmit" onclick="assign_user()">Save</button>
                        </div>

                    </div>
                </div>
            </div>

            <div class="modal fade" id="history">
                <div class="modal-dialog modal-dialog-scrollable">
                    <div class="modal-content">

                        <!-- Modal Header -->
                        <div class="modal-header">
                            <h4 class="modal-title">Status Changes</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>

                        <!-- Modal body -->
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12 col-md-12 col-xs-12"> 
                                    <div id="status-history">Loading ...</div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 col-md-12 col-xs-12"><div id="d_msg_s" class="alert-primary"></div><div id="d_msg_e" class="alert-danger"></div></div>
                            </div>

                        </div>



                    </div>
                </div>
            </div>  

            <div class="modal fade" id="status_pop">
                <div class="modal-dialog modal-dialog-scrollable">
                    <div class="modal-content">

                        <!-- Modal Header -->
                        <div class="modal-header">
                            <h4 class="modal-title">Change status</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>

                        <!-- Modal body -->
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-6 col-md-6 col-xs-6">Status </div>
                                <div class="col-md-6 col-md-6 col-xs-6"> 
                                    <select class="form-control" id="status-list" name="status-list">
                                        <option>Processing</option>
                                        <option>Delivering</option>
                                        <option>Completed</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 col-md-12 col-xs-12"><div id="d_msg_p" class="alert-primary"></div><div id="d_msg_e" class="alert-danger"></div></div>
                            </div>

                        </div>

                        <!-- Modal footer -->
                        <div class="modal-footer">
                            <input type="hidden" name="u_id" id="u_id" class="text-field">
                            <button type="button" class="btn btn-secondary status-change" id="btnSubmit" onclick="change_status()">Save</button>
                        </div>

                    </div>
                </div>
            </div>            
            <!-- end popup --> 
            </tbody>
        </table>
    </div>   

    <script>
        prescription_id = '';
        $(document).on("click", ".assign_prescription", function () {
        prescription_id = $(this).data('id');
        $("#d_msg_s").html('');
        $("#btnSubmit").attr("disabled", false);
        });
        function assign_user() {
        var u_id = $("#user-list").val();
        $("#btnSubmit").attr("disabled", true);
        var user_id = $("#user-list").val();
        var id = $("#native_id").val();        
        $.ajax({
        url: '{{ url('pharmacy/assign') }}/' + prescription_id + '/' + u_id,
                type: 'get',
                data: {prescription: prescription_id, user: u_id},
                success: function (data) {
                $("#btnSubmit").attr("disabled", false);
                if (data.toString() == 1) {
                $("#d_msg_s").html('Prescription assigned to the user.');
//                window.setTimeout(function(){location.reload()}, 3000)
                } else {
                $("#d_msg_e").html('Errors occured.');
                $("#btnSubmit").attr("disabled", false);
                }
                //                    styleload();
                },
                complete: function () {
                    $("#assigned-"+prescription_id).html($("#user-list option:selected").text());
                    $("#status-"+prescription_id).html('Assigned');
                },
                error: function (data) {

                },
                timeout: 120000,
        });
        }

        $(document).on("click", ".history_change", function () {
        prescription_id = $(this).data('id');
        $.ajax({
        url: '{{ url('pharmacy/history') }}/' + prescription_id,
                type: 'get',
                success: function (data) {
                $("#btnSubmit").attr("disabled", false);
                $("#status-history").html(data);
                },
                complete: function () {

                },
                error: function (data) {

                },
                timeout: 120000,
        });
        });
        $(document).on("click", ".status_pop", function () {
        $("#d_msg_p").html('');    
        prescription_id = $(this).data('id');
        });
        function change_status() {
        var status = $("#status-list").val();
        $("#btnSubmit").attr("disabled", true);
        $.ajax({
        url: '{{ url('pharmacy/status') }}/' + prescription_id + '/' + status,
                type: 'get',
                success: function (data) {
                $("#btnSubmit").attr("disabled", false);
                if (data.toString() == 1) {
                $("#d_msg_p").html('Status changed.');
//                window.setTimeout(function(){location.reload()}, 3000)
                } else {
                $("#d_msg_p").html('Errors occured.');
                $("#btnSubmit").attr("disabled", false);
                }
                //                    styleload();
                },
                complete: function () {
                    $("#status-"+prescription_id).html($("#status-list option:selected").text());
                },
                error: function (data) {

                },
                timeout: 120000,
        });
        }
    </script>


</div>	






@stop
