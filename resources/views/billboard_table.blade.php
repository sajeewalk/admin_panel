@extends('master')
@section('content')

<div class="container">
<div class="row content">
<div class="col-md-12 col-sm-12 col-xs-12 quiz_table">
  <h3 class="">Billboard Table</h3>
<div class="row">
<div class="col-md-12 col-sm-12 col-xs-12">
    <table id="example" class="table table-striped" style="width:100%">
            <thead>
              <tr>
                <th >Id</th>
                <th >Name</th>
                <th >Image</th>
                <th >Logo</th>
                <th >Banner Image</th>
                <th>Treasure image</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>    
            @foreach($billbords as $keydetails)
            <tr>
              <th>{{$keydetails->id}}</th>
              <td>{{$keydetails->name}}</td>
              <td><img src="{{$keydetails->image}}" width="100" alt=""></td>
              <td><img src="{{$keydetails->logo}}" width="100" alt=""></td>
              <td><img src="{{$keydetails->banner}}" width="100" alt=""></td>
              <td><img src="{{$keydetails->treasure_image}}" width="100" alt=""></td>
              <td>{{$keydetails->name}}</td>
              <td></td>
            </tr>
            @endforeach
            </tbody>
           
        </table>
</div>
</div>
<!-- </div> -->
</div>
</div>

</div>

        <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap.min.js"></script>


            <script type="text/javascript">
            $(document).ready(function() {
           $('#example').DataTable( {
           "order": [[ 0, "desc" ]]
           });
          });
            </script>
            <script>

            </script>




@stop