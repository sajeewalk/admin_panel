@extends('master')

@section('content')
<div class="row content" style="padding: 30px 15px; margin-top: 100px;">
<form method="post" action="{{ url('create_program') }}" id="create_program">
    <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
    <div class="offset-md-3 col-md-6 col-sm-12 col-xs-12 create_program">
<div id="response">
            @if (session()->has('success'))
            <div class="alert alert-success" id="popup_notification">
                <strong>{{ session('success') }}</strong>
            </div>
            @endif
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif             
            </div>        
        <h5>Create New Program</h5>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <label>Program Name</label>
                <input type="text" name="name" id="name" required="">
            </div>    
        </div>

        <div class="row">
            <div class="col-md-10 col-sm-9 col-xs-6">
                <label>Program Image</label>
                <input type="text" name="program_image" id="program_image" onclick=open_dialog("program_image","image")>
            </div>
            <div class="col-md-2 col-sm-3 col-xs-6">
               <span><img src="#" width="100%" id="program_image-thumb" alt="Image"></span>
            </div>    
        </div>

        <div class="row">
            <div class="col-md-10 col-sm-9 col-xs-6">
                <label>Tile Image</label>
                <input type="text" name="tile_image" id="tile_image" onclick=open_dialog("tile_image","image")>
            </div>
            <div class="col-md-2 col-sm-3 col-xs-6">
                <span><img src="#" width="100%" id="tile_image-thumb" alt="Image"></span>
            </div>    
        </div>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <label>Intro action</label>
                <input type="text" name="intro_action" id="intro_action">
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
                <label>Action ID (Meta)</label>
                <input type="text" name="action_id" id="action_id">
            </div>
<!--             <div class="col-md-12 col-sm-12 col-xs-12">
                <label>Tags</label>
                <input type="text" name="tags" id="tags">
            </div>-->
            <div class="col-md-12 col-sm-12 col-xs-12">
                <label>Type</label>
                <select data-style="bg-white rounded-pill px-4 py-3 shadow-sm " class="selectpicker w-100" name="type" id="type">
                    <option>Health</option>
                    <option>Mind</option>
                    <option>Fitness</option>
                    <option>Diet</option>
                </select>
            </div>

            <div class="col-md-12 col-sm-12 col-xs-12">
                <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
                <button typ="submit">Create</button>
            </div>

        </div>

    </div>
</form>




   

</div>	
    <!-- popup -->
    <div class="modal fade" id="myModal">
    <div class="modal-dialog modal-xl modal-dialog-scrollable">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Select Image</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
        <div><input type="search" id="myInput1" name="myInput1" placeholder="Search"></div>
        <div id="myDIV" class="row">              
            <div id="gallery-images"></div>
        </div>
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary">Insert</button>
        </div>
        
      </div>
    </div>
  </div>
    <!-- end popup -->
<script>
   function open_dialog(ref,type){
       image_type=type;
       selected_obaject=ref;
       jQuery("#myModal").modal('show');         
    } 
       $('#myModal').on('show.bs.modal', function () {
            load_gallery_images();
        }); 
        function load_gallery_images(folder){ 
            folder = typeof folder !== 'undefined' ? folder : '';
            url='{{ url('image_library/popup/') }}';
            if(folder!=''){
                url='{{ url('image_library/popup') }}'+'/'+folder;
            }
             $.ajax({
                url: url,
                type: 'get',
                data:{"image_type":image_type},
                success: function (data) {
                    $('#gallery-images').html(data);
//                    styleload();
                },
                complete: function () {
                   
                },
                error: function (data) {

                },
                timeout: 120000,
            });           
        }
   function insert_url(url){
       $("#"+selected_obaject).val(url);
       $("#"+selected_obaject+"-thumb").attr("src",url);
       $('#myModal').fadeOut();
       $('#myModal').modal('hide');
    }        
</script>

@stop
