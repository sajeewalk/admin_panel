@extends('master')

@section('content')
<div class="row content">
<div class="offset-md-4 col-md-2 col-sm-3 col-xs-12 home_card">
<a href="{{ url('challenge')}}"><div>
<img src="{{ asset('images/challenge.png') }}" width="" alt="placeholder+image">
<h5>Create Challenges<br /></h5>
</div></a>
</div>

<div class="col-md-2 col-sm-3 col-xs-12 home_card">
<a href="{{ url('create_billboards')}}"><div>
<img src="{{ asset('images/manage.png') }}" width="" alt="placeholder+image">
<h5>Billboard Vendors<br/></h5>
</div></a>
</div>
</div>

@stop
