@extends('master') 

@section('content')
<div class="row content" style="padding: 30px 15px; margin-top: 100px;">
    <form method="post" action="{{ url('onboard/sessions') }}" id="create_contact">
        <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
        <div class="offset-md-12 col-md-12 col-sm-12 col-xs-12 create_program">
            <div id="response">
                @if (session()->has('success'))
                <div class="alert alert-success" id="popup_notification">
                    <strong>{{ session('success') }}</strong>
                </div>
                @endif
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif             
            </div>        
            <h5>Create Sesions</h5>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <label>Expert</label>
                    <select name="contact_id" id="salutation" class="form-control mselect" required="">
                        @foreach($contacts as $k=>$contact)
                        <option label="" value="{{ $contact->id }}" selected="selected">{{ $contact->first_name }} {{ $contact->last_name }}</option>
                        @endforeach
                    </select>
                </div> 
            </div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <label>Wellness center</label>
                    <select name="wellness_center" id="wellness_center" class="form-control" required="">
                        @foreach($wellness_centers as $k=>$wellness_center)
                        <option value="{{$k}}">{{$wellness_center}}</option>
                        @endforeach
                    </select>
                </div>    
            </div> 

            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <label>Start</label>
                    <input type="text" name="session_start" id="session_start" placeholder='YYYY-MM-DD' required="">
                </div> 
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <label>End</label>
                    <input type="text" name="session_end" id="session_end" placeholder='YYYY-MM-DD' required="">
                </div>              
            </div>            
            <?php for ($j = 1; $j < 8; $j++) { ?>
                <div class="row">
                    <div class="col-md-2 col-sm-2 col-xs-12">
                        <label><?php echo jddayofweek($j - 1, 1); ?> from</label>
                        <select name="from<?php echo $j; ?>[]" id="from<?php echo $j; ?>[]" class="form-control">
                            <option value=""></option>
                            <?php
                            $minutes = ['00', '30'];
                            for ($i = 0; $i < 24; $i++) {
                                $num_padded = sprintf("%02d", $i);
                                foreach ($minutes as $minute) {
                                    ?>
                                    <option value="<?php echo $num_padded . ":" . $minute . ":00" ?>"><?php echo $num_padded . ":" . $minute . ":00" ?></option>
                                <?php }
                            } ?>
                        </select>
                    </div> 
                    <div class="col-md-2 col-sm-2 col-xs-2">
                        <label>To</label>
                        <select name="to<?php echo $j; ?>[]" id="to<?php echo $j; ?>[]" class="form-control" >
                            <option value=""></option>
                            <?php
                            $minutes = ['00', '30'];
                            for ($i = 0; $i < 24; $i++) {
                                $num_padded = sprintf("%02d", $i);
                                foreach ($minutes as $minute) {
                                    ?>
                                    <option value="<?php echo $num_padded . ":" . $minute . ":00" ?>"><?php echo $num_padded . ":" . $minute . ":00" ?></option>
        <?php }
    } ?>
                        </select>
                    </div>
                    <div class="col-md-2 col-sm-2 col-xs-12">
                        <label>From</label>
                        <select name="from<?php echo $j; ?>[]" id="from<?php echo $j; ?>[]" class="form-control" >
                            <option value=""></option>
                            <?php
                            $minutes = ['00', '30'];
                            for ($i = 0; $i < 24; $i++) {
                                $num_padded = sprintf("%02d", $i);
                                foreach ($minutes as $minute) {
                                    ?>
                                    <option value="<?php echo $num_padded . ":" . $minute . ":00" ?>"><?php echo $num_padded . ":" . $minute . ":00" ?></option>
        <?php }
    } ?>
                        </select>
                    </div> 
                    <div class="col-md-2 col-sm-2 col-xs-2">
                        <label>To</label>
                        <select name="to<?php echo $j; ?>[]" id="to<?php echo $j; ?>[]" class="form-control">
                            <option value=""></option>
                            <?php
                            $minutes = ['00', '30'];
                            for ($i = 0; $i < 24; $i++) {
                                $num_padded = sprintf("%02d", $i);
                                foreach ($minutes as $minute) {
                                    ?>
                                    <option value="<?php echo $num_padded . ":" . $minute . ":00" ?>"><?php echo $num_padded . ":" . $minute . ":00" ?></option>
        <?php }
    } ?>
                        </select>
                    </div>   
                    <div class="col-md-2 col-sm-2 col-xs-12">
                        <label>From</label>
                        <select name="from<?php echo $j; ?>[]" id="from<?php echo $j; ?>[]" class="form-control">
                            <option value=""></option>
                            <?php
                            $minutes = ['00', '30'];
                            for ($i = 0; $i < 24; $i++) {
                                $num_padded = sprintf("%02d", $i);
                                foreach ($minutes as $minute) {
                                    ?>
                                    <option value="<?php echo $num_padded . ":" . $minute . ":00" ?>"><?php echo $num_padded . ":" . $minute . ":00" ?></option>
        <?php }
    } ?>
                        </select>
                    </div> 
                    <div class="col-md-2 col-sm-2 col-xs-2">
                        <label>To</label>
                        <select name="to<?php echo $j; ?>[]" id="to<?php echo $j; ?>[]" class="form-control">
                            <option value=""></option>
                            <?php
                            $minutes = ['00', '30'];
                            for ($i = 0; $i < 24; $i++) {
                                $num_padded = sprintf("%02d", $i);
                                foreach ($minutes as $minute) {
                                    ?>
                                    <option value="<?php echo $num_padded . ":" . $minute . ":00" ?>"><?php echo $num_padded . ":" . $minute . ":00" ?></option>
        <?php }
    } ?>
                        </select>
                    </div>                    
                </div>
<?php } ?>
            <div class="col-md-12 col-sm-12 col-xs-12">
                <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
                <button typ="submit">Create</button>
            </div>

        </div>

</div>
</form>






</div>	
<!-- popup -->
<div class="modal fade" id="myModal">
    <div class="modal-dialog modal-xl modal-dialog-scrollable">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Select Image</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <div><input type="search" id="myInput1" name="myInput1" placeholder="Search"></div>
                <div id="myDIV" class="row">              
                    <div id="gallery-images"></div>
                </div>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary">Insert</button>
            </div>

        </div>
    </div>
</div>
<!-- end popup -->
<script>
    function open_dialog(ref, type) {
        image_type = type;
        selected_obaject = ref;
        jQuery("#myModal").modal('show');
    }
    $('#myModal').on('show.bs.modal', function () {
        load_gallery_images();
    });
    function load_gallery_images(folder) {
        folder = typeof folder !== 'undefined' ? folder : '';
                url = '{{ url('image_library / popup / ') }}';
        if (folder != ''){
        url = '{{ url('image_library / popup') }}' + '/' + folder;
    }
    $.ajax({
    url: url,
            type: 'get',
            data:{"image_type":image_type},
            success: function (data) {
            $('#gallery-images').html(data);
//                    styleload();
            },
            complete: function () {

            },
            error: function (data) {

            },
            timeout: 120000,
    });
    }
    function insert_url(url) {
        $("#" + selected_obaject).val(url);
        $("#" + selected_obaject + "-thumb").attr("src", url);
        $('#myModal').fadeOut();
        $('#myModal').modal('hide');
    }
$(document).ready(function() {
    $('.mselect').select2();
});    
</script>

@stop
