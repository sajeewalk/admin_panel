@extends('master')

@section('content')
<?php print_r($post);?>
<div class="row content">

    <div class="col-md-2 col-sm-3 col-xs-12 sidebar">
        <button onclick="add_heading()">Heading</button>
        <button onclick="add_sub_heading()">Subheading</button>
        <button onclick="add_paragraph()">Paragraph</button>
        <button onclick="add_image_full()">Images</button>
        <button onclick="add_button()">Buttons</button>
        <button onclick="add_list()">List</button>
        <button onclick="add_gif()">GIF</button>
        <button onclick="add_video_full()">Video</button>
        <button onclick="add_price()">Price</button>
        <button onclick="add_table()">Table</button>
        <button onclick="add_authors()">Author</button>
        <button onclick="add_terms()">Tearms & Conditions</button>
<!--        <button data-toggle="modal" data-target="#myModal">Popup</button>-->
    </div>

    <div class="col-md-10 col-sm-9 col-xs-12 templates" >
        <form method="post" action="{{ url('dynamic_template_submit') }}" id="template_form">
            <div id="response">
            @if (session()->has('success'))
            <div class="alert alert-success" id="popup_notification">
                <strong>{{ session('success') }}</strong>
            </div>
            @endif
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif             
            </div>            
            <div id="work_area">

            </div>
            <div class="footer_btn">            
                <input type="hidden" name="type" id="type" value="">
                <input type="hidden" name="id" id="id" value="####">
                <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
                <button data-toggle="modal" data-target="#preview_template" type="button">Preview</button>
                <button type="submit">Save</button>
            </div>
        </form>   
    </div>


    <!-- popup -->
    <div class="modal fade" id="myModal">
    <div class="modal-dialog modal-xl modal-dialog-scrollable">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Select Image</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
            <div id="gallery-images"></div>

        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary">Insert</button>
        </div>
        
      </div>
    </div>
  </div>
    <!-- end popup -->

    <!-- popup -->
    <div class="modal fade" id="preview_template">
    <div class="modal-dialog modal-dialog-scrollable">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Native Post</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
          <div class="row"><div id="preview_template_body"></div></div>


        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" onclick="main_form_submit()">Save</button>
        </div>
        
      </div>
    </div>
  </div>
    <!-- end popup -->    

</div>	

<script>
    var counter = 1;
    var image_type='all';
    $(document).ready(function () {
        add_template_name('<?php echo trim($name);?>');
        <?php
        foreach($post['template'] as $t){
            if($t['type']=='heading'){
                echo 'add_heading("'.trim($t['text']).'");';
            }
            if($t['type']=='sub-heading'){
                echo 'add_sub_heading("'.trim($t['text']).'");';
            }
            if($t['type']=='paragraph'){
                echo 'add_paragraph("'.trim($t['text']).'");';
            }
            if($t['type']=='image-full' || $t['type']=='image-sub'){
                echo 'add_image_full("'.trim($t['thumb']).'");';
            }
            if($t['type']=='button'){
                foreach($t['list'] as $l){
                echo 'add_button("'.trim($l['label']).'","'.trim($l['action']).'","'.trim($l['meta']).'","'.trim($l['type']).'","'.trim($l['status']).'");';
                }
            }             
        }
        ?>
    });
    
    function add_sample() {
        paragraph = '';
        $('#work_area').append(paragraph);
        counter++;
    }    
    
    function add_template_name(val) {
         heading = '<div class="main_block">\
            <h4>Native Post Template</h4>\
            <div class="block">\
                Enter name \
                <input type="text" name="name" value="'+val+'" class="text-field" required>\
            </div>\
        </div>';
        $('#work_area').append(heading);       
    }

    function add_heading(val) {
        heading = '<div class="main_block">\
            <h4>Heading</h4>\
            <div class="block">\
                Enter heading \
                <input type="text" name=field[' + counter + '][heading] value="'+val+'" class="text-field">\
            </div>\
            <div class="close">\
                <span class="remove"><img src="{{ asset("images/delete.png") }}" width="100%"></span>\
            </div>\
        </div>';
        $('#work_area').append(heading);
        counter++;
    }
    function add_sub_heading(val) {
        sub_heading = '<div class="main_block">\
            <h4>Subheading</h4>\
            <div class="block">\
                Enter subheading \
                <input type="text" name=field[' + counter + '][sub-heading] value="'+val+'" class="text-field">\
            </div>\
            <div class="close">\
                    <span class="remove"><img src="{{ asset("images/delete.png") }}" width="100%" alt="placeholder+image"></span>\
            </div>\
        </div>';
        $('#work_area').append(sub_heading);
        counter++;
    }
    function add_paragraph(val) {
        paragraph = '<div class="main_block">\
            <h4>Paragraph</h4>\
            <div class="block">\
                Enter paragraph \
                <textarea name=field[' + counter + '][paragraph] class="text-field">'+val+'</textarea>\
            </div>\
            <div class="close">\
                    <span class="remove"><img src="{{ asset("images/delete.png") }}" width="100%" alt="placeholder+image"></span>\
            </div>\
        </div>';
        $('#work_area').append(paragraph);
        counter++;
    }
    
    function add_image_full(val) {
        image_full = '<div class="main_block">\
            <h4>Image</h4>\
            <div class="row">\
                <div class="col-md-6 col-sm-6 col-xs-12 block">\
                    Image type\
                    <!-- radio btn -->\
                    <div class="btn-group btn-group-toggle" data-toggle="buttons">\
                        <label class="btn btn-secondary active">\
                            <input type="radio" name=field[' + counter + ']["image-full-image-type"] id="option1" autocomplete="off" checked> Full Image (1:1)\
                        </label>\
                        <label class="btn btn-secondary">\
                            <input type="radio" name=field[' + counter + ']["image-full-image-type"] id="option2" autocomplete="off"> Post Image (3:4)\
                        </label>\
                    </div>\
                    <!-- end radio btn -->\
                </div>\
                <div class="col-md-6 col-sm-6 col-xs-12 block">\
                    <!-- ... -->\
                </div>\
            </div>\
            <div class="block">\
                Select image from image library\
                <span><img src="#" width="104" id=field-' + counter + '-image-full-thumb alt="Image Preview"></span>\
                <input type="text" name=field[' + counter + '][image-full] id=field-' + counter + '-image-full value="'+val+'" onclick=open_dialog("field-' + counter + '-image-full","image") class="text-field">\
            </div>\
            <div class="close">\
                <span class="remove"><img src="{{ asset("images/delete.png") }}" width="100%" alt="placeholder+image"></span>\
            </div>\
        </div>';
        $('#work_area').append(image_full);
        counter++;
    } 
    
    function add_video_full() {
        paragraph = '<div class="main_block">\
            <h4>Video</h4>\
            <div class="block">\
                Select Video\
                <!-- <span><img src="images/user.jpg" width="100%" alt="placeholder+image"></span> -->\
                <input type="text" name=field[' + counter + '][video-full] id=field-' + counter + '-video-full onclick=open_dialog("field-' + counter + '-video-full","video") class="text-field">\
            </div>\
            <div class="block">\
                Select Video thumbnail\
                <span><img src="#" width="100%" id=field-' + counter + '-video-full-thumb-thumb alt="Image Preview"></span>\
            <input type="text" name=field[' + counter + '][video-full-thumb] id=field-' + counter + '-video-full-thumb onclick=open_dialog("field-' + counter + '-video-full-thumb","image") class="text-field">\
            </div>\
            <div class="close">\
                <span class="remove"><img src="{{ asset("images/delete.png") }}" width="100%" alt="placeholder+image"></span>\
            </div>\
        </div>';
        $('#work_area').append(paragraph);
        counter++;
    }  
    
    function add_gif() {
        paragraph = '<div class="main_block">\
            <h4>GIF</h4>\
            <div class="block">\
                Select image from image library\
                <span><img src="#" width="100%" id=field-' + counter + '-image-full-thumb alt="Image Preview"></span>\
            <input type="text" name=field[' + counter + '][image-full] id=field-' + counter + '-image-full onclick=open_dialog("field-' + counter + '-image-full","image") class="text-field">\
            </div>\
            <div class="close">\
                <span class="remove"><img src="{{ asset("images/delete.png") }}" width="100%" alt="placeholder+image"></span>\
            </div>\
        </div>';
        $('#work_area').append(paragraph);
        counter++;
    }   
    
    function add_price() {
        paragraph = '<div class="main_block">\
            <h4>Price</h4>\
            <div class="row">\
                <div class="col-md-6 col-sm-6 col-xs-12 block">\
                    Currency type\
                    <select name=field[' + counter + '][price-type]>\
                        <option>LKR</option>\
                        <option>USD</option>\
                        <option>TK</option>\
                    </select>\
                </div>\
                <div class="col-md-6 col-sm-6 col-xs-12 block">\
                    Amount \
                    <input type="text" name=field[' + counter + '][price] class="text-field">\
                </div>\
            </div>\
            <div class="close">\
                <span class="remove"><img src="{{ asset("images/delete.png") }}" width="100%" alt="placeholder+image"></span>\
            </div>\
        </div>';
        $('#work_area').append(paragraph);
        counter++;
    }
    
    function add_terms() {
        paragraph = '<div class="main_block">\
            <h4>Terms & Conditions</h4>\
            <div class="block">\
                Page url\
                <input type="text" name=field[' + counter + '][terms] class="text-field">\
            </div>\
                <a onclick="add_button()">\
                <div class="block">\
                    <img src="images/add.png" alt="placeholder+image">\
                    <label>Add buttons</label>\
                </div></a>\
            <div class="close">\
                <span class="remove"><img src="{{ asset("images/delete.png") }}" width="100%" alt="placeholder+image"></span>\
            </div>\
        </div>';
        $('#work_area').append(paragraph);
        counter++;
    }  
    
    function add_authors() {
        paragraph = '<div class="main_block">\
            <h4>Authors</h4>\
            <div class="row">\
                <div class="col-md-6 col-sm-6 col-xs-12 block">\
                    Name\
                    <input type="text" name=field[' + counter + '][authors][] class="text-field">\
                </div>\
                <div class="col-md-6 col-sm-6 col-xs-12 block">\
                    Image\
                    <span><img src="#" width="100%" id=field-' + counter + '-authors-image-thumb alt="Image Preview"></span>\
                <input type="text" name=field[' + counter + '][authors-image][] id=field-' + counter + '-authors-image onclick=open_dialog("field-' + counter + '-authors-image","image") class="text-field">\
            </div>\
                <div class="col-md-6 col-sm-6 col-xs-12 block">\
                    Sub label\
                    <input type="text" name=field[' + counter + '][authors-sub][] class="text-field">\
                </div>\
            </div>\
            <div class="close">\
                <span class="remove"><img src="{{ asset("images/delete.png") }}" width="100%" alt="placeholder+image"></span>\
            </div>\
        </div>';
        $('#work_area').append(paragraph);
        counter++;
    }  
    
    function add_table() {
        paragraph = '<div class="main_block">\
            <h4>Tables</h4>\
            <div class="row">\
                <div class="col-md-5 col-sm-4 col-xs-12 block">\
                    Number of columns \
                    <select id=col-'+counter+'>\
                        <option selected="">1</option>\
                        <option>2</option>\
                        <option>3</option>\
                        <option>4</option>\
                        <option>5</option>\
                        <option>6</option>\
                        <option>7</option>\
                        <option>8</option>\
                    </select>\
                </div>\
                <div class="col-md-5 col-sm-4 col-xs-12 block">\
                    Number of rows\
                    <select id=row-'+counter+'>\
                        <option>2</option>\
                        <option>3</option>\
                        <option>4</option>\
                        <option>5</option>\
                        <option>6</option>\
                        <option>7</option>\
                        <option>8</option>\
                        <option>9</option>\
                        <option>10</option>\
                        <option>11</option>\
                        <option>12</option>\
                        <option>13</option>\
                        <option>14</option>\
                        <option>15</option>\
                        <option>16</option>\
                        <option>17</option>\
                        <option>18</option>\
                        <option>19</option>\
                        <option>20</option>\
                        <option>21</option>\
                        <option>22</option>\
                        <option>23</option>\
                        <option>24</option>\
                        <option>25</option>\
                        <option>26</option>\
                    </select>\
                </div>\
                <div class="col-md-2 col-sm-4 col-xs-12 block">\
                    <div id=table-'+counter+'></div>\
                    <button style="position: absolute; bottom: 7px;" type="button" onclick="generate_table(this)">Generate</button>\
                </div>\
                <div class="col-md-5 col-sm-4 col-xs-12 block">\
                <div id=dynamic-table-'+counter+' ></div>\
                </div>\
            </div>\
            <div class="close">\
                <span class="remove"><img src="{{ asset("images/delete.png") }}" width="100%" alt="placeholder+image"></span>\
            </div>\
        </div>';
        $('#work_area').append(paragraph);
        $('#table-'+counter).val(counter);
        counter++;
    }
    
    function add_button(label,action,meta,type,status) {
        paragraph = '<div class="main_block">\
            <h4>Buttons</h4>\
            <div class="row">\
                <div class="col-md-6 col-sm-6 col-xs-12 block">\
                    Label\
                    <input type="text" name=field[' + counter + '][button][] value="'+label+'" class="text-field">\
                </div>\
                <div class="col-md-6 col-sm-6 col-xs-12 block">\
                    Action\
                    <select name=field[' + counter + '][button-action][]>\
                        <option>commonview</option>\
                        <option>paynow</option>\
                        <option>challenge</option>\
                        <option>dynamicquestion</option>\
                        <option>native</option>\
                        <option>web</option>\
                    </select>\
                </div>\
            </div>\
            <div class="row">\
                <div class="col-md-6 col-sm-6 col-xs-12 block">\
                    Meta\
                    <input type="text" name=field[' + counter + '][button-meta][] value="'+meta+'" class="text-field">\
                </div>\
                <div class="col-md-6 col-sm-6 col-xs-12 block">\
                    Type\
                    <select name=field[' + counter + '][button-type][]>\
                        <option></option>\
                        <option>native</option>\
                    </select>\
                </div>\
            </div>\
            <div class="row">\
                <div class="col-md-6 col-sm-6 col-xs-12 block">\
                    Status\
                    <!-- radio btn -->\
                    <div class="btn-group btn-group-toggle" data-toggle="buttons">\
                        <input type="hidden" name=field[' + counter + '][button-status-text][] class="text-field">\
                        <label class="btn btn-secondary active">\
                            <input type="radio" onchange="set_text_box_value(this)" name=field[' + counter + '][button-status][] id="option1" value="enabled" autocomplete="off" checked> Active\
                        </label>\
                        <label class="btn btn-secondary">\
                            <input type="radio" onchange="set_text_box_value(this)" name=field[' + counter + '][button-status][] id="option2" value="disabled" autocomplete="off"> Disable\
                        </label>\
                    </div>\
                    <!-- end radio btn -->\
                </div>\
                <div class="col-md-6 col-sm-6 col-xs-12 block">\
                    <!-- ... -->\
                </div>\
            </div>\
            <div id=field-' + counter + '-button-more ></div>\
            <div id=button-'+counter+'></div>\
            <a onclick=add_button_more(this)>\
                <div class="block">\
                    <img src="{{ asset("images/add.png") }}" alt="placeholder+image">\
                    <label>Add new button</label>\
                </div>\
            </a>\
            <div class="close">\
                <span class="remove"><img src="{{ asset("images/delete.png") }}" width="100%" alt="placeholder+image"></span>\
            </div>\
        </div>';
        $('#work_area').append(paragraph);
        $('#button-'+counter).val(counter);
        counter++;
    }
    
    function add_button_force(id) {
        paragraph = '<div class="button-remove"><div class="row">\
                <div class="col-md-6 col-sm-6 col-xs-12 block">\
                    Label\
                    <input type="text" name=field[' + id + '][button][] class="text-field">\
                </div>\
                <div class="col-md-6 col-sm-6 col-xs-12 block">\
                    Action\
                    <select name=field[' + id + '][button-action][]>\
                        <option>commonview</option>\
                        <option>paynow</option>\
                        <option>challenge</option>\
                        <option>dynamicquestion</option>\
                        <option>native</option>\
                        <option>web</option>\
                    </select>\
                </div>\
            </div>\
            <div class="row">\
                <div class="col-md-6 col-sm-6 col-xs-12 block">\
                    Meta\
                    <input type="text" name=field[' + id + '][button-meta][] class="text-field">\
                </div>\
                <div class="col-md-6 col-sm-6 col-xs-12 block">\
                    Type\
                    <select name=field[' + id + '][button-type][]>\
                        <option></option>\
                        <option>native</option>\
                    </select>\
                </div>\
            </div>\
            <div class="row">\
                <div class="col-md-6 col-sm-6 col-xs-12 block">\
                    Status\
                    <!-- radio btn -->\
                    <div class="btn-group btn-group-toggle" data-toggle="buttons">\
                        <input type="hidden" name=field[' + id + '][button-status-text][] class="text-field">\
                        <label class="btn btn-secondary active">\
                            <input type="radio" onchange="set_text_box_value(this)" name=field[' + id + '][button-status][] id="option1" value="enabled" autocomplete="off" checked> Active\
                        </label>\
                        <label class="btn btn-secondary">\
                            <input type="radio" onchange="set_text_box_value(this)" name=field[' + id + '][button-status][] id="option2" value="disabled" autocomplete="off"> Disable\
                        </label>\
                    </div>\
                    <!-- end radio btn -->\
                </div>\
                <div class="col-md-6 col-sm-6 col-xs-12 block">\
                    <!-- ... -->\
                </div>\
            </div>\
            <a onclick=add_button_remove(this)>\
                <div class="block" style="color:#f44747;float:right">\
                    <label>Remove</label>\
                </div>\
            </a></div>';
        $('#field-' + id + '-button-more').append(paragraph);
//        $('#button-'+counter).val(counter);
       
    }    
    
    function add_button_more(obj) {
        id=$(obj).prev("div").val();
        add_button_force(id);
    }    
    
    function add_button_remove(obj) {
        $(obj).closest('.button-remove').remove();
    }  
  
    function add_list() {
        paragraph = '<div class="main_block">\
            <h4>Lists</h4>\
            <div class="row">\
                <div class="col-md-6 col-sm-6 col-xs-12 block">\
                    Status\
                    <!-- radio btn -->\
                    <div class="btn-group btn-group-toggle" data-toggle="buttons">\
                        <label class="btn btn-secondary active">\
                            <input type="radio" name=field[' + counter + '][list-status] id="option1" autocomplete="off" value="numbered-list" checked> Ordered List\
                        </label>\
                        <label class="btn btn-secondary">\
                            <input type="radio" name=field[' + counter + '][list-status] id="option2" value="bulleted-list" autocomplete="off">\
                            Unordered List\
                        </label>\
                    </div>\
                    <!-- end radio btn -->\
                </div>\
                <div class="col-md-6 col-sm-6 col-xs-12 block">\
                    <!-- ... -->\
                </div>\
            </div>\
            <div class="row">\
                <div class="col-md-12 col-sm-12 col-xs-12 block">\
                    Label\
                    <input type="text" name=field[' + counter + '][list][] class="text-field">\
                    <div id=field-' + counter + '-list-more ></div>\
                </div>\
            </div>\
            <div id=list-'+counter+'></div>\
            <a onclick=add_list_more(this)>\
                <div class="block">\
                    <img src="images/add.png" alt="placeholder+image">\
                    <label>Add new list item</label>\
                </div>\
            </a>\
            <div class="close">\
                <span class="remove"><img src="images/delete.png" width="100%" alt="placeholder+image"></span>\
            </div>\
        </div>';
        $('#work_area').append(paragraph);
        $('#list-'+counter).val(counter);
        counter++;
    }  
    
    function add_list_more(obj) {
        id=$(obj).prev("div").val();
        var string='<div class="list-remove"><input type="text" name=field[' + id + '][list][] class="text-field"><a onclick=add_list_more_remove(this)><strong>Remove</strong></a></div>';
        $('#field-' + id + '-list-more').append(string);
    }
    function add_list_more_remove(obj) {
        $(obj).closest('.list-remove').remove();
    } 
    
    function generate_table(obj){
    temp=$(obj).prev("div").val(); 
    rows=$('#col-'+temp).val();
    cols=$('#row-'+temp).val();
    
    dynamic_table=$('#dynamic-table-'+temp);
    dynamic_table.empty();
    table ='<table border=1>' ;

    for(i=0;i<cols;i++)
    {
    header='';    
    table += '<tr> ';

        for(ii=0;ii<rows;ii++)
        {
            if(i==0){
            header='Header '+(ii+1);    
        }
        table += '<td><input type=text value="'+header+'" name="field['+temp+'][table]['+i+'][]"></td>' ;
        }
    table += '</tr> ';    
    }
    
    table +='</table>' ; 
    
    dynamic_table.append( table );
    }

    $('#work_area').on('click', '.remove', function () {
        $(this).parent().parent().remove();
        counter++;
    });

    function process_json() {
        p = 0;
        var entity = [];
        var post = [];
        entity['type'] = $('#type').val();
        entity['name'] = $('#name').val();
        entity['id'] = $('#id').val();
        $.each($('.main_block .text-field'), function (i, val) {
            str = $(this).attr('name');
            if (str.indexOf("sub_heading") >= 0) {
                f_type = 'sub_heading';
            } else if (str.indexOf("heading") >= 0) {
                f_type = 'heading';
            } else if (str.indexOf("paragraph") >= 0) {
                f_type = 'paragraph';
            }
            post.push({
                'type': f_type,
                'text': $(this).val(),
            });

//            post[p].type=f_type;
//            post[p].text=$(this).val();
//            alert($(this).attr('name'));
            p++;
        });
        entity['template'] = post;
        console.log(entity);
        alert(JSON.stringify(entity));
    }
    
       $('#myModal').on('show.bs.modal', function () {
            load_gallery_images();
        });
        $('#preview_template').on('show.bs.modal', function () {
            load_template_preview();
        });       
        function load_template_preview(){
           $.ajax({
            type: 'post',
            url: '{{ url('preview_template') }}',
            data: $('#template_form').serialize(),
            success: function (data) {
             $('#preview_template_body').html(data);
            }
          });           
        }
     
        function load_gallery_images(){          
             $.ajax({
                url: '{{ url('image_library/popup') }}',
                type: 'get',
                data:{"image_type":image_type},
                success: function (data) {
                    $('#gallery-images').html(data);
//                    styleload();
                },
                complete: function () {
                   
                },
                error: function (data) {

                },
                timeout: 120000,
            });           
        } 
   selected_obaject='';     
   function open_dialog(ref,type){
       image_type=type;
       selected_obaject=ref;
       jQuery("#myModal").modal('show');         
    }
   function insert_url(url){
       $("#"+selected_obaject).val(url);
       $("#"+selected_obaject+"-thumb").attr("src",url);
       $('#myModal').fadeOut();
       $('#myModal').modal('hide');
    }
</script>

<script type="text/javascript">
    var addclass = 'select_div';
    var $cols = $('.img-tile').click(function(e) {
    $cols.removeClass(addclass);
    $(this).addClass(addclass);
});

function set_text_box_value(obj){
//    console.log(obj);
//    $(obj).closest("div").find("input[name='field[\" + counter + \"][button-status-text][]']").val('sssss');
//    $(obj).siblings("input[name='field[1][button-status-text][]']").val('ssssssss');
//    $(obj).closest("input[name='field[1][button-status-text][]']").focus();
var temp=counter;
temp--;
$(obj).closest("div").find("input[name='field["+temp+"][button-status-text][]']").val($(obj).val());
}

function main_form_submit(){
    if ($("#template_form")[0].checkValidity()){
    $( "#template_form" ).submit();
    }else{
        $("#template_form")[0].reportValidity()
    }
}
//$(function () {
//    $('#template_form').submit(function () {
//        if($('#template_form').validate()) {
//            alert('the form is valid');
//        }
//    });
//});
</script>

@stop
