@extends('master')

@section('content')
<div class="row content" style="background: rgba(255,255,255,0.7); padding: 30px 15px;">
 <div class="col-md-12 col-sm-12 col-xs-12">
        <h5><a href="{{ url("image_library") }}">Image Library</a> /{{$folder}}</h5> 
    <?php if(empty($folder)){?>
           <a href="" data-toggle="modal" data-target="#create_folder">
           <span>
           <img src="{{ url('images/logout.png')}}" height="30px" alt="placeholder+image"> 
           Create Folder
           </span>
           </a>    
    <?php } ?>
    <input type="search" id="myInput" name="myInput" placeholder="Search">     
</div>
<div id="myDIV" class="row" >
<?php
$sfiles = array();
$sdirs = array();
foreach ($files as $fileinfo) {
    if (!$fileinfo->isDot()) {

        $sfiles[$fileinfo->getMTime()][] = $fileinfo->getFilename();
    }
    if ($fileinfo->isDir() && !$fileinfo->isDot()) {
        $sdirs[$fileinfo->getMTime()][] = $fileinfo->getFilename();
    }
}
krsort($sfiles);
krsort($sdirs);
//    print_r($sfiles);
//    die;
$image_path = "uploads/";
$del_path = "";
if (!empty($folder)) {
    $image_path = $image_path . $folder . '/';
    $del_path = $folder . '/';
}


foreach ($sfiles as $sfile_name) {
    $file_name = $sfile_name[0];
    $file_explode = explode('.', $file_name);
    $extention = strtolower(end($file_explode));
    if ($extention != 'jpg' && $extention != 'gif' && $extention != 'png' && $extention != 'mp4') {
        ?>
                    <div class="col-md-2 col-md-3 col-xs-12 img-tile">
                        <div style="min-width:140px;overflow: hidden;text-overflow: ellipsis;">    
                            <a href="{{ url('image_library/'.$sfile_name[0]) }}"><img src="{{ asset('images/folder.jpg') }}" width="100%" alt="{{ $sfile_name[0] }}" title="{{ $sfile_name[0] }}"></a>
                            <span onclick="myFunction('{{ $del_path.$sfile_name[0] }}')">
        <!--                        <img src="{{ asset('images/delete.png') }}" width="100%" alt="placeholder+image">-->
                            </span>
                            <p class="font-italic small">{{ $sfile_name[0] }}</p>
                        </div>
                    </div>    
                    <?php
                }
            }
            foreach ($sfiles as $sfile_name) {
                $file_name = $sfile_name[0];
                $file_explode = explode('.', $file_name);
                $extention = strtolower(end($file_explode));
                if ($extention == 'jpg' || $extention == 'gif' || $extention == 'png') {
                    ?>
                    <div class="col-md-2 col-md-3 col-xs-12 img-tile">
                        <div style="min-width:140px;overflow: hidden;text-overflow: ellipsis;">    
                            <img src="{{ asset($image_path.$sfile_name[0]) }}" width="100%" alt="File">
                            <span onclick="myFunction('{{ $del_path.$sfile_name[0] }}')">
                                <img src="{{ asset('images/delete.png') }}" width="100%" alt="{{ $sfile_name[0] }}" title="{{ $sfile_name[0] }}">
                            </span>
                            <p class="font-italic small">{{ $image_path.$sfile_name[0] }}</p>
                        </div>
                    </div>
                    <?php
                } elseif ($extention == 'mp4') {
                    ?>
                    <div class="col-md-2 col-md-3 col-xs-12 img-tile">
                        <div style="min-width:140px;overflow: hidden;text-overflow: ellipsis;">    
                            <video width="250" controls>
                                <source src="{{ asset($image_path.$sfile_name[0]) }}" type="video/mp4">
                                Your browser does not support the video tag.
                            </video> 
                            <span onclick="myFunction('{{ $del_path.$sfile_name[0] }}')">
                                <img src="{{ asset('images/delete.png') }}" width="100%" alt="placeholder+image">
                            </span>
                            <p class="font-italic small">{{ $sfile_name[0] }}</p>
                        </div>
                    </div>
                    <?php
                }
            }
            ?>


    </div>	
    <form action="{{ url('image_library/'.$folder) }}" method="post" enctype="multipart/form-data"> 
        <div class="row img_upload">
            <div class="col-md-10 col-sm-10 col-xs-12">
                File name <input type="text" name="file_name" required="" onkeyup="nospaces(this)">
            </div>        
            <div class="col-md-10 col-sm-10 col-xs-12">
                <p>Type </p>
                Native post <input type="radio" name="file_type" value="native_post" checked="" >
                Content icon <input type="radio" value="content_icon" name="file_type" >
                Timeline image <input type="radio" value="timeline_image" name="file_type" >
                Video <input type="radio" value="video" name="file_type" >
                Other <input type="radio" value="other_type" name="file_type" >
                Program <input type="radio" value="program" name="file_type" >
                Questionnaire <input type="radio" value="questionnaire" name="file_type" >
            </div> 
            <div class="col-md-10 col-sm-10 col-xs-12">
                <input type="file" name="file">
            </div>
            <div class="col-md-2 col-sm-2 col-xs-12">
                <button type="submit">Upload</button>
            </div>

        </div>
        <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
    </form> 

<!-- <script type="text/javascript">
    var addclass = 'select_div';
    var $cols = $('.img-tile').click(function(e) {
    $cols.removeClass(addclass);
    $(this).addClass(addclass);
});
</script> -->

    <script>
        function myFunction(file) {
        var sure = confirm("Are you sure to delete this image");
        if (sure){
        location.href = "{{ url('image_library') }}?del=" + file;
        }
        }
        function nospaces(t){
  if(t.value.match(/\s/g)){
    t.value=t.value.replace(/\s/g,'');
  }
}

    </script>
    <!-- popup -->
    <div class="modal fade" id="create_folder">
    <div class="modal-dialog modal-dialog-scrollable">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Create Folder</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <form method="post" action="{{ url("/create_folder/") }}">  
        <div class="modal-body">
            
            <div class="row">
                <div class="col-md-6 col-md-6 col-xs-6">Name </div>
                <div class="col-md-6 col-md-6 col-xs-6"><input type="text" name="folder_name" id="folder_name" class="text-field" onkeyup="nospaces(this)"> </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-md-12 col-xs-12"><div id="d_msg_s" class="alert-primary"></div><div id="d_msg_e" class="alert-danger"></div></div>
            </div>
            
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
<!--            <input type="hidden" name="program_id" id="program_id" class="text-field">-->
            <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">  
            <button type="submit" class="btn btn-secondary" id="btnSubmit">Save</button>
        </div>
        </form>   
      </div>
    </div>
  </div>
<script>
$(document).ready(function(){
  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $('#myDIV .img-tile').filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>    
    @stop
