
@extends('master')

@section('content')

<div class="container">
<div class="row content">
<div class="col-md-12 col-sm-12 col-xs-12 quiz_table">
  <h3 class="">Quiz Table</h3>
<!-- <div class=""> -->

<?php $exception = Session::get('exception'); ?>
@if($exception)
<p class="alert alert-success">
 {{$exception}}
</p>
 <?php Session::put('exception',null); ?>

@endif

@if(count($errors)>0)
<ul>
@foreach($errors->all() as $error)
<li class="alert alert-danger">{{$error}}</li>
@endforeach
</ul>
@endif
<div class="row">
<div class="col-md-12 col-sm-12 col-xs-12">
    <table id="example" class="table table-striped" style="width:100%">
            <thead>
              <tr>
                <th >Id</th>
                <th >Question Name</th>
                <th >Description</th>
                <th >Group Intro</th>
                <th >Intro Title</th>
                <th>Intro Text</th>
                <th>Group Ending</th>
                <th >Ending Text</th>
                <th >Update</th>
                <th >Create Tile</th>
              </tr>
            </thead>
            <tbody>
              @foreach($questionnaire as $que)
            <tr>
              <th>{{$que->id}}</th>
              <td><a  href="{{ url('/quetions/'.$que->id) }}">{{$que->type}}</a></td>
              <td>{{$que->description}}</td>
              <td>{{$que->group_intro}}</td>
              <td>{{$que->Intro_title}}</td>
              <td>{{$que->intro_text}}</td>
                <td>{{$que->group_ending}}</td>
              <td>{{$que->ending_text}}</td>
              <td> <!-- <a href="updatequestinnaire/{{$que->id}}" class="btn btn-info">Update</a> -->
              <button class="btn-success" title="Edit" onclick="window.location.href = 'updatequestinnaire/{{$que->id}}';"><i class="fa fa-edit"></i></button>
              </td>
              <td> <!-- <a href="updatequestinnaire/{{$que->id}}" class="btn btn-info">Update</a> -->
               <a href="" class="btn btn-success tile_create" data-toggle="modal" data-target="#create_tile" data-id="{{$que->id}}">Create</a>
              </td>
            </tr>
            @endforeach
            </tbody>
            <!-- popup -->
<div class="modal fade" id="create_tile">
<div class="modal-dialog modal-dialog-scrollable">
<div class="modal-content">

<!-- Modal Header -->
<div class="modal-header">
<h4 class="modal-title">Create Tile</h4>
<button type="button" class="close" data-dismiss="modal">&times;</button>
</div>

<!-- Modal body -->
<div class="modal-body">
<div class="row">
    <div class="col-md-6 col-md-6 col-xs-6">Mobile </div>
    <div class="col-md-6 col-md-6 col-xs-6"><input type="text" name="mobile" id="mobile" class="text-field"> </div>
</div>
<div class="row">
    <div class="col-md-12 col-md-12 col-xs-12"><div id="d_msg_s" class="alert-primary"></div><div id="d_msg_e" class="alert-danger"></div></div>
</div>

</div>

<!-- Modal footer -->
<div class="modal-footer">
<input type="hidden" name="native_id" id="native_id" class="text-field">
<button type="button" class="btn btn-info" id="btnSubmit" onclick="np_create_tile()">Save</button>
</div>

</div>
</div>
</div>
<!-- end popup -->
        </table>
</div>
</div>
<!-- </div> -->
</div>
</div>

</div>

        <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap.min.js"></script>


            <script type="text/javascript">
            $(document).ready(function() {
           $('#example').DataTable( {
           "order": [[ 0, "desc" ]]
           });
          });
            </script>
            <script>


            //       $('#create_tile').on('show.bs.modal', function () {
            //
            //        });
                    $(document).on("click", ".tile_create", function () {
                        var id=$(this).data('id');
                        $("#native_id").val( id );
                        var mobile=$("#mobile").val();
                        $("#btnSubmit").attr("disabled", false);

                    });
                    function np_create_tile(){
                        $("#d_msg_s").html('');
                        $("#d_msg_e").html('');
                        $("#btnSubmit").attr("disabled", true);
                        var mobile=$("#mobile").val();
                        var id=$("#native_id").val();
                         $.ajax({
                            url: '{{ url('dynamic_template/quiz_create_tile') }}',
                            type: 'get',
                            data:{mobile:mobile,id:id},
                            success: function (data) {
                                if(data.toString()==1){
                                    $("#d_msg_s").html('Tile created');
                                }else{
                                    $("#d_msg_e").html('Invalid mobile or user name');
                                    $("#btnSubmit").attr("disabled", false);
                                }
            //                    styleload();
                            },
                            complete: function () {

                            },
                            error: function (data) {

                            },
                            timeout: 120000,
                        });
                    }


            </script>
            @stop
  <!-- </body>
</html> -->
