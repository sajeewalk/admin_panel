<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use DB;
use Session;
use Config;

Session_start();

class HomeController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
//        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return view('home');
    }

    public function post_login(Request $request) {
//        print_r($request->all());
//        die;
//        echo $password = Hash::make('123456');
        if (Auth::attempt(['user_name' => $request->user_name, 'password' => $request->password], true)) {
            // Authentication passed...
//            print_r(Auth::check());
//            echo 'logged in';
//            die;
            $admin_group = [1];
            $farlin_group = ['ce7ec81d-c8f9-2d77-fca7-5dee1e275be6', '5d8fe13f-fe6b-d4dc-5777-5bb44326b397', '4c9ba901-1612-5261-0c18-5d9ae6c37ba6'];
            $user = Auth::user();
            if (in_array($user->id, $farlin_group)) {
                $company = 'farlin_program_subscription';
                return redirect()->intended('promotion/form?company=' . $company);
            }
            if ($user->role == 'pharmacy_admin' || $user->role == 'pharmacy_operator') {
                return redirect()->intended('pharmacy');
            }
            if ($user->role == 'appointment_admin' || $user->role == 'appointment_hemas') {
                return redirect()->intended('appointments');
            } 
            if ($user->role == 'onboard_admin' || $user->role == 'onboard_hemas') {
                return redirect()->intended('onboard');
            } 
            if ($user->role == 'bangladesh_admin' || $user->role == 'bangladesh_hemas') {
                return redirect()->intended('bangladesh');
            }            
            return redirect()->intended('/');
//            return Auth::user();
        } else {
            return Redirect::back()->withErrors(['msg' => 'Invalid credentials']);
        }
    }

    public function logout() {
        Auth::logout();
        return redirect()->intended('login');
    }

    public function savedata(Request $req) {
        $data = array();
        $data['type'] = $req->name;
        $data['description'] = $req->discription;
        $data['group_intro'] = $req->gr_intro;
        $data['Intro_title'] = $req->Introduction_title;
        $data['intro_text'] = $req->Introduction_Text;
        $data['group_ending'] = $req->group_ending;
        $data['ending_heading'] = $req->ending_heading;
        $data['ending_text'] = $req->ending_text;
        $data['intro_image'] = $req->intro_image;
        $data['intro_video'] = $req->intro_video;
        $data['ending_image'] = $req->ending_image;
        $data['back_enabled'] = $req->back_enabled;
        $data['action'] = $req->action;
        $data['meta'] = $req->meta;
        DB::table('questionnaire')->insert($data);
        Session::put('exception', 'Added Successfully..!');
        return Redirect::to('/Questionnaire_Form');
        // $image=$req->file('intro_image');
        // if($image)
        // {
        // $image_name=str_random(20)."_".time();
        // $ext=strtolower($image->getClientOriginalExtension());
        // $image_full_name=$image_name.'.'.$ext;
        // $upload_path= 'images/';
        // $image_url=$upload_path.$image_full_name;
        // $success=$image->move($upload_path,$image_full_name);
        // if($success)
        // {
        //
      //  $data['intro_image'] = Config::get('app.global_image_url')."/".$image_url;
        // }
        //
      // }
    }

}
