<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Validator;
use App\Repositories\QuizRepository;
use Auth;
use Illuminate\Support\Facades\Hash;

class QuizController extends Controller {

    private $post;

    public function __construct(QuizRepository $post) {
        $this->middleware('auth');
        $this->post = $post;
    }

    public function quiz_welcome(Request $request) {
        $result = $this->post->quiz_welcome($request);
        return $result;
    }
    public function Questionnaire_Form(Request $request) {
        $result = $this->post->Questionnaire_Form($request);
        return $result;
    }

    public function Questionnaire_update_form(Request $request) {
        $result = $this->post->Questionnaire_update_form($request);
        return $result;
    }

    public function Quiz_table(Request $request) {
        $result = $this->post->Quiz_table($request);
        return $result;
    }

    public function test(Request $request) {
        $result = $this->post->test($request);
        return $result;
    }

    public function Questionnaire_edit($id) {
        $result = $this->post->Questionnaire_edit($id);
        return $result;
    }

    public function getdata($id) {
        $result = $this->post->getdata($id);
        return $result;
    }

    public function subquiz($id) {
        $result = $this->post->subquiz($id);
        return $result;
    }

    public function create_tile(Request $request) {
        $result = $this->post->create_tile($request);
        return $result;
    }


}
