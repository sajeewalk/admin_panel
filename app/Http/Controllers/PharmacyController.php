<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Validator;
use App\Repositories\PharmacyRepository;
use Auth;
use Illuminate\Support\Facades\Hash;

class PharmacyController extends Controller {

    private $post;

    public function __construct(PharmacyRepository $post) {
        $this->middleware('auth');
        $this->post = $post;
    }

    public function pharmacy(Request $request) {
        $result = $this->post->pharmacy($request);
        return $result;
    }

    public function assign($prescription, $user) {
        $result = $this->post->assign($prescription, $user);
        return $result;
    }

    public function history($prescription) {
        $result = $this->post->history($prescription);
        return $result;
    }

    public function status($prescription,$status) {
        $result = $this->post->status($prescription,$status);
        return $result;
    }    
}
