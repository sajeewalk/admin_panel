<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Validator;
use App\Repositories\IndexRepository;
use Auth;
use Illuminate\Support\Facades\Hash;

class IndexController extends Controller {

    private $post;

    public function __construct(IndexRepository $post) {
        $this->middleware('auth');
        $this->post = $post;
    }

    public function dashboard(Request $request) {
        $result = $this->post->dashboard($request);
        return $result;
    }

    public function dynamic_template(Request $request) {
        $result = $this->post->dynamic_template($request);
        return $result;
    }

    public function dynamic_template_edit($id) {
        $result = $this->post->dynamic_template_edit($id);
        return $result;
    }

    public function dynamic_template_submit(Request $request) {
        $result = $this->post->dynamic_template_submit($request);
        return $result;
    }

    public function create_folder(Request $request) {
        $result = $this->post->create_folder($request);
        return $result;
    }

    public function image_library(Request $request, $folder = '') {
        $result = $this->post->image_library($request, $folder);
        return $result;
    }

    public function image_library_popup(Request $request,$folder='') {
        $result = $this->post->image_library_popup($request,$folder);
        return $result;
    }

    public function preview_template(Request $request) {
        $result = $this->post->preview_template($request);
        return $result;
    }

    public function nativepost_table(Request $request) {
        $result = $this->post->nativepost_table($request);
        return $result;
    }

    public function all_programs(Request $request) {
        $result = $this->post->all_programs($request);
        return $result;
    }

    public function create_program(Request $request) {
        $result = $this->post->create_program($request);
        return $result;
    }

    public function post_create_program(Request $request) {
        $result = $this->post->post_create_program($request);
        return $result;
    }

    public function program_page($id) {
        $result = $this->post->program_page($id);
        return $result;
    }

    public function program_page_post($id, Request $request) {
        $result = $this->post->program_page_post($id, $request);
        return $result;
    }

    public function dynamic_template_edit_save(Request $request) {
        $result = $this->post->dynamic_template_edit_save($request);
        return $result;
    }

    public function create_tile(Request $request) {
        $result = $this->post->create_tile($request);
        return $result;
    }

    public function list_native_post(Request $request) {
        $result = $this->post->list_native_post($request);
        return $result;
    }

    public function list_select_question(Request $request) {
        $result = $this->post->list_select_question($request);
        return $result;
    }

    public function list_chat(Request $request) {
        $result = $this->post->list_chat($request);
        return $result;
    }

    public function add_empty_row($id) {
        $result = $this->post->add_empty_row($id);
        return $result;
    }

    public function del_row($program_id, $seq_id) {
        $result = $this->post->del_row($program_id, $seq_id);
        return $result;
    }

    public function tags_save($program_id, Request $request) {
        $result = $this->post->tags_save($program_id, $request);
        return $result;
    }


    public function dynamic_template_welcome(Request $request) {
        $result = $this->post->dynamic_template_welcome($request);
        return $result;
    }
    
    public function get_farlin_form(Request $request) {
        $result = $this->post->get_farlin_form($request);
        return $result;
    } 
    public function post_farlin_form(Request $request) {
        $result = $this->post->post_farlin_form($request);
        return $result;
    }
    public function farlin(Request $request) {
        return redirect('login');
    }    
}
