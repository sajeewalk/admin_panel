<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Validator;
use App\Repositories\BangladeshRepository;
use Auth;
use Illuminate\Support\Facades\Hash;

class BangladeshController extends Controller {

    private $post;

    public function __construct(BangladeshRepository $post) {
        $this->middleware('auth');
        $this->post = $post;
    }

    public function bangladesh(Request $request) {
        $result = $this->post->bangladesh($request);
        return $result;
    }
    public function download(Request $request) {
        $result = $this->post->download($request);
        return $result;
    }    
     public function subscriptions(Request $request) {
        $result = $this->post->subscriptions($request);
        return $result;
    }  
}
