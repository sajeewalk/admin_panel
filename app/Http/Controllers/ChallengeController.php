<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Repositories\ChallengeRepository;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use App\Questionnaire;
use DB;
use Session;
use Config;

Session_start();

class ChallengeController extends Controller
{
private $post;

public function __construct(ChallengeRepository $post) {
$this->middleware('auth');
$this->post = $post;
}

public function table_billboard(Request $request){
    $result = $this->post->table_billboard($request);
    return $result;

}

public function create_billbord_add(Request $request){
$result = $this->post->create_billbord_add_new($request);
return $result;
}

public function getbillbord_add($id){
$result = $this->post->getbillbord_add($id);
return $result;
}

public function challenge(Request $request) {
$result = $this->post->challenge($request);
return $result;
}

public function open_challenge_dashboard(Request $request){
$result = $this->post->open_challenge_dashboard($request);
return $result;
}

public function open_create_add_page(Request $request){
$result = $this->post->open_create_add_page($request);
return $result;
}

public function image_library(Request $request, $folder = '') {
$result = $this->post->image_library($request, $folder);
return $result;
}
public function create_folder(Request $request) {
$result = $this->post->create_folder($request);
return $result;
}

public function dynamic_challenge_submit(Request $request) {
$result = $this->post->dynamic_challenge_submit($request);
return $result;
}

public function challenge_billboard($challenge_id){

$bilbords_locations = file_get_contents("challenge_json/{$challenge_id}_billboards.json");
$billboard_path = json_decode($bilbords_locations,true);
// echo "<pre>"; print_r($billboard_path);die;
$checksize = count($billboard_path);
return $checksize;

}

/**
* Show the application dashboard.
*
* @return \Illuminate\Http\Response
*/



}
