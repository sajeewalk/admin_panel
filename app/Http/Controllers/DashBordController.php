<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Questionnaire;
use App\Questionas;
use App\queid;
use App\test;
use Illuminate\Support\Facades\Redirect;
use DB;
use Session;
use Config;
Session_start();

class DashBordController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     // public function __construct()
     // {
     //     $this->middleware('auth');
     // }


    public function indexdash()
    {
      $questionnaireone = Questionnaire::all();
      return view('Quiz_table',[
        "questionnaire" => $questionnaireone
      ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function edit($id)
     {
 $questionnaire = DB::select('select * from questionnaire where id=?',[$id]);
 return view ('Questionnaire_update_form',['questionnaire'=>$questionnaire]);
     }

     public function editqustion($id)
     {
      $questions = DB::select('select * from questions where id=?',[$id]);
      $questionnaire = queid::all();
      $que = test::where('questionnaire_id',$questions[0]->questionnaire_id)->get();

 return view ('questiona_update',
[
   "questions"=>$questions,
   "que" => $que,
   "questionnaire"=> $questionnaire
])->with('title', 'quiz');}

public function newqustion()
{

return view ('newquiz');

}

    public function update(Request $req, $id)
    {
      $data=array();
      $data['type']=$req->name;
      $data['description']=$req->discription;
      $data['group_intro']=$req->group_intro;
      $data['Intro_title']=$req->Introduction_title;
      $data['intro_text']=$req->Introduction_Text;
      $data['group_ending']=$req->group_ending;
      $data['ending_heading']=$req->ending_heading;
      $data['ending_text']=$req->ending_text;
      $data['intro_image']=$req->intro_image;
      $data['intro_video']=$req->intro_video;
      $data['ending_image']=$req->ending_image;
      $data['back_enabled']=$req->back_enabled;
      $data['action']=$req->action;
      $data['meta']=$req->meta;

      DB::table('questionnaire')->where('id', $id)->update($data);
      Session::put('exception','Update Successfully..!');
      return Redirect::to('/Quiz_table');



    }

    public function savedataone(Request $req)
    {

         $option_array = array();
         $option_list = array();
         $data=array();
         if(!empty($req->option_name)){
         for($i=0; $i<sizeof($req->option_name);$i++){
          $option_list[] = $req->option_name[$i];

          $temp_array = array();
          $temp_array["id"] = $i;
          $temp_array["option"] = $req->option_name[$i];
          $temp_array["skip"] = (empty($req->skip[$i])?array():$req->skip[$i]);
          $temp_array["correct"] = $req->correct[$i];
          $temp_array["output_tile"] = $req->output_tile[$i];
          $temp_array["output_text"] = $req->output_text[$i];
          $temp_array["button_image"] = $req->button_image[$i];
          $temp_array["answer_image"] = $req->answer_image[$i];
          $temp_array["output_image"] = $req->output_image[$i];
          $temp_array["output_icon"] = $req->output_icon[$i];

          $option_array[] = $temp_array;
          }
        }

      $option_string = implode(",",$option_list);
      $final_json = json_encode($option_array);
      $data['question']=$req->question;
      $data['options']=$option_string;
      $data['options_new']=$final_json;
      $data['questionnaire_id']=$req->question_id;
      $data['type']=$req->type;
      $data['sex']=$req->sex;
      $data['question_type']=$req->question_type;
      $data['max_selection']=$req->max_selection;
      $data['hint_show']=$req->hint_show;
      $data['hint_action']=$req->hint_action;
      $data['hint_meta']=$req->hint_meta;
      $data['image']=$req->image;

      DB::table('questions')->insert($data);
      Session::put('exception','Added Successfully..!');
      return Redirect::to('/Quiz_table');
    }

    public function updateque(Request $req, $id)
    {
            // echo "<pre>";print_r($req->all());die;
           $option_array = array();
           $option_list = array();
           $data=array();
           if(!empty($req->option_name)){
           for($i=0; $i<sizeof($req->option_name);$i++){
            $option_list[] = $req->option_name[$i];

            $temp_array = array();
            $temp_array["id"] = $i;
            $temp_array["option"] = $req->option_name[$i];
            $temp_array["skip"] = (empty($req->skip[$i])?array():$req->skip[$i]);
            $temp_array["correct"] = $req->correct[$i];
            $temp_array["output_tile"] = $req->output_tile[$i];
            $temp_array["output_text"] = $req->output_text[$i];
            $temp_array["button_image"] = $req->button_image[$i];
            $temp_array["answer_image"] = $req->answer_image[$i];
            $temp_array["output_image"] = $req->output_image[$i];
            $temp_array["output_icon"] = $req->output_icon[$i];

            $option_array[] = $temp_array;
            }
          }
        $option_string = implode(",",$option_list);
        $final_json = json_encode($option_array);
        $data['question']=$req->question;
        $data['options']=$option_string;
        $data['options_new']=$final_json;
        $data['questionnaire_id']=$req->question_id;
        $data['type']=$req->type;
        $data['sex']=$req->sex;
        $data['question_type']=$req->question_type;
        $data['max_selection']=$req->max_selection;
        $data['hint_show']=$req->hint_show;
        $data['hint_action']=$req->hint_action;
        $data['hint_meta']=$req->hint_meta;
        $image=$req->file('image');
        $data['image']=$req->image;

        DB::table('questions')->where('id', $id)->update($data);
        Session::put('exception','Update Successfully..!');
        return Redirect::to('/Quiz_table');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $ques = queid::find($id);
      if($ques->delete){
        Session::put('exception','Delete Successfully..!');
        return Redirect::to('/dashboard');
      }
      else{
        Session::put('exception','Delete Not Successfully..!');
        return Redirect::to('/dashboard');

      }

    }
}
