<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Validator;
use App\Repositories\OnboardRepository;
use Auth;
use Illuminate\Support\Facades\Hash;

class OnboardController extends Controller {

    private $post;

    public function __construct(OnboardRepository $post) {
        $this->middleware('auth');
        $this->post = $post;
    }

    public function dashboard(Request $request) {
        $result = $this->post->dashboard($request);
        return $result;
    }

    public function onboard(Request $request) {
        $result = $this->post->onboard($request);
        return $result;
    }

    public function create(Request $request) {
        $result = $this->post->create($request);
        return $result;
    }

    public function post_create(Request $request) {
        $result = $this->post->post_create($request);
        return $result;
    }

    public function transactions(Request $request) {
        $result = $this->post->transactions($request);
        return $result;
    }

    public function old_payments(Request $request) {
        $result = $this->post->old_payments($request);
        return $result;
    }

    public function sessions(Request $request) {
        $result = $this->post->sessions($request);
        return $result;
    }
    public function post_sessions(Request $request) {
        $result = $this->post->post_sessions($request);
        return $result;
    }
}
