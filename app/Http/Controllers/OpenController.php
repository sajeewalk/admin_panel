<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Validator;
use App\Repositories\IndexRepository;
use Auth;
use Illuminate\Support\Facades\Hash;

class OpenController extends Controller {

    private $post;

    public function __construct(IndexRepository $post) {
        $this->post = $post;
    }

    public function create_tile_program(Request $request) {
        $result = $this->post->create_tile_program($request);
        return $result;
    }   
}
