<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Validator;
use App\Repositories\AppointmentRepository;
use Auth;
use Illuminate\Support\Facades\Hash;

class AppointmentController extends Controller {

    private $post;

    public function __construct(AppointmentRepository $post) {
        $this->middleware('auth');
        $this->post = $post;
    }

    public function view(Request $request) {
        $result = $this->post->view($request);
        return $result;
    }


}
