<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Authenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->guest()) {
            if ($request->ajax() || $request->wantsJson()) {
                return response('Unauthorized.', 401);
            } else {
                return redirect()->guest('login');
            }
        }
        $admin_group=[1];
        $farlin_group=['ce7ec81d-c8f9-2d77-fca7-5dee1e275be6','5d8fe13f-fe6b-d4dc-5777-5bb44326b397','4c9ba901-1612-5261-0c18-5d9ae6c37ba6'];
        $user=$request->user();
        if(in_array($user->id, $farlin_group)){    
            $url= request()->path();
            if($url=='promotion/form' || $url=='farlin'){
                return $next($request);
            }else{
                return response('Unauthorized. <a href="'.url('logout').'">Try again</a>', 401);
            }           
        }
//        if (Auth::user()->role == 'pharmacy_admin') {
//            route('xxxx');
//        }
        return $next($request);
        
    }
}
