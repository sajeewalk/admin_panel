<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Pharmacy {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null) {
        if (Auth::guard($guard)->guest()) {
            if ($request->ajax() || $request->wantsJson()) {
                return response('Unauthorized.', 401);
            } else {
                return redirect()->guest('login');
            }
        }        
        $user=$request->user();
        
        if ($user->role=='pharmacy_admin' || $user->role=='pharmacy_operator' || $user->role=='admin') {
            return $next($request);
        } else {
            return response('Unauthorized. <a href="'.url('logout').'">logout</a>', 401);
        }
        return $next($request);
    }

}
