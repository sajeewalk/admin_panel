<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the controller to call when that URI is requested.
  |
 */

//Route::get('/', function () {
//    return view('welcome');
//});
//Route::get('/','IndexController@index');
//Route::get('/index','IndexController@index');
//Route::post('/search','IndexController@search');
//Route::get('/sessions/{doctor_id}/{hospital_id}/{specilization_id}/{source}/{direct}','IndexController@sessions');
//Route::get('/sessions/{doctor_id}/{hospital_id}/{specilization_id}/{source}','IndexController@sessions');
//Route::get('/booking/{key}/','IndexController@booking');
//Route::post('/booking','IndexController@bookingCreate');
//Route::get('/logout', function () {
//    return view('index');
//});
//Route::get('admin', ['middleware' => 'auth', function() {
//
//    }]);
//Route::group(['middleware' =>  ['web', 'auth']], function () {
Route::group(['middleware' => ['admin']], function () {
    Route::get('/', 'IndexController@dashboard');
    Route::get('/dashboard', 'IndexController@dashboard');
    Route::get('/dynamic_template', 'IndexController@dynamic_template');
    Route::get('/dynamic_template/edit/{id}', 'IndexController@dynamic_template_edit');
    Route::post('/dynamic_template/edit/{id}', 'IndexController@dynamic_template_edit_save');
    Route::get('/dynamic_template/create_tile', 'IndexController@create_tile');
    Route::get('/dynamic_template/create_tile_program', 'OpenController@create_tile_program');
    Route::post('/dynamic_template/create_tile_program', 'OpenController@create_tile_program');
    Route::get('/dynamic_template_welcome', 'IndexController@dynamic_template_welcome');

//challenge
Route::get('/challenge', 'ChallengeController@challenge');
Route::get('/challenge/billboard/{id}', 'ChallengeController@challenge_billboard');
Route::post('/dynamic_challenge_submit', 'ChallengeController@dynamic_challenge_submit');
Route::get('/Quiz_name', 'ChallengeController@get_quiz_name');
Route::get('/challenge_dashboad', 'ChallengeController@open_challenge_dashboard');
Route::get('/create_billboards', 'ChallengeController@open_create_add_page');
Route::post('/create_billboard_vendors', 'ChallengeController@create_billbord_add');
Route::get('/get_billboarddetails/checkid/{id}', 'ChallengeController@getbillbord_add');
Route::get('billboard_table','ChallengeController@table_billboard');


//Quiz form route
    Route::get('/quiz_welcome', 'QuizController@quiz_welcome');
    Route::get('/Questionnaire_Form', 'QuizController@Questionnaire_Form');
    Route::get('/Quiz_table', 'QuizController@Quiz_table');
    Route::get('/test', 'QuizController@test');
    Route::get('/Questionnaire_update_form', 'QuizController@Questionnaire_update_form');
    Route::get('updatequestinnaire/{id}', 'QuizController@Questionnaire_edit');
    Route::get('/updatequestions/{id}', 'DashBordController@editqustion');
    Route::get('/quetions/{id}', 'QuizController@subquiz');
    Route::get('/postdata/{id}', 'QuizController@getdata');
    Route::get('/dynamic_template/quiz_create_tile', 'QuizController@create_tile');


    Route::post('/save_data', 'HomeController@savedata');
    Route::post('/update/{id}', 'DashBordController@update');
    Route::post('/save_option', 'DashBordController@savedataone');
    Route::post('/updateque/{id}', 'DashBordController@updateque');
//Route::post('/dynamic_template','IndexController@dynamic_template');


    Route::post('/dynamic_template_submit', 'IndexController@dynamic_template_submit');
    Route::get('/image_library', 'IndexController@image_library');
    Route::post('/image_library', 'IndexController@image_library');
    Route::post('/image_library/{folder}', 'IndexController@image_library');
    Route::get('/image_library/popup', 'IndexController@image_library_popup');
    Route::get('/image_library/popup/{folder}', 'IndexController@image_library_popup');
    Route::get('/image_library/{folder}', 'IndexController@image_library');
    Route::post('/preview_template', 'IndexController@preview_template');
    Route::get('/nativepost_table', 'IndexController@nativepost_table');
    Route::get('/all_programs', 'IndexController@all_programs');
    Route::get('/create_program', 'IndexController@create_program');
    Route::post('/create_program', 'IndexController@post_create_program');
    Route::get('/program_page/{id}', 'IndexController@program_page');
    Route::post('/program_page/{id}', 'IndexController@program_page_post');
    Route::get('/list/native_post', 'IndexController@list_native_post');
    Route::get('/list/select_question', 'IndexController@list_select_question');
    Route::get('/list/chat', 'IndexController@list_chat');
    Route::get('/add_empty_row/{id}', 'IndexController@add_empty_row');
    Route::get('/del_row/{program_id}/{seq_id}', 'IndexController@del_row');
    Route::post('/create_folder', 'IndexController@create_folder');

    Route::post('/tags/save/{program_id}', 'IndexController@tags_save');
});


Route::group(['middleware' => ['promotion']], function () {
    Route::get('promotion/form', 'IndexController@get_farlin_form');
    Route::post('promotion/form', 'IndexController@post_farlin_form');
    Route::get('farlin', 'IndexController@farlin');
});


Route::group(['middleware' => ['pharmacy']], function () {
    Route::get('pharmacy', 'PharmacyController@pharmacy');
    Route::get('pharmacy/assign/{prescription}/{user}', 'PharmacyController@assign');
    Route::get('pharmacy/history/{prescription}', 'PharmacyController@history');
    Route::get('pharmacy/status/{prescription}/{status}', 'PharmacyController@status');
});

Route::group(['middleware' => ['appointment']], function () {
    Route::get('appointments', 'AppointmentController@view');
});

Route::group(['middleware' => ['onboard']], function () {
    Route::get('onboard', 'OnboardController@dashboard');
    Route::get('onboard/list', 'OnboardController@onboard');
    Route::get('onboard/create', 'OnboardController@create');
    Route::post('onboard/create', 'OnboardController@post_create');
    Route::get('onboard/transactions', 'OnboardController@transactions');
    Route::get('onboard/payments', 'OnboardController@old_payments');
    Route::get('onboard/sessions', 'OnboardController@sessions');
    Route::post('onboard/sessions', 'OnboardController@post_sessions');
});

Route::group(['middleware' => ['bangladesh']], function () {
    Route::get('bangladesh', 'BangladeshController@bangladesh');
    Route::get('bangladesh/download', 'BangladeshController@download');
    Route::get('bangladesh/subscriptions', 'BangladeshController@subscriptions');
});

Route::auth();
//Route::get('/home', 'HomeController@index');
Route::post('/post_login', 'HomeController@post_login');
Route::get('/logout', 'HomeController@logout');
