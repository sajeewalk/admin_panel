<?php

if (!function_exists('success_response')) {
    /**
     * Make return array format
     *
     * @param  string $data Json
     * @param  integer $result number
     * @return array
     */
    function success_response($result = null,$data = null)
    {
        return ['result'=>$result,'data'=>$data];
    }
}
if (!function_exists('error_response')) {
    /**
     * Make return array format
     *
     * @param  string $error Json
     * @param  integer $result Number
     * @return array
     */
    function error_response($result,$error)
    {
        return ['result'=>$result,'error'=>$error];
    }
}
if (!function_exists('extract_user_id')) {
    /**
     * Make return array format
     *
     * @param  string $error Json
     * @param  integer $result Number
     * @return array
     */
    function extract_user_id($token)
    {
        // if(explode(' ',trim($token))[0] === "AuthUser"):
        //     $encStr = explode(' ',trim($token))[1];
        //     return doDecrypt($encStr)[0];
        // else:
            $decode = \Tymon\JWTAuth\Facades\JWTAuth::getPayload($token)->toArray();
            return $decode['id'];
        // endif;
    }
}
if (!function_exists('image_rename')) {
    /**
     * Make an unique name for image store
     *
     * @param  string $error Json
     * @param  integer $result Number
     * @return array
     */
    function image_rename($ext) // $ext = extension 
    {
        $length = 10;
        $rand_string =  substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyz', ceil($length/strlen($x)) )),1,$length);
        return time().'_'.$rand_string.'.'.$ext;
    }
}

if (!function_exists('get_user_profile_picture_url')) {
    /**
     * Make an unique name for image store
     *
     * @param  string $error Json
     * @param  integer $result Number
     * @return array
     */
    function get_user_profile_picture_url($userId) // $ext = extension 
    {
        $url = "https://livehappy.ayubo.life/resize.php?img=/datadrive/wellness_upload/upload/user_img/".$userId;
        return $url;
    }
}

if(!function_exists('get_doctors_speciality')){
      /**
     * Get doctors specializaton name
     *
     * @param  string $error Json
     * @param  integer $result Number
     * @return array
     */
    function get_doctors_speciality($key = null)
    {
        $arr = [
            '' => 'Any',
            'Clinical_Psychology' => 'Clinical Psychology',
            'dental' => 'Dentist',
            'Fitness_Trainer' => 'Fitness Trainer',
            'General_Practitioner' => 'General Practitioner',
            'Nutritionist' => 'Nutritionist',
            'physical_instructor' => 'Fitness Expert',
            'Physician' => 'Physician',
            'Sports_Scientist' => 'Sports Scientist',
            'Surgeon' => 'Surgeon',
            'MO' => 'Medical Officer',
            'MOCG' => 'MO, Clinical Genetician',
            'Physiotherapist' => 'Physiotherapist',  
            'Psychologist' => 'Psychologist',  
            'Dietician' => 'Dietician', 
            'Senior_Nutritionist' => 'Senior Nutritionist',
            'Senior_Dietician' => 'Senior Dietician', 
            'Consultant_Physician' => 'Consultant Physician',  
            'Clinical_Psychologist' => 'Clinical Psychologist', 
            'Test_your_video_call' => 'Test your video call', 
            'Nutrition_consultant' => 'Nutrition consultant',  
            'Nutritionist_Sports_and_Exercise' => 'Nutritionist (Sports and Exercise)',
            'Mindful_Practitioner' => 'Mindful Practitioner',
            'Yoga_Relaxation_counsellor' => 'Yoga & Relaxation Counsellor ',
            'Sport_Exercise_Physician' => 'Sport & Exercise Physician',  
            'Family_Physician' => 'Primary Care / Family Medicine'
        ];

        //FamilyPhysician
        //^Clinical_Psychology^,^Consultant_Endocrinologist^

        $specialities_array = explode(',',$key);

        $specialities_string = "";
        $count = count($specialities_array);
        $i = 1;
        foreach($specialities_array as $speciality):
            $string = str_replace("^","",$speciality);
            $string = str_replace("_"," ",$speciality);

            if($count == 1){
                $specialities_string = $string;
            }else{
                $specialities_string .= ($count == $i)?$string:$string." / ";
            }

            $i++;
        endforeach;

        return $specialities_string;

        // if($key == "" || $key == null):
        //     return "";
        // else:
        //     if(isset($arr[$key])){
        //         return $arr[$key];
        //     }else{
        //         return "";
        //     };
        // endif;
    }

    
}

if(!function_exists('doctor_logo_show')){
      /**
     * If doctor enter "VIP" word into appointment note 
     * if "VIP" exist - don't show the logo in prescription/investigation/refferal
     * @param  string $error Json
     * @param  integer $result boolean
     * @return array
     */
    function doctor_logo_show($appointment = null)
    {
        if(!is_null($appointment)){
            $paragraph = $appointment->description;
            $paragraph = strtolower(str_replace( array( "&", "!", '"', ".", "'", ",", "?", "\r", "\n" ), '', $paragraph));
            $paragraph = explode(" ", $paragraph);
            
            $result = in_array("vip",$paragraph);

            return $result;
        }

            return true; // if true logo is not showing
    }

    
}

if(!function_exists('get_age_from_bday')){

    function get_age_from_bday($bday = null)
    {
        if(!is_null($bday)){

            $date = DateTime::createFromFormat("Y-m-d", $bday);
          
            $year = Carbon\Carbon::create($date->format("Y"));

            $now = Carbon\Carbon::now();
    
            return (int)$year->diffInYears($now);
        }

            return (int)0; 
    }

}

if(!function_exists('doEncrypt')){

    //if you want to add multiple value add with quama Ex: doEncrypt('255,d4E45R55fde');
    function doEncrypt($plaintext)
    {

        $key="Cm2rS";
       
        $ivlen = openssl_cipher_iv_length($cipher="AES-128-CBC");
        $iv = openssl_random_pseudo_bytes($ivlen);
        $ciphertext_raw = openssl_encrypt($plaintext, $cipher, $key, $options=OPENSSL_RAW_DATA, $iv);
        $hmac = hash_hmac('sha256', $ciphertext_raw, $key, $as_binary=true);
        $ciphertext = base64_encode( $iv.$hmac.$ciphertext_raw );

        return $ciphertext;
        
       
    }

}

if(!function_exists('doDecrypt')){

    function doDecrypt($ciphertext)
    {
        $key="Cm2rS";

        $c = base64_decode($ciphertext);
        $ivlen = openssl_cipher_iv_length($cipher="AES-128-CBC");
        $iv = substr($c, 0, $ivlen);
        $hmac = substr($c, $ivlen, $sha2len=32);
        $ciphertext_raw = substr($c, $ivlen+$sha2len);
        $original_plaintext = openssl_decrypt($ciphertext_raw, $cipher, $key, $options=OPENSSL_RAW_DATA, $iv);
        $calcmac = hash_hmac('sha256', $ciphertext_raw, $key, $as_binary=true);
        if (hash_equals($hmac, $calcmac))//PHP 5.6+ timing attack safe comparison
        {
            $original_plaintext;
        }

        $arr = explode(',',$original_plaintext);

        return $arr;

    }

}

if(!function_exists('getAppId')){
//get app object from provided key
    function getAppId($key)
    {
        $brand_app = App\Models\BrandApp::where('key',$key)->first();

        if(!is_null($brand_app)):
            return $brand_app->id;
        endif;
            return NULL;

    }

}

if(!function_exists('getPricing')){
    //get defalt price of any item
    function getPricing($related_id = Null ,$item_type = Null)
    {
        if(!is_null($related_id ) && !is_null($item_type)){
            $item_master = App\Models\Payments\ItemMaster::where('related_id',$related_id)->where('item_type',$item_type)->first();

            if(!is_null($item_master) && $item_master->prices->count() > 0):
                foreach($item_master->prices as $price):
                    return $price->currency->display_name." ".$price->default_price;
                endforeach;
            endif;

            return "-";
        }

        return "related id and item type are required";
    }
}






