<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\User;
use DB;

class ProgramDashboardMaster extends Model {

    use SoftDeletes;

    protected $table = 'program_dashboard_master';

}
