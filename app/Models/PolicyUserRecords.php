<?php

namespace App\Models;

use App\Models\PolicyUserMaster;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Tags;

class PolicyUserRecords extends Model {

    use SoftDeletes;

    protected $table = 'policy_user_records';
    protected $fillable = ['parameter_id', 'value', 'uom', 'policy_user_master_id', 'target_start_date', 'target_end_date','target_year'];

    public function policy_user_master() {
        return $this->hasOne(PolicyUserMaster::class, 'id', 'policy_user_master_id');
    }
    public function tag() {
        return $this->hasOne(Tags::class, 'id', 'tag_id');
    }
}
