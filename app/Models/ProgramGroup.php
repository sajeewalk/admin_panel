<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\User;
use DB;
use Carbon\Carbon;
use App\Models\Program;
use App\Models\ProgramGroupMapping;

class ProgramGroup extends Model {

    use SoftDeletes;

    protected $table = 'program_group';
    protected $fillable = ['user_id','name','assigned_type', 'assigned_id','policy_user_master_id','status','start_date','last_sequence','pause_date','subscription_type','telco_app_id','logo_url'];

//    public function programs() {
//        return $this->belongsToMany(Program::class, 'program_group_mapping', 'program_group_id', 'program_id');
//    } 

    public function program_group_mapping() {
        return $this->hasMany(ProgramGroupMapping::class, 'program_id', 'id');
    }    
}
