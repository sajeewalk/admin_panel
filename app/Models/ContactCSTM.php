<?php
namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\User;
use App\Models\Contact;
use DB;
use Carbon\Carbon;

class ContactCSTM extends Model
{
    public $incrementing = false;
    public $timestamps = false;
    protected $table = 'contacts_cstm';

   public function contact()
   {
       return $this->hasOne(Contact::class,'id','id_c');
   }

}