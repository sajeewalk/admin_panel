<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\User;
use DB;
use Carbon\Carbon;
use App\Models\Tags;
use App\Models\ProgramSequenceMaster;
use App\Models\ProgramGroup;
use App\Models\PolicyUserInfo;

class Program extends Model {

    use SoftDeletes;

    protected $table = 'program';
    protected $fillable = ['name','type','hide', 'decription','default_offset','dash_board_image','program_image','tile_image','intro_action','action_id','created_by_id'];

    public function tags() {
        return $this->belongsToMany(Tags::class, 'program_tags', 'program_id', 'tag_id');
    }

    public function program_sequence_master() {
        return $this->belongsToMany(ProgramSequenceMaster::class, 'program_sequence_master_mapping', 'program_id', 'program_sequence_master_id');
    }

    public function program_groups() {
        return $this->belongsToMany(ProgramGroup::class, 'program_group_mapping', 'program_id', 'program_group_id');
    }
    
    public function policy_user_info() {
        return $this->belongsToMany(PolicyUserInfo::class, 'policy_user_subscribed_programs', 'program_id', 'policy_user_info_id');
    }
}
