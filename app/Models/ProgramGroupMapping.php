<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\User;
use DB;
use Carbon\Carbon;
use App\Models\Questions;

class ProgramGroupMapping extends Model {

    use SoftDeletes;

    protected $table = 'program_group_mapping';
    protected $fillable = ['program_group_id','program_id', 'offset'];
}
