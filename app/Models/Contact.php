<?php
namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\User;
use App\Models\MedicationPrescriptionItem;
use App\Models\ContactCSTM;
use DB;
use Carbon\Carbon;
use App\Models\ProgramSequenceMaster;

class Contact extends Model
{
    public $incrementing = false;
    public $timestamps = false;
    protected $table = 'contacts';

   public function cstm()
   {
    return $this->hasOne(ContactCSTM::class,'id_c','id');

   }
   
    public function program_sequence_master() {
        return $this->belongsToMany(ProgramSequenceMaster::class, 'program_sequence_master_contact', 'contact_id', 'program_sequence_master_id');
    }

    public function user() {
        return $this->hasOne(User::class,'id','assigned_user_id');
    }    
}