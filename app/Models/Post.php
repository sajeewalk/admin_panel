<?php
namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\PostLike;
use App\Models\PostComment;
use App\Models\PostMedia;
use App\User;
use DB;

class Post extends Model
{
    use SoftDeletes;
    protected $dateFormat = 'U';

    public function media()
    {
        return $this->hasMany(PostMedia::class,'post_id','id');
    }

    public function likesCount()
    {
        return $this->hasMany(PostLike::class,'post_id','id')->count();
    }

    public function commentsCount()
    {
        return $this->hasMany(PostComment::class,'post_id','id')->count();
    }

    public function likeText($auth_user_id)
    {
        
        $liked_user_names = DB::table('post_likes')
                            ->join('users','users.id','=','post_likes.user_id')
                            ->select(DB::raw('CONCAT(users.first_name," ",users.last_name) as name'),'users.id')
                            ->where('post_likes.post_id','=',$this->id)
                            ->orderBy('post_likes.created_at','desc')
                            ->get();
                            $likedUsersText = [];

                          

        //Liked users text

        $i=0;
        $isLiked = false;
        foreach($liked_user_names as $liked_user){
            if($liked_user->id != $auth_user_id){
               
                $likedUsersText[] =$liked_user->name;
            }else{
                
                $isLiked = true;
            }
        $i++;
        }

        if($isLiked):
            if($this->likesCount() == 1){
                $likedUsersText = "You";
            }elseif($this->likesCount() == 2){
                $likedUsersText = "You and ".$likedUsersText[0];
            }elseif($this->likesCount() == 3){
                $likedUsersText = "You,".$likedUsersText[0]." and ".$likedUsersText[1];
            }elseif($this->likesCount() > 3){
                $likedUsersText = "You,".$likedUsersText[0].",".$likedUsersText[1]." and ".($this->likesCount()-3)." others";
            }else{

            }
        else:
           
            if($this->likesCount() == 1){
                $likedUsersText =  $likedUsersText[0];
               
            }elseif($this->likesCount() == 2){
                $likedUsersText = $likedUsersText[0]." and ".$likedUsersText[1];
               
            }elseif($this->likesCount() > 2){
                $likedUsersText = $likedUsersText[0].",".$likedUsersText[1]." and ".($this->likesCount()-2)." others";
            }
        endif;

        return ["isLiked"=>$isLiked, "text"=>empty($likedUsersText)?"":$likedUsersText];
        
    }

    public function getCreateByUsers($query, $created_by_id)
    {
        return $query->where('created_by', $created_by_id)->get();
    }

   
}