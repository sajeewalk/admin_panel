<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\User;
use DB;
use Carbon\Carbon;
use App\Models\PolicyUserInfo;
use App\Models\PolicyUserRecords;
use App\Models\PolicyMaster;
use App\Models\Payments\UserPaymentMethod;

class PolicyUserMaster extends Model {

    public $timestamps = false;
    protected $table = 'ins_policy_user_master';

//    public function cstm()
//    {
//     return $this->hasOne(ContactCSTM::class,'id_c','id');
//    }
    public function user() {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function policy_user_info() {
        return $this->hasMany(PolicyUserInfo::class, 'policy_user_master_id', 'id');
    }

    public function policy_user_records() {
        return $this->hasMany(PolicyUserRecords::class, 'policy_user_master_id', 'id');
    }
    public function policy_master() {
        return $this->belongsTo(PolicyMaster::class, 'policy_master_id', 'id');
    }

    public function payment_methods(){
        return $this->hasMany(UserPaymentMethod::class, 'user_id', 'user_id');
    }

    public function default_payment_method(){
        return $this->payment_methods()->where('default',1)
                                        ->where('status','active')
                                        ->first();
    }
}
