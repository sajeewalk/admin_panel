<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\User;
use DB;
use Carbon\Carbon;
use App\Models\Questions;

class ProgramTags extends Model {

    use SoftDeletes;

    protected $table = 'program_tags';
    protected $fillable = ['tag_id', 'program_id'];

}
