<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\User;
use DB;
use Carbon\Carbon;
use App\Models\Program;

class Tags extends Model {

    use SoftDeletes;

    protected $table = 'tags';

    public function programs() {
        return $this->belongsToMany(Program::class,'program_tags', 'tag_id', 'program_id');
    }    
}
