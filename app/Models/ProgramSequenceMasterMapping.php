<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\User;
use DB;
use Carbon\Carbon;
use App\Models\Questions;

class ProgramSequenceMasterMapping extends Model {

    use SoftDeletes;

    protected $table = 'program_sequence_master_mapping';

}
