<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\User;
use DB;
use Carbon\Carbon;
use App\Models\Program;
use App\Models\Contact;

class ProgramSequenceMaster extends Model {

    use SoftDeletes;

    protected $table = 'program_sequence_master';
    protected $fillable = ['program_id','type', 'sequence','timeline_header','timeline_text','timeline_image_link','timeline_video_link','inner_content_type','inner_content','tile','tile_image_link','tile_text','notification_header','notification_text','post_time','relate_id','relate_text','category','male','female','general','under_weight','normal_weight','over_weight','obese','low_bp','ideal_blood_pressure','high_blood_pressure','low_hdl','high_ldl','high_tga','low','normal','pre_diabatic','diabatic','sedentary','moderately_active','active','do_not_take_part_in_challenge','tag_name_1','tag_icon_1','tag_name_2','tag_icon_2','tag_name_3','tag_icon_3','points','content_icon','sub_heading'];

    public function programs() {
        return $this->belongsToMany(Program::class,'program_sequence_master_mapping','program_sequence_master_id','program_id');
    }

    public function contacts() {
        return $this->belongsToMany(Contact::class,'program_sequence_master_contact','program_sequence_master_id','contact_id');
    }     
}
