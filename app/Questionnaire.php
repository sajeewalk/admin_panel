<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Questionas;


class Questionnaire extends Model
{


  protected $table = 'questionnaire';
  protected $fillable = ['id','type','description','group_intro','Intro_title','
intro_text','intro_image','intro_video','group_ending','ending_heading','ending_text','ending_image'];


public function question_relationship()
   {
       return $this->hasMany('App\Questionas','questionnaire_id','id');
   }


}
