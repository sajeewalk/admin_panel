<?php
namespace App\Interfaces;

interface PostCommentInterface {

    public function add_new_comment($post_id,$params);

    public function delete_comment($post_id,$comment_id,$request);
}