<?php
namespace App\Interfaces;

interface PostInterface {

    public function create_normal_post($params);

    public function post_like($post_id,$params);
}