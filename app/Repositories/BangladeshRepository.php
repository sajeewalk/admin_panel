<?php

namespace App\Repositories;

use Illuminate\Support\Facades\View;
use App\Interfaces\BangladeshInterface;
use Illuminate\Support\Facades\DB;

class BangladeshRepository implements BangladeshInterface {

    public function bangladesh($request) {
//        $contacts = DB::table('contacts')
//                ->join('contacts_cstm', 'contacts.id', '=', 'contacts_cstm.id_c')
//                ->join('users', 'contacts.assigned_user_id', '=', 'users.id')
//                ->orderBy('contacts.date_entered', 'DESC')
//                ->select('contacts.*', 'contacts_cstm.*', 'users.user_name')
//                ->paginate(20);
        return View::make('bangladesh_welcome')
//                        ->with('data', $contacts)
                        ->with('title', 'Welcome');
    }

    public function download($request) {
        $contacts = DB::table('users')
                ->join('users_cstm', 'users.id', '=', 'users_cstm.id_c')
                ->leftJoin('email_addr_bean_rel', 'users.id', '=', 'email_addr_bean_rel.bean_id')
                ->leftJoin('email_addresses', 'email_addr_bean_rel.email_address_id', '=', 'email_addresses.id')
                ->where('ban_device_id_c','!=', '')
                ->whereNotNull('ban_device_id_c')
                ->where('users.deleted','=', 0)
                 ->orderBy('users.date_entered', 'DESC')
                ->select('users.first_name','users.last_name', "users_cstm.user_dob_c", 'users.user_name','users_cstm.user_gender_c','users.user_name','users_cstm.ban_device_id_c','users.date_entered','email_addresses.email_address','users_cstm.user_nic_c')
                ->paginate(20);
        return View::make('bangladesh_download')
                        ->with('data', $contacts)
                        ->with('title', 'Download');
    }

     public function subscriptions($request) {
        $contacts = DB::table('users')
                ->join('users_cstm', 'users.id', '=', 'users_cstm.id_c')
                ->join('subscription_payments', 'subscription_payments.user_id', '=', 'users.id')
                ->leftJoin('email_addr_bean_rel', 'users.id', '=', 'email_addr_bean_rel.bean_id')
                ->leftJoin('email_addresses', 'email_addr_bean_rel.email_address_id', '=', 'email_addresses.id')
                ->where('ban_device_id_c','!=', '')
                ->whereNotNull('ban_device_id_c')
                ->where('users.deleted','=', 0)
                ->where('subscription_payments.relate_id','=', '5')
                ->where('subscription_payments.relate_type','=', '11')
                ->where('subscription_payments.subscription_status','=', 'active')
                ->orderBy('users.date_entered', 'DESC')
                ->select('users.first_name','users.last_name', "users_cstm.user_dob_c", 'users.user_name','users_cstm.user_gender_c','users.user_name','users_cstm.ban_device_id_c','users.date_entered','email_addresses.email_address','users_cstm.user_nic_c')
                ->paginate(20);
        return View::make('bangladesh_subscriptions')
                        ->with('data', $contacts)
                        ->with('title', 'Subscriptions');
    }

}
