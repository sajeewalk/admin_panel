<?php

namespace App\Repositories;

use Illuminate\Support\Facades\View;
use App\Interfaces\OnboardInterface;
use Illuminate\Support\Facades\DB;
use Webpatser\Uuid\Uuid;
use App\User;
use App\Models\Contact;

class OnboardRepository implements OnboardInterface {

    public function dashboard($request) {

        return View::make('onboard_dashboard')
                        ->with('title', 'Experts');
    }

    public function onboard($request) {
        $contacts = DB::table('contacts')
                ->join('contacts_cstm', 'contacts.id', '=', 'contacts_cstm.id_c')
                ->join('users', 'contacts.assigned_user_id', '=', 'users.id')
                ->orderBy('contacts.date_entered', 'DESC')
                ->select('contacts.*', 'contacts_cstm.*', 'users.user_name')
                ->get();
        return View::make('onboard')
                        ->with('data', $contacts)
                        ->with('title', 'Experts');
    }

    public function create($request) {
        $specials = self::get_specializations();
        $wellness_centers = self::get_wellness_centers();

        return View::make('create_expert')
                        ->with('specials', $specials)
                        ->with('wellness_centers', $wellness_centers)
                        ->with('title', 'Create Expert');
    }

    public function post_create($request) {
        print_r($request->all());

        $mobile = $request->assigned_user_name;
        $user = User::where('user_name', 'like', '%' . $mobile . '%')->where('deleted', '=', 0)->first();
        if (!$user) {
            return redirect()->back()->withErrors(['create_program' => 'User not registered.']);
        }

        $uuid = Uuid::generate()->string;
        $date = date('Y-m-d H:i:s');
        $assigned_user_id = $user->id;
        $salutation = $request->salutation;
        $first_name = $request->first_name;
        $last_name = $request->last_name;
        $phone_mobile = $request->phone_mobile;
        $specialization_c = $request->specialization_c;
        $slmc_no_c = $request->slmc_no_c;
        $qualification_and_destinatio_c = $request->qualification_and_destinatio_c;
        $pc_wellness_center_contacts_1pc_wellness_center_ida = $request->wellness_center;

        DB::statement("update users_cstm set verified_c=1 where id_c='{$user->id}'");

        $base64 = '';
        if (!empty($_FILES['signature']['name'])) {
            $file_name = $_FILES['signature']['name'];
            //   $file_name =$_FILES['image']['tmp_name'];
//        $file_ext = strtolower(end(explode('.', $file_name)));


            $file_size = $_FILES['signature']['size'];
            $file_tmp = $_FILES['signature']['tmp_name'];


            $type = pathinfo($file_tmp, PATHINFO_EXTENSION);
            $data = file_get_contents($file_tmp);
            $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
        }
        DB::statement("INSERT INTO `contacts` (`id`, `date_entered`, `date_modified`, `assigned_user_id`, `salutation`, `first_name`, `last_name`, `phone_mobile`,`modified_user_id`) VALUES ('$uuid', '$date', '$date', '$assigned_user_id', '$salutation', '$first_name', '$last_name', '$phone_mobile', '1')");
        DB::statement("INSERT INTO `contacts_cstm` (`id_c`, `specialization_c`, `slmc_no_c`, `qualification_and_destinatio_c`,signature_c) VALUES ('$uuid', '^$specialization_c^', '$slmc_no_c', '$qualification_and_destinatio_c', '$base64')");

        $wid = Uuid::generate()->string;
        DB::statement("INSERT INTO `pc_wellness_center_contacts_1_c` (`id`, `date_modified`, `pc_wellness_center_contacts_1pc_wellness_center_ida`, `pc_wellness_center_contacts_1contacts_idb`) VALUES ('$wid', '$date', '$pc_wellness_center_contacts_1pc_wellness_center_ida', '$uuid')");

        foreach ($request->brand_apps as $brand_app) {
            DB::statement("INSERT INTO `contact_brand_app` (`brand_apps_id`, `contacts`) VALUES ('$brand_app', '$uuid')");
        }

        //service type
        $sid = Uuid::generate()->string;
        $duration = $request->duration;
        $fee_c = $request->fee;
        DB::statement("INSERT INTO `pc_service_type` (`id`, `name`, `date_entered`, `modified_user_id`) VALUES ('$sid', 'Channeling', '$date', '1')");
        DB::statement("INSERT INTO `pc_service_type_cstm` (`id_c`, `duration_c`, `sort_order_c`, `fee_c`,pc_wellness_center_id_c,contact_id_c,wipro_appointment_c) VALUES ('$sid', '$duration','1','$fee_c', '$pc_wellness_center_contacts_1pc_wellness_center_ida','$uuid','0')");
        $ssid = Uuid::generate()->string;
        DB::statement("INSERT INTO `contacts_pc_service_type_1_c` (`id`, `date_modified`, `contacts_pc_service_type_1contacts_ida`, `contacts_pc_service_type_1pc_service_type_idb`) VALUES ('$ssid', '$date', '$uuid', '$sid')");

        //price update
        foreach ($request->brand_apps as $brand_app) {
            if ($brand_app == 1 || $brand_app == 2) {
                $doctor_id = $uuid;
                $item_name = $salutation . ' ' . $first_name . ' ' . $last_name;
                $service_id = 'cff136b0-5201-cbc3-8331-5df76240d9af';
                $related_type = '7';
                $item_status = 1;
                $app_id = $brand_app;
                $amount = $fee_c;

                $created_at = $date;

                $sql = "INSERT INTO  billing_item_master(related_id,item_name,service_id,item_status,item_type,created_at,updated_at) VALUES ('$doctor_id','$item_name','$service_id','$item_status','$related_type','$created_at','$created_at')";
                DB::statement($sql);

//        $billing_item_master_id = $GLOBALS['db']->getOne("SELECT MAX(id) FROM  billing_item_master");
                $sugar_programs = DB::select("SELECT MAX(id) as id FROM  billing_item_master");
                $billing_item_master_id = $sugar_programs[0]->id;


                $related_id = $doctor_id;
                $currency_id = 1;

                $sql = "INSERT INTO  pmnt_service_payments(related_type,related_id,currency_id,app_id,created_at,updated_at) VALUES ('$related_type','$related_id','$currency_id','$app_id','$created_at','$created_at')";
                DB::statement($sql);
//        $service_payment_id = $GLOBALS['db']->getOne("SELECT MAX(id) FROM  pmnt_service_payments");
                $sugar_programs = DB::select("SELECT MAX(id) as id FROM  pmnt_service_payments");
                $service_payment_id = $sugar_programs[0]->id;

                $frequency = 'onetime';
                $instance_charge = 1;

                $instance_charge = 1;

                $sql = "INSERT INTO  pmnt_service_payment_frequencies(service_payment_id,frequency,amount,instance_charge,currency_id,created_at,updated_at) VALUES ('$service_payment_id','$frequency','$amount','$instance_charge','$currency_id','$created_at','$created_at')";
                DB::statement($sql);
//            $pmnt_service_payment_frequencies_id1 = $GLOBALS['db']->getOne("SELECT MAX(id) FROM  pmnt_service_payment_frequencies");
                $sugar_programs = DB::select("SELECT MAX(id) as id FROM  pmnt_service_payment_frequencies");
                $pmnt_service_payment_frequencies_id1 = $sugar_programs[0]->id;

                $sql = "INSERT INTO  pmnt_service_payment_frequencies(service_payment_id,frequency,amount,instance_charge,currency_id,created_at,updated_at) VALUES ('$service_payment_id','$frequency','$amount','$instance_charge','$currency_id','$created_at','$created_at')";
                DB::statement($sql);
//        $pmnt_service_payment_frequencies_id2 = $GLOBALS['db']->getOne("SELECT MAX(id) FROM  pmnt_service_payment_frequencies");
                $sugar_programs = DB::select("SELECT MAX(id) as id FROM  pmnt_service_payment_frequencies");
                $pmnt_service_payment_frequencies_id2 = $sugar_programs[0]->id;


                $payment_method_id = 1;
                $sql = "INSERT INTO  pmnt_service_payment_source_frequencies(service_payment_frequency_id,payment_method_id,payment_method_type,currency_id,created_at,updated_at) VALUES ('$pmnt_service_payment_frequencies_id1','$payment_method_id','$frequency','$currency_id','$created_at','$created_at')";
                DB::statement($sql);
                $payment_method_id = 4;
                $sql = "INSERT INTO  pmnt_service_payment_source_frequencies(service_payment_frequency_id,payment_method_id,payment_method_type,currency_id,created_at,updated_at) VALUES ('$pmnt_service_payment_frequencies_id2','$payment_method_id','$frequency','$currency_id','$created_at','$created_at')";
                DB::statement($sql);

                $effective_date = date('Y-m-d');
                $effective_date = date("Y-m-d", strtotime($effective_date . "-1 day"));
                $payment_method_id = 1;
                $default_price = 1;
                $sql = "INSERT INTO  billing_item_price_master(item_code,institution_fee,effective_date,frequency,currency_id,app_id,payment_method_id,default_price,created_at,updated_at) VALUES ('$billing_item_master_id','$amount','$effective_date','$frequency','$currency_id','$app_id','$payment_method_id','$default_price','$created_at','$created_at')";
                DB::statement($sql);
                $payment_method_id = 4;
                $sql = "INSERT INTO  billing_item_price_master(item_code,institution_fee,effective_date,frequency,currency_id,app_id,payment_method_id,default_price,created_at,updated_at) VALUES ('$billing_item_master_id','$amount','$effective_date','$frequency','$currency_id','$app_id','$payment_method_id','$default_price','$created_at','$created_at')";
                DB::statement($sql);


                $status = 'active';
                $sql = "INSERT INTO  billing_item_master_apps(item_master_id,app_id,status,created_at,updated_at) VALUES ('$billing_item_master_id','$app_id','$status','$created_at','$created_at')";
                DB::statement($sql);
            }
            if ($brand_app == 6) {
                $doctor_id = $uuid;
                $item_name = $salutation . ' ' . $first_name . ' ' . $last_name;
                $service_id = 'cff136b0-5201-cbc3-8331-5df76240d9af';
                $related_type = '7';
                $item_status = 1;
                $app_id = $brand_app;
                $amount = $fee_c;

                $created_at = $date;

                $sql = "INSERT INTO  billing_item_master(related_id,item_name,service_id,item_status,item_type,created_at,updated_at) VALUES ('$doctor_id','$item_name','$service_id','$item_status','$related_type','$created_at','$created_at')";
                DB::statement($sql);

                $sugar_programs = DB::select("SELECT MAX(id) as id FROM  billing_item_master");
                $billing_item_master_id = $sugar_programs[0]->id;


                $related_id = $doctor_id;
                $currency_id = 10;

                $sql = "INSERT INTO  pmnt_service_payments(related_type,related_id,currency_id,app_id,created_at,updated_at) VALUES ('$related_type','$related_id','$currency_id','$app_id','$created_at','$created_at')";
                DB::statement($sql);

                $sugar_programs = DB::select("SELECT MAX(id) as id FROM  pmnt_service_payments");
                $service_payment_id = $sugar_programs[0]->id;


                $frequency = 'onetime';
                $instance_charge = 1;

                $instance_charge = 1;

                $sql = "INSERT INTO  pmnt_service_payment_frequencies(service_payment_id,frequency,amount,instance_charge,currency_id,created_at,updated_at) VALUES ('$service_payment_id','$frequency','$amount','$instance_charge','$currency_id','$created_at','$created_at')";
                DB::statement($sql);
                $sugar_programs = DB::select("SELECT MAX(id) as id FROM  pmnt_service_payment_frequencies");
                $pmnt_service_payment_frequencies_id1 = $sugar_programs[0]->id;


                $payment_method_id = 3;
                $sql = "INSERT INTO  pmnt_service_payment_source_frequencies(service_payment_frequency_id,payment_method_id,payment_method_type,currency_id,created_at,updated_at) VALUES ('$pmnt_service_payment_frequencies_id1','$payment_method_id','$frequency','$currency_id','$created_at','$created_at')";
                DB::statement($sql);


                $effective_date = date('Y-m-d');
                $effective_date = date("Y-m-d", strtotime($effective_date . "-1 day"));
                $payment_method_id = 3;
                $default_price = 1;
                $sql = "INSERT INTO  billing_item_price_master(item_code,institution_fee,effective_date,frequency,currency_id,app_id,payment_method_id,default_price,created_at,updated_at) VALUES ('$billing_item_master_id','$amount','$effective_date','$frequency','$currency_id','$app_id','$payment_method_id','$default_price','$created_at','$created_at')";
                DB::statement($sql);


                $status = 'active';
                $sql = "INSERT INTO  billing_item_master_apps(item_master_id,app_id,status,created_at,updated_at) VALUES ('$billing_item_master_id','$app_id','$status','$created_at','$created_at')";
                DB::statement($sql);
            }
        }
//session update

        $todate = DATE("Y-m-d", strtotime($request->session_start));
        $enddate = DATE("Y-m-d", strtotime($request->session_end));
        $docId = $uuid;
        $locId = $pc_wellness_center_contacts_1pc_wellness_center_ida;

        $sessionArray = [];
        while (strtotime($todate) <= strtotime($enddate)) {

            $f1 = [];
            $f2 = [];
            $f3 = [];
            $f4 = [];
            $f5 = [];
            $f6 = [];
            $f7 = [];
            foreach ($request->from1 as $k => $from) {
                if (!empty($from)) {
                    $f1[] = ['from' => $todate . ' ' . $from, 'to' => $todate . ' ' . $request->to1[$k]];
                }
            }
            foreach ($request->from2 as $k => $from) {
                if (!empty($from)) {
                    $f2[] = ['from' => $todate . ' ' . $from, 'to' => $todate . ' ' . $request->to1[$k]];
                }
            }
            foreach ($request->from3 as $k => $from) {
                if (!empty($from)) {
                    $f3[] = ['from' => $todate . ' ' . $from, 'to' => $todate . ' ' . $request->to1[$k]];
                }
            }
            foreach ($request->from4 as $k => $from) {
                if (!empty($from)) {
                    $f4[] = ['from' => $todate . ' ' . $from, 'to' => $todate . ' ' . $request->to1[$k]];
                }
            }
            foreach ($request->from5 as $k => $from) {
                if (!empty($from)) {
                    $f5[] = ['from' => $todate . ' ' . $from, 'to' => $todate . ' ' . $request->to1[$k]];
                }
            }
            foreach ($request->from6 as $k => $from) {
                if (!empty($from)) {
                    $f6[] = ['from' => $todate . ' ' . $from, 'to' => $todate . ' ' . $request->to1[$k]];
                }
            }
            foreach ($request->from7 as $k => $from) {
                if (!empty($from)) {
                    $f7[] = ['from' => $todate . ' ' . $from, 'to' => $todate . ' ' . $request->to1[$k]];
                }
            }
            $sessionArray = [
                1 => $f1,
                2 => $f2,
                3 => $f3,
                4 => $f4,
                5 => $f5,
                6 => $f6,
                7 => $f7,
            ];

            $day_of_the_week = DATE("N", strtotime($todate));
            switch ($day_of_the_week) {
                case 1:
                    if (!empty($sessionArray[1])) {
                        $this->timeUpdate($docId, $locId, $sessionArray[1]);
                        break;
                    }
                    if (empty($sessionArray[0])) {
                        break;
                    }
                case 2:
                    if (!empty($sessionArray[2])) {
                        $this->timeUpdate($docId, $locId, $sessionArray[2]);
                        break;
                    }
                    if (empty($sessionArray[0])) {
                        break;
                    }
                case 3:
                    if (!empty($sessionArray[3])) {
                        $this->timeUpdate($docId, $locId, $sessionArray[3]);
                        break;
                    }
                    if (empty($sessionArray[0])) {
                        break;
                    }
                case 4:
                    if (!empty($sessionArray[4])) {
                        $this->timeUpdate($docId, $locId, $sessionArray[4]);
                        break;
                    }
                    if (empty($sessionArray[0])) {
                        break;
                    }
                case 5:
                    if (!empty($sessionArray[5])) {
                        $this->timeUpdate($docId, $locId, $sessionArray[5]);
                        break;
                    }
                    if (empty($sessionArray[0])) {
                        break;
                    }
                case 6:
                    if (!empty($sessionArray[6])) {
                        $this->timeUpdate($docId, $locId, $sessionArray[6]);
                        break;
                    }
                    if (empty($sessionArray[0])) {
                        break;
                    }
                case 7:
                    if (!empty($sessionArray[7])) {
                        $this->timeUpdate($docId, $locId, $sessionArray[7]);
                        break;
                    }
                    if (empty($sessionArray[0])) {
                        break;
                    }
                default:
                    if (!empty($sessionArray[0])) {
                        $this->timeUpdate($docId, $locId, $sessionArray[0]);
                        break;
                    }
                    echo 'skipped -- ';
                    break;
            }
            $todate = DATE("Y-m-d", strtotime("$todate +1 day"));
        }
        return redirect('onboard/create')->with('success', 'Expert successfully created.');

        die;
    }

    static function get_wellness_centers() {
        return array(
            'b2c1e932-7155-f733-1de4-5886ec9ec7c5' => 'Video Chat',);
    }

    static function get_specializations() {
        return array(
 '' => 'Any',
  'Clinical_Psychology' => 'Clinical Psychology',
  'dental' => 'Dentist',
  'dental_surgion' => 'Dental Surgeon',  
  'Fitness_Trainer' => 'Fitness Trainer',
  'General_Practitioner' => 'General Practitioner',
  'Nutritionist' => 'Nutritionist',
  'physical_instructor' => 'Fitness Expert',
  'Physician' => 'Physician',
  'FamilyPhysician' => 'Primary Care Physician/ Family Physician',  
  'Sports_Scientist' => 'Sports Scientist',
  'Surgeon' => 'Surgeon',
  'MO' => 'Medical Officer',
  'MOCG' => 'MO, Clinical Genetician',
  'Physiotherapist' => 'Physiotherapist',  
  'Psychologist' => 'Psychologist',  
  'Dietician' => 'Dietician', 
  'Senior_Nutritionist' => 'Senior Nutritionist',
  'Senior_Dietician' => 'Senior Dietician', 
  'Consultant_Physician' => 'Consultant Physician',  
  'Clinical_Psychologist' => 'Clinical Psychologist', 
  'Test_your_video_call' => 'Test your video call', 
  'Nutrition_consultant' => 'Nutrition consultant',  
  'Nutritionist_Sports_and_Exercise' => 'Nutritionist (Sports and Exercise)',
  'Mindful_Practitioner' => 'Mindful Practitioner',
  'Yoga_Relaxation_counsellor' => 'Yoga & Relaxation Counsellor ',
  'Sport_Exercise_Physician' => 'Sport & Exercise Physician',  
  'consultant_plastic' => 'Consultant Plastic & Reconstructive Surgeon ',
  'Consultant_Psychiatry' => 'Consultant Psychiatry', 
  'Psychiatry' => 'Psychiatry',   
  'Consultant_Endocrinologist' => 'Consultant Endocrinologist',   
  'Family_Medicine' => 'Family Medicine', 
  'Primary_Care' => 'Primary Care',
  'Consultant_Cardiologist' => 'Consultant Cardiologist',  
  'Family_Physician' => 'Family Physician', 
  'Consultant_Obstetrician_Gynaecologist' => 'Consultant Obstetrician Gynaecologist',  
  'Orthopedic_Surgeon' => 'Orthopedic Surgeon',  
  'Consultant_Pediatrician' => 'Consultant Pediatrician',
'Senior_Business_Manager' => 'Senior Business Manager',
'Field_Manager' => 'Field Manager',
'Medical_Deligate' => 'Medical Deligate',
'Kurunagala_Anuradapura' => 'Kurunagala-Anuradapura',
'Medical_Representative' => 'Medical Representative  ',
'Negambo' => 'Negambo',
'Kandy_Upcountry' => 'Kandy-Upcountry',
'Medical_Representative_EAST' => 'Medical Representative (EAST)',  
'Medical_Deligate_South' => 'Medical Deligate (South)', 
'Medical_Representative_col' => 'Medical Representative (Col 1-15)',  
'Marketing_Executive' => 'Marketing Executive',   
'Cardiologist' => 'Cardiologist', 
'Intern_Corporate_Activations' => 'Intern Corporate Activations',
'Consultant_Obstetrics_Gynaecologist' => 'Consultant Obstetrics & Gynaecologist',
'Clinical_Nurse' => 'Clinical Nurse',
'Medical_Practitioner' => 'Medical Practitioner', 
'Pediatrician' => 'Pediatrician', 
'Doctor_in_Medicine' => 'Doctor in Medicine(MD)Cuba,Master in Emergency medicine& pre-hospital care(Spain),SDMC - PIE-01-10-1-M-2',     
'Consultant_Rheumatology' => 'Consultant in Rheumatology & Medical  Rehabilitation', 
'Speech_and_language_therapist' => 'Speech and language therapist',  
'Audiologist' => 'Audiologist',
'Consultant_Respiratory_Physician' => 'Consultant Respiratory Physician', 
'Physical_Trainer' => 'Physical Trainer',
'Fitness_Consultant' => 'Fitness Consultant', 
'SMO' => 'Senior Medical Officer',
'Staff_Nurse' => 'Staff Nurse',
'MD_Anesthesiology' => 'MD Anesthesiology',  
'Consultant_Physician_and_Allergy_and_Immunology_Specialist' => 'Consultant Physician and Allergy and Immunology Specialist',    
    'Senior_Registrar_Community_pediatrics' => 'Senior Registrar Community pediatrics ',
    'Senior_Lecturer_Grade_I)' => 'Senior Lecturer (Grade I)',
    'Senior_Lecturer_Head_Department_of_Medical_Education_Developmental_Psychologist' => 'Senior Lecturer & Head, Department of Medical Education / Developmental Psychologist',
    'Speech_and_Language_Pathologist_Audiologist_Senior_Lecturer_Grade_II' => 'Speech and Language Pathologist/ Audiologist Senior Lecturer (Grade II)',
    'Audiologist_and_Senior_lecturer' => 'Audiologist and Senior lecturer',
    'Lecturer_Probationary_Audiologist' => 'Lecturer (Probationary)/ Audiologist',
    'Lecturer_Speech_and_Language_Therapy)' => 'Lecturer (Speech and Language Therapy)',
    'Speech_and_Language_Therapist_Visiting_Lecturer' => 'Speech and Language Therapist Visiting Lecturer',
    'Occupational_therapist' => 'Occupational therapist',
    'Educational_Therapists' => 'Educational Therapists',
    'Visiting_Lecturer' => 'Visiting Lecturer',
    'Consultant_Child_and_Adolescent_Psychiatrist' => 'Consultant Child and Adolescent Psychiatrist',
    'Special_Educator' => 'Special Educator',
    'Specialist_Urologist' => 'Specialist Urologist',
    'Consultant_Medicine' => 'Consultant Medicine',
    'Pediatric_Oncologist' => 'Pediatric Oncologist',
    'SPECIALIST_GYNECOLOGIST' => 'SPECIALIST GYNECOLOGIST',  
    'Doctor' => 'Doctor',
    'Consultant_Dermatologist' => 'Consultant Dermatologist',
    'Consultant_General_Surgeon' => 'Consultant General Surgeon',
    'Conssultant_Urologist_and_Kidney_Transplant_Surgeon' => 'Conssultant Urologist and Kidney Transplant Surgeon', 
    'Consultant_Surgeon_Cancer_Specialsit' => 'Consultant Surgeon (Cancer Specialsit)',
    'Consultant_Eye_Surgeon' => 'Consultant Eye Surgeon',
    'Consultant_Paediatrician' => 'Consultant Paediatrician',
    'Consultant_Urological_Surgeon' => 'Consultant Urological Surgeon',
    'Consultant_Nephrologist' => 'Consultant Nephrologist',
    'Resident_Consultant_Paediatrician' => 'Resident Consultant Paediatrician',
    'Consultant_Physician_General_Medicine_VP)' => 'Consultant Physician (General Medicine, VP)',
    'Consultant_Psychiatrists' => 'Consultant Psychiatrists',
    'Consultant_Rhumetology_and_Rehabilitation' => 'Consultant Rhumetology and Rehabilitation',
    'Consultant_in_Sexual_Medicine_and_Sexual_Health' => 'Consultant in Sexual Medicine and Sexual Health',
    'Consultant_Orthopaedic_Surgeon' => 'Consultant Orthopaedic Surgeon',
    'ehour_Fitness_Training' => '18 hour Fitness Training',  
    'Consultant_Chest_Physician' => 'Consultant Chest Physician',
    'Fitness_Training' => 'Fitness Training',
    'MO_Nutrition' => 'MO- Nutrition',
    'Consultant_Nutritionist' => 'Consultant Nutritionist',
    'General_Physician' => 'General Physician',
    'Consultant_in_Restorative_Dentistry' => 'Consultant in Restorative Dentistry',
    'Consultant_ENT_Surgeon' => 'Consultant ENT Surgeon',
    'Consultant_Restorative_Dentistry' => 'Consultant Restorative Dentistry',
    'Consultant_Neurologist' => 'Consultant Neurologist',
    'Specialist_Physician' => 'Specialist Physician',
    'Medical_Officer_Clinical_Genetics' => 'Medical Officer (Clinical Genetics)',
    'Medical_officer_Nutrition' => 'Medical officer (Nutrition)',
    'Dietitian_Nutritionist' => 'Dietitian & Nutritionist',
    'Consultant_Family_Physician' => 'Consultant Family Physician',
    'Consultant_venereologist' => 'Consultant venereologist',
    'Endocrinologist' => 'Endocrinologist',
    'SENIOR_MEDICAL_OFFICER_SURGERY' => 'SENIOR MEDICAL OFFICER (SURGERY)',
    'In_charge_wellness_center_and_health_educator' => 'In charge wellness center and health educator',
    'Gynaecologist' => 'Gynaecologist',
    'General_physician_Emergency_Medicine' => 'General physician, Emergency Medicine',
    'Obstetrition_and_gynecologist' => 'Obstetrition and gynecologist',
    'ENT_and_neuro_surgeon' => 'ENT and neuro surgeon',
    'General_physician_Emergency_Medicine' => 'General physician, Emergency Medicine',
    'Homoeopath' => 'Homoeopath',
    'Chest_Asthma_and_allergy_specialist' => 'Chest, Asthma and allergy specialist',
    'SHO_MEDICINE' => 'SHO - MEDICINE',
    'MO_RESPIRATORY_MEDICINE' => 'MO - RESPIRATORY MEDICINE',
    'MEDICINE_SHO' => 'MEDICINE - SHO',
    'MEDICINE_HO' => 'MEDICINE -  HO',
    'MBBS_SL' => 'MBBS (SL)',
    'MBBS_PERADANIYA' => 'MBBS (PERADANIYA)',
        );
    }

    static function timeUpdate($docId, $locId, $timeArray) {
        foreach ($timeArray as $time) {
            $toTime1 = $time['from'];
            $toTime1 = date('Y-m-d H:i:s', strtotime("-330 minutes", strtotime($toTime1)));
            $toTime2 = $time['to'];
            $toTime2 = date('Y-m-d H:i:s', strtotime("-330 minutes", strtotime($toTime2)));
            $generated_id = Uuid::generate()->string;
//            $GLOBALS["db"]->query("INSERT INTO pc_appointment_settings(id, name, date_entered, date_modified) VALUES ('$generated_id','Doctor Session Record Manual',now(),now())");
            DB::statement("INSERT INTO pc_appointment_settings(id, name, date_entered, date_modified) VALUES ('$generated_id','Doctor Session Record Manual',now(),now())");
//            $GLOBALS["db"]->query("INSERT INTO pc_appointment_settings_cstm(id_c, type_c, contact_id_c, duration_from_c, duration_to_c, duration_c, pc_wellness_center_id_c) VALUES ('$generated_id','Available','$docId','$toTime1','$toTime2','0','$locId')");
            DB::statement("INSERT INTO pc_appointment_settings_cstm(id_c, type_c, contact_id_c, duration_from_c, duration_to_c, duration_c, pc_wellness_center_id_c) VALUES ('$generated_id','Available','$docId','$toTime1','$toTime2','0','$locId')");
        }
    }

    public function transactions($request) {
        $contacts = DB::table('pmnt_notify')
                ->orderBy('pmnt_notify.created_at', 'DESC')
                ->get();
        return View::make('onboard_transactions')
                        ->with('data', $contacts)
                        ->with('title', 'Transactions');
    }

    public function old_payments($request) {
        $contacts = DB::table('al_payments')
                ->orderBy('al_payments.date_entered', 'DESC')
                ->get();
        return View::make('old_payments')
                        ->with('data', $contacts)
                        ->with('title', 'Payments');
    }

    public function sessions($request) {
        $contacts = DB::table('contacts')
                ->where('deleted', '=', 0)
                ->orderBy('first_name', 'ASC')
                ->get();
        $wellness_centers = self::get_wellness_centers();
        return View::make('create_sessions')
                        ->with('contacts', $contacts)
                        ->with('wellness_centers', $wellness_centers)
                        ->with('title', 'Sessions');
    }
    static function del_sessions($docId,$locId='') {  
//die; 
//        $docId = '75519724-4d85-d8a9-4706-5df729a84aa5';     

        $locId = 'b2c1e932-7155-f733-1de4-5886ec9ec7c5';
        $settings = DB::select("SELECT * FROM pc_appointment_settings_cstm WHERE contact_id_c='$docId' AND pc_wellness_center_id_c='$locId'"); //AND CONVERT_TZ(duration_from_c,'+00:00','+05:30') LIKE '%2018-07-05%' 
        foreach ($settings as $setting) {
            $sql2 = "delete FROM pc_appointment_settings_cstm WHERE id_c='" . $setting->id_c . "'";
            DB::statement($sql2);
            $sql2 = "delete FROM pc_appointment_settings WHERE id='" . $setting->id_c . "'";
            DB::statement($sql2);
        }
        return '';
    }
    public function post_sessions($request) {
//session update
        self::del_sessions($request->contact_id);
        $todate = DATE("Y-m-d", strtotime($request->session_start));
        $enddate = DATE("Y-m-d", strtotime($request->session_end));
        $docId = $request->contact_id;
        $locId = $request->wellness_center;

        $sessionArray = [];
        while (strtotime($todate) <= strtotime($enddate)) {

            $f1 = [];
            $f2 = [];
            $f3 = [];
            $f4 = [];
            $f5 = [];
            $f6 = [];
            $f7 = [];
            foreach ($request->from1 as $k => $from) {
                if (!empty($from)) {
                    $f1[] = ['from' => $todate . ' ' . $from, 'to' => $todate . ' ' . $request->to1[$k]];
                }
            }
            foreach ($request->from2 as $k => $from) {
                if (!empty($from)) {
                    $f2[] = ['from' => $todate . ' ' . $from, 'to' => $todate . ' ' . $request->to1[$k]];
                }
            }
            foreach ($request->from3 as $k => $from) {
                if (!empty($from)) {
                    $f3[] = ['from' => $todate . ' ' . $from, 'to' => $todate . ' ' . $request->to1[$k]];
                }
            }
            foreach ($request->from4 as $k => $from) {
                if (!empty($from)) {
                    $f4[] = ['from' => $todate . ' ' . $from, 'to' => $todate . ' ' . $request->to1[$k]];
                }
            }
            foreach ($request->from5 as $k => $from) {
                if (!empty($from)) {
                    $f5[] = ['from' => $todate . ' ' . $from, 'to' => $todate . ' ' . $request->to1[$k]];
                }
            }
            foreach ($request->from6 as $k => $from) {
                if (!empty($from)) {
                    $f6[] = ['from' => $todate . ' ' . $from, 'to' => $todate . ' ' . $request->to1[$k]];
                }
            }
            foreach ($request->from7 as $k => $from) {
                if (!empty($from)) {
                    $f7[] = ['from' => $todate . ' ' . $from, 'to' => $todate . ' ' . $request->to1[$k]];
                }
            }
            $sessionArray = [
                1 => $f1,
                2 => $f2,
                3 => $f3,
                4 => $f4,
                5 => $f5,
                6 => $f6,
                7 => $f7,
            ];

            $day_of_the_week = DATE("N", strtotime($todate));
            switch ($day_of_the_week) {
                case 1:
                    if (!empty($sessionArray[1])) {
                        $this->timeUpdate($docId, $locId, $sessionArray[1]);
                        break;
                    }
                    if (empty($sessionArray[0])) {
                        break;
                    }
                case 2:
                    if (!empty($sessionArray[2])) {
                        $this->timeUpdate($docId, $locId, $sessionArray[2]);
                        break;
                    }
                    if (empty($sessionArray[0])) {
                        break;
                    }
                case 3:
                    if (!empty($sessionArray[3])) {
                        $this->timeUpdate($docId, $locId, $sessionArray[3]);
                        break;
                    }
                    if (empty($sessionArray[0])) {
                        break;
                    }
                case 4:
                    if (!empty($sessionArray[4])) {
                        $this->timeUpdate($docId, $locId, $sessionArray[4]);
                        break;
                    }
                    if (empty($sessionArray[0])) {
                        break;
                    }
                case 5:
                    if (!empty($sessionArray[5])) {
                        $this->timeUpdate($docId, $locId, $sessionArray[5]);
                        break;
                    }
                    if (empty($sessionArray[0])) {
                        break;
                    }
                case 6:
                    if (!empty($sessionArray[6])) {
                        $this->timeUpdate($docId, $locId, $sessionArray[6]);
                        break;
                    }
                    if (empty($sessionArray[0])) {
                        break;
                    }
                case 7:
                    if (!empty($sessionArray[7])) {
                        $this->timeUpdate($docId, $locId, $sessionArray[7]);
                        break;
                    }
                    if (empty($sessionArray[0])) {
                        break;
                    }
                default:
                    if (!empty($sessionArray[0])) {
                        $this->timeUpdate($docId, $locId, $sessionArray[0]);
                        break;
                    }
                    echo 'skipped -- ';
                    break;
            }
            $todate = DATE("Y-m-d", strtotime("$todate +1 day"));
        }
        return redirect('onboard/sessions')->with('success', 'Sessions successfully created.');

        die;
    }

}
