<?php

namespace App\Repositories;

use Illuminate\Support\Facades\View;
use App\Interfaces\PharmacyInterface;
use Illuminate\Support\Facades\DB;

class PharmacyRepository implements PharmacyInterface {

    public function pharmacy($request) {
        $user_name = $request->q;
        if (auth()->user()->role == 'pharmacy_admin') {
            $orders = DB::table('pc_external_prescription')
                    ->join('pc_external_prescription_cstm', 'pc_external_prescription.id', '=', 'pc_external_prescription_cstm.id_c')
                    ->join('users', 'pc_external_prescription_cstm.user_id_c', '=', 'users.id')
                    ->where('pc_external_prescription.deleted', '=', 0)
                    ->where('pc_external_prescription.date_entered', '>', '2020-02-29')
                    ->where('users.user_name', 'like', '%' . $user_name . '%')
                    ->orderBy('pc_external_prescription.date_entered', 'DESC')
                    ->select('pc_external_prescription.*','pc_external_prescription_cstm.*')
                    ->paginate(20);
        } else {
            $orders = DB::table('pc_external_prescription')
                    ->join('pc_external_prescription_cstm', 'pc_external_prescription.id', '=', 'pc_external_prescription_cstm.id_c')
                    ->join('users', 'pc_external_prescription_cstm.user_id_c', '=', 'users.id')
                    ->where('pc_external_prescription.deleted', '=', 0)
                    ->where('pc_external_prescription.date_entered', '>', '2020-02-29')
                    ->where('pc_external_prescription.assigned_user_id', '=', auth()->user()->id)
                    ->where('pc_external_prescription.status', '<>', 'Completed')
                    ->where('users.user_name', 'like', '%' . $user_name . '%')
                    ->orderBy('pc_external_prescription.date_entered', 'DESC')
                    ->select('pc_external_prescription.*','pc_external_prescription_cstm.*')
                    ->paginate(20);
        }
        return View::make('pharmacy')
                        ->with('data', $orders)
                        ->with('title', 'Dashboard');
    }

    public function assign($prescription, $user) {
        if (!empty($prescription) && !empty($user)) {
            DB::table('pc_external_prescription')->where('id', $prescription)->update(array(
                'assigned_user_id' => $user,
                'status' => 'Assigned',
            ));
            //precription history update
            $pres = DB::table('pc_external_prescription')->where('id', $prescription)->first();
            DB::table('pc_external_prescription_status')->insert(
                    ['prescription_id' => $pres->id,
                        'old_status' => $pres->status,
                        'new_status' => 'Assigned',
                        'user' => auth()->user()->first_name . ' ' . auth()->user()->last_name,
                        'date' => date('Y-m-d H:i:s'),
                    ]
            );
            $target_url = "https://livehappy.ayubo.life/index.php?entryPoint=janashakthi_service";
            $post = array('type' => 'prescription_mail', 'prescription_id' => $prescription);
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $target_url);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
            $result = curl_exec($ch);
            curl_close($ch);
            echo '1';
            die;
        }

        echo '2';
        die;
    }

    public function history($prescription) {
        $pres = DB::table('pc_external_prescription_status')->where('prescription_id', $prescription)->orderBy('date', 'DESC')->get();
        echo "<table class='table'>";
        echo "<tr><th>Status</th><th>User</th><td>Date</th></tr>";
        foreach ($pres as $p) {
            echo "<tr><td>{$p->new_status}</td><td>{$p->user}</td><td>{$p->date}</td></tr>";
        }
        if (count($pres) == 0) {
            echo "<tr><td colspan=4>No changes done.</td></tr>";
        }
        echo "</table>";
        die;
    }

    public function status($prescription, $status) {
        if (!empty($prescription) && !empty($status)) {
            DB::table('pc_external_prescription')->where('id', $prescription)->update(array(
                'status' => $status,
            ));
            //precription history update
            $pres = DB::table('pc_external_prescription')->where('id', $prescription)->first();
            DB::table('pc_external_prescription_status')->insert(
                    ['prescription_id' => $pres->id,
                        'old_status' => $pres->status,
                        'new_status' => $status,
                        'user' => auth()->user()->first_name . ' ' . auth()->user()->last_name,
                        'date' => date('Y-m-d H:i:s'),
                    ]
            );
            echo '1';
            die;
        }
        echo '2';
        die;
    }

}
