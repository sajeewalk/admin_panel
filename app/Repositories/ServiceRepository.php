<?php

namespace App\Repositories;

use App\Models\Post;
use DB;
use App\Models\PostLike;
use Intervention\Image\Facades\Image;
use App\Models\PostMedia;
use App\User;
use App\Models\PostCommunity;
use App\Models\PostTaggedUser;
use App\Models\CommunityUser;
use App\Models\Community;
use Illuminate\Support\Facades\Storage;
use App\Interfaces\IndexInterface;
use Illuminate\Support\Facades\View;
use App\Interfaces\ServiceInterface;
use Illuminate\Support\Facades\Cache;

class ServiceRepository implements ServiceInterface {

    const USER_ID = '9f4aa4a4-0c80-8bfd-ba41-5894069edb38';

    public function ayuboRequest($method, $data) {
        $curl = curl_init();
        $data = json_encode($data, 1);
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://livehappy.ayubo.life/custom/service/v4_1_custom/rest.php",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"method\"\r\n\r\n$method\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"input_type\"\r\n\r\nJSON\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"response_type\"\r\n\r\nJSON\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"rest_data\"\r\n\r\n$data\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--",
            CURLOPT_HTTPHEADER => array(
                "Postman-Token: c4cd54bf-cc34-4e11-ade3-0316434333da",
                "app_id: hemas_app",
                "cache-control: no-cache",
                "content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW",
                "password: password",
                "username: e_channeling_integration"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            return $response;
        }
    }

    public function getHospitals() {
        $hospitals = Cache::get('hospitals');
        if (empty($hospitals)) {
            return $this->setHospitals();
        }
        return $hospitals;
    }

    public function setHospitals() {
        $data = [self::USER_ID, ''];
        $hospitals = $this->ayuboRequest('chanellingLocation', $data);
        $hospitals = json_decode($hospitals, 1);
        $hospitals = $hospitals['data'];
        Cache::put('hospitals', $hospitals, 43200);
        return $hospitals;
    }

    public function getDoctors() {
        $doctors = Cache::get('doctors');
        if (empty($doctors)) {
            return $this->setDoctors();
        }
        return $doctors;
    }

    public function setDoctors() {
        $data = [self::USER_ID, ''];
        $doctors = $this->ayuboRequest('chanellingDoctor', $data);
        $doctors = json_decode($doctors, 1);
        $doctors = $doctors['data'];
        Cache::put('doctors', $doctors, 43200);
        return $doctors;
    }

    public function getSpecializations() {
        $specializations = Cache::get('specializations');
        if (empty($specializations)) {
            return $this->setSpecializations();
        }
        return $specializations;
    }

    public function setSpecializations() {
        $data = [self::USER_ID, ''];
        $specializations = $this->ayuboRequest('chanellingSpecialization', $data);
        $specializations = json_decode($specializations, 1);
        $specializations = $specializations['data'];
        Cache::put('specializations', $specializations, 43200);
        return $specializations;
    }

    public function search($request) {
        $date = $request->date;
        if (!empty($date)) {
            $date = str_replace('/', '-', $date);
            $date = date('Y-m-d', strtotime($date));
        }
        $data = [self::USER_ID, $request->doctor_id, $request->hospital_id, $request->specialization_id, $date];

        $search = $this->ayuboRequest('chanellingSearch', $data);
        $search = json_decode($search, 1);
        if (isset($search['data'])) {
            $search = $search['data'];
        } else {
            $search = [];
        }
        return $search;
    }

    public function sessions($request) {
        $data = [self::USER_ID, $request->doctor_id, $request->hospital_id, $request->specilization_id, unserialize($request->source), $request->direct];

        $sessions = $this->ayuboRequest('chanellingSessions', $data);
        $sessions = json_decode($sessions, 1);
        if (isset($sessions['data'])) {
            $sessions = $sessions['data'];
        } else {
            $sessions = [];
        }
        return $sessions;
    }

    public function book($request) {
        $data = [self::USER_ID, $request->patient, $request->session];

        $sessions = $this->ayuboRequest('chanellingBooking', $data);
        $sessions = json_decode($sessions, 1);
        if (isset($sessions['data'])) {
            $sessions = $sessions['data'];
        } else {
            $sessions = [];
        }

        return $sessions;
    }

}
