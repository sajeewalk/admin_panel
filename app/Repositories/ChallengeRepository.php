<?php
namespace App\Repositories;

use App\Repositories\ProgramRepository;
use App\Interfaces\ChallengeInterface;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Webpatser\Uuid\Uuid;
use App\Questionnaire;
use App\Billboard;
use Auth;
use DirectoryIterator;

class ChallengeRepository implements ChallengeInterface {

      // public $dir_path = "C:/xampp7/htdocs/native_admin/admin_panel/public/uploads";
      //  public $dir_path = "C:/xampp/htdocs/challenge/public/uploads";

      //   public $dir_path = "/var/www/html/admin_panel/public/uploads";
      //public $challenge_path = "C:/xampp/htdocs/admin_panel/public/public/challenge_data_folder";

      public function challenge($request) {
      $questionnaireone = Questionnaire::all();
      $billboard_vendors = Billboard::all();
      return View::make('challenge',[
      "questionnaire" => $questionnaireone,
      "billbord" => $billboard_vendors])
      ->with('title', 'challenge');
      }

      public function open_challenge_dashboard($request){
       
      return View::make('challenge_dashboad')
      ->with('title', 'challenge dashboad');
      }

      public function table_billboard($request)
      {
        $billboard_vendor = Billboard::all();
        return View::make('billboard_table',[
          "billbords" => $billboard_vendor
        ])
        ->with('title','challenge');

      }

      public function open_create_add_page($request){
      return View::make('create_billboards')
      ->with('title', 'create_billboards');
      }

      public function create_billbord_add_new($request){
      $data=[];
      $data['name'] = $request->name;
      $data['image'] = $request->image;
      $data['logo'] = $request->logo;
      $data['banner'] = $request->banner;
      $data['treasure_image'] = $request->image_treasure;
      $data['action'] = $request->action;
      $data['link'] = $request->link;
      if(isset($data)){
      DB::table('billboard_vendors')->insert($data);
      return redirect('create_billboards')->with('success', 'Billboards Add ' . $data['name'] . ' saved. #' . DB::getPdo()->lastInsertId());
      }


      }

      public function getbillbord_add($id){
      $data = Billboard::find($id);
      return response()->Json($data);
      }

      public function dynamic_challenge_submit($request) {

        $fields = $request->all();
        $gift = $request->all();
        $uuid = (string) Uuid::generate(4);

        $welcome = [];
        $shareimage = [];
        $treasures = [];
        $gifts = [];
        $gifts_list =[];
        $billboard_list=[];
        $billboard_vender_details=[];
        $treasures_vender_details=[];
        $quiz_list=[];
        $quizdatalist=[];
        $leaderboard_data=[];
        $entity = [];


        if (isset($fields['field'])) {
        $req_data = $fields['field'];
        //  echo "<pre>"; print_r($billbord_path);die;
        $welcome["Title"] = $req_data["Title"];
        $welcome["image"] = $req_data["image_banner"];
        $welcome["body"] = $req_data["body"];

        $shareimage["pending"] = $req_data["pending"];
        $shareimage["completed"] =$req_data["completed"];
        $shareimage["ongoing"] =$req_data["ongoing"];

        if(isset($req_data['lost_heading'])){
        $treasures['lost_heading'] = $req_data['lost_heading'];
        $treasures['lost_paragraph'] = $req_data['lost_paragraph'];
        $treasures['probability'] = (int)$req_data['probability'];
        $treasures['raffle_draw_entries'] = $req_data['raffle_draw_entries'];
        }
    if(isset($req_data['type'])){
        $leaderboard_data["type"] = $req_data["type"];
        $leaderboard_data["heading"] =$req_data["heading"];
        $leaderboard_data["text"] =$req_data["text"];
        $leaderboard_data["icon"] =$req_data["icon"];
        $leaderboard_data["action"] =$req_data["leaderboard_action"];
        $leaderboard_data["meta"] = $uuid;
        $leaderboard_data["image"] =$req_data["image_leaderboard"];
      }
        // $billboard_vender_details['billboard_vendors_min'] = (int)$req_data["billboard_vendors_min"];

        $challenge_id = $req_data["challenge"];
        $bilboardpath = file_get_contents("challenge_json/{$challenge_id}_billboards.json");
        $giftlocation = file_get_contents("challenge_json/{$challenge_id}_giftlocation.json");
        $questionnairelocation = file_get_contents("challenge_json/{$challenge_id}_questionnairelocation.json");
        $route = file_get_contents("challenge_json/{$challenge_id}_route.json");

        $giftlocation_path = json_decode($giftlocation,true);
        $billbord_path = json_decode($bilboardpath,true);
        $quiz_path = json_decode($questionnairelocation,true);
        $rout_path = json_decode($route,true);

        if(isset($req_data['points_value'])){
        $quiz_list['points_value'] = (int)$req_data['points_value'];
        $quiz_list['questionnaire'] = $req_data['questionnaire'];
        $quiz_list['list'] = $quiz_path;
        }

        //  echo "<pre>"; print_r($req_data);die;

        foreach ($billbord_path as $billbord_data){
        if(isset($req_data['collected_img'])){
        $temp_array_b = [];
        $temp_array_b["id"] = $billbord_data['id'];
        $temp_array_b["steps_limit"] = $billbord_data['steps_limit'];
        $temp_array_b["Lat"] = $billbord_data['Lat'];
        $temp_array_b["Long"] = $billbord_data['Long'];
        $temp_array_b["collected_img"] = $req_data['collected_img'];
        $temp_array_b["meta"] = (int)$req_data['meta_billboard'];
        $temp_array_b["action"] = $req_data['action_billboard'];
        $billboard_list[] = $temp_array_b;
        }
        }
        }

        $all_billboard = [];
        $all_treasure_vendors =[];
        if(isset($gift["billboard"])){
        for($i=0;$i<sizeof($gift["billboard"]);$i++){
        $billboard__adds_list = [];
        $treasures__adds_list =[];
        $billboard_id = $gift['billboard'][$i];
        $billboard_impression['impressions'] = $gift['impressions'][$i];
        $data = Billboard::find($billboard_id);

        $uuid_id = (string) Uuid::generate(4);
        $uuid_treasures = (string) Uuid::generate(4);

        $temp_data_billboard=[];
        $temp_data_billboard["id"] =  $uuid_treasures;
        $temp_data_billboard["name"] = $data['name'];
        $temp_data_billboard["image"] = $data['image'];
        $temp_data_billboard["logo"] = $data['logo'];
        $temp_data_billboard["banner"] = $data['banner'];
        $temp_data_billboard["action"] = $data['action'];
        $temp_data_billboard["link"] = $data['link'];
        $billboard__adds_list[] = $temp_data_billboard;

        $temp_data_treasures=[];
        $temp_data_treasures["id"] = $uuid_treasures;
        $temp_data_treasures["name"] = $data['name'];
        $temp_data_treasures["image"] = $data['treasure_image'];
        $temp_data_treasures["action"] = $data['action'];
        $temp_data_treasures["link"] = $data['link'];
        $treasures__adds_list[] = $temp_data_treasures;

        $billboard_vender_details['id'] = $uuid_id;
        $billboard_vender_details['name'] = $data['name'];
        $billboard_vender_details['impressions'] = (int)$billboard_impression['impressions'];
        $billboard_vender_details['billboards'] = $billboard__adds_list;

        $treasures_vender_details['id'] = $uuid_id;
        $treasures_vender_details['name'] = $data['name'];
        $treasures_vender_details['impressions'] = (int)$billboard_impression['impressions'];
        $treasures_vender_details['billboards'] = $treasures__adds_list;

        $all_billboard[] = $billboard_vender_details;
        $all_treasure_vendors[]=$treasures_vender_details;

        }
        }
        if(isset($gift["names"])){
        for($i=0; $i<sizeof($gift["names"]);$i++){
        $temp_array = [];
        $temp_array["id"] = (string) Uuid::generate(4);
        $temp_array["vendor"] = (string) Uuid::generate(4);
        $temp_array["name"] = $gift["names"][$i];
        $temp_array["product"] =$gift["product"][$i];
        $temp_array["message"] = $gift["message"][$i];
        $temp_array["logo"] = $gift["logo"][$i];
        $temp_array["count"] = (int)$gift["count"][$i];
        $temp_array["steps_limit"] = (int)$gift["steps_limit"][$i];
        $temp_array["countEach"] = (int)$gift["countEach"][$i];
        $temp_array["life_points"] = $gift["life_points"][$i];
        $temp_array["point_value"] = (int)$gift["point_value"][$i];
        $temp_array["Gender"] = $gift["Gender"][$i];
        $gifts_list[] = $temp_array;

        }
        }

        $entity = [];
        $entity['name'] = $request->name;
        $entity['steps_points_value']= (int)$request->steps_points_value;
        $entity['welcome'] =  $welcome;
        $entity['share_images'] =  $shareimage;
        //$entity['treasure'] = $treasures;
        if(!empty($treasures)){
        $entity['treasure'] = $treasures;
        }
        if(!empty($leaderboard_data)){
        $entity['cards'] = $leaderboard_data;
        }
        if(!empty($gifts_list)){
        $entity['gifts'] = $gifts_list;
        $entity['gift_locations'] = $giftlocation_path;
        }
        if(!empty($billboard_list)){
        $entity['billboards'] = $billboard_list;
        }
        if(!empty($quiz_list)){
        $entity['dynamic_questionnaire'] = $quiz_list;
        }
        if(!empty($all_billboard)){
          $entity['billboard_vendors_min']=(int)$request->billboard_vendors_min;
          $entity['billboard_vendors'] = $all_billboard;
          $entity['treasure_vendors_min']=(int)$request->billboard_vendors_min;
          $entity['treasure_vendors'] = $all_treasure_vendors;
        }

      if (isset($entity['name'])) {
      $data_file = file_put_contents("challenge_data_folder/{$uuid}_data.json", json_encode($entity));
      $data_route = file_put_contents("challenge_data_folder/{$uuid}_route.json", json_encode($rout_path));
      }
      return redirect('challenge')->with('success', 'Challenges Created: ' . $entity['name'] . " {$uuid}");

      //save data
      // if (isset($entity['name'])) {
      //     $values = array('name' => $entity['name'], 'json' => json_encode($entity), 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'));
      //     DB::table('challenge')->insert($values);
      //     return redirect('challenge')->with('success', 'Challenges ' . $entity['name'] . ' saved. #' . DB::getPdo()->lastInsertId());
      // }
      // else {
      //     return redirect()->back()->withErrors(['template' => 'Please add atleast one template field(s).']);
      // }
      }


      public function process_json($request)
      {

      //echo "<pre>"; print_r($entity);die;
      }

      public function image_library($request, $folder) {
      //get the file
      if (Input::file('file')) {
      $file = Input::file('file');

      //create a file path
      $path = public_path() . '/uploads/';
      if (!empty($folder)) {
      $path = public_path() . '/uploads/' . $folder . '/';
      }
      //get the file name
      $file_name = $request->file_type . '_' . $request->file_name . '_' . time() . '_' . $file->getClientOriginalName();
      $file_name = str_replace(' ', '_', $file_name);

      //save the file to your path
      $file->move($path, $file_name); //( the file path , Name of the file)
      //save that to your database
      //            $new_file = new Uploads(); //your database model
      //            echo $new_file->file_path = $path . $file_name;
      //            $new_file->save();
      //            die;
      }
      $files = [];
      $dir_path = $this->dir_path;
      $dirs = new DirectoryIterator($dir_path);
      if (!empty($folder)) {
      $dirs = new DirectoryIterator($dir_path . '/' . $folder);
      }

      if (isset($request->del)) {
      $path = public_path() . '/uploads/' . $request->del;

      if (File::exists($path)) {
      unlink($path);
      }
      }

      return View::make('image_library')
      ->with('folder', $folder)
      ->with('title', 'Image Library')
      ->with('files', $dirs);
      }

      public function image_library_popup($request, $folder) {
      $files = [];
      $dir_path = $this->dir_path;
      $image_path = "uploads/";
      $back = '';
      if (!empty($folder)) {
      $dir_path = $dir_path . '/' . $folder;
      $image_path = $image_path . $folder . '/';
      $back = '<p onclick="load_gallery_images()">Back</p>';
      }
      $dirs = new DirectoryIterator($dir_path);
      $image_type = $request->image_type;

      $sfiles = array();
      foreach ($dirs as $fileinfo) {
      if (!$fileinfo->isDot()) {

      $sfiles[$fileinfo->getMTime()][] = $fileinfo->getFilename();
      }
      }
      krsort($sfiles);
      //        $html = '<div><input type="search" id="myInput1" onkeyup="my_search(this)" name="myInput1" placeholder="Search"></div>';
      //        $html .='<div id="myDIV" class="row">';
      $html = '';
      foreach ($sfiles as $sfile_name) {
      $file_name = $sfile_name[0];
      $file_explode = explode('.', $file_name);
      $extention = strtolower(end($file_explode));
      if ($extention != 'jpg' && $extention != 'gif' && $extention != 'png' && $extention != 'mp4') {
      $html .= '<div class="col-md-3 col-md-3 col-xs-12 img-tile">';
      $html .= '<div style="min-width:140px;overflow: hidden;text-overflow: ellipsis;">  ';
      $html .= '<img src="' . asset('images/folder.jpg') . '" alt="' . $sfile_name[0] . '" title="' . $sfile_name[0] . '" onclick=load_gallery_images("' . $sfile_name[0] . '")>';
      $html .= '<p class="font-italic small">' . $sfile_name[0] . '</p>';
      $html .= '</div>';
      $html .= '</div> ';
      }
      if ($image_type == 'video') {
      if ($extention != 'mp4') {
      continue;
      }
      } else {
      if ($extention == 'mp4') {
      continue;
      }
      }
      if ($extention == 'jpg' || $extention == 'gif' || $extention == 'png') {
      $html .= '<div class="col-md-3 col-md-3 col-xs-12 img-tile">';
      $html .= '<div style="min-width:140px;overflow: hidden;text-overflow: ellipsis;">  ';
      $html .= '<img src="' . asset($image_path . $sfile_name[0]) . '" alt="' . $sfile_name[0] . '" title="' . $sfile_name[0] . '" onclick=insert_url("' . asset($image_path) . '/' . $sfile_name[0] . '")>';
      $html .= '<p class="font-italic small">' . $sfile_name[0] . '</p>';
      $html .= '</div>';
      $html .= '</div> ';
      } elseif ($extention == 'mp4') {
      $html .= '<div class="col-md-3 col-md-3 col-xs-12 img-tile" onclick=insert_url("' . asset($image_path) . '/' . $sfile_name[0] . '")>';
      $html .= '<div style="min-width:140px;overflow: hidden;text-overflow: ellipsis;">';
      $html .= '<video width="200" controls>';
      $html .= '<source src="' . asset($image_path . $sfile_name[0]) . '" type="video/mp4">';
      $html .= 'Your browser does not support the video tag.';
      $html .= '</video> ';
      $html .= '<p class="font-italic small">' . $sfile_name[0] . '</p>';
      $html .= '</div>';
      $html .= '</div> ';
      }
      }
      //         $html .= '</div> ';
      return $back . '<div class="row">' . $html . '</div>';
      }


      public function create_folder($request) {
      $folder_name = $request->folder_name;
      $dir_path = $this->dir_path . '/' . $folder_name;
      if (!file_exists($dir_path)) {
      mkdir($dir_path, 0777, true);
      }
      return redirect('image_library/' . $folder_name);
      }

      }

      ?>
