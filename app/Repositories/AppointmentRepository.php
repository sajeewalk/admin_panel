<?php

namespace App\Repositories;

use Illuminate\Support\Facades\View;
use App\Interfaces\AppointmentInterface;
use Illuminate\Support\Facades\DB;

class AppointmentRepository implements AppointmentInterface {

    public function view($request) {
        $user_name = $request->q;
        if (auth()->user()->role == 'appointment_admin') {
            $orders = DB::table('pc_appointments')
                    ->join('pc_appointments_cstm', 'pc_appointments.id', '=', 'pc_appointments_cstm.id_c')
                    ->join('users', 'pc_appointments.assigned_user_id', '=', 'users.id')
                    ->where('pc_appointments.deleted', '=', 0)
                    ->where('users.user_name', 'like', '%' . $user_name . '%')
                    ->orderBy('pc_appointments.date_entered', 'DESC')
                    ->select('pc_appointments.*','pc_appointments_cstm.*')
                    ->paginate(20);
        } else {
            $orders = DB::table('pc_appointments')
                    ->join('pc_appointments_cstm', 'pc_appointments.id', '=', 'pc_appointments_cstm.id_c')
                   ->join('users', 'pc_appointments.assigned_user_id', '=', 'users.id')
                    ->where('pc_appointments.deleted', '=', 0)
                    ->where('users.user_name', 'like', '%' . $user_name . '%')
                    ->where('pc_appointments_cstm.app_type_c', '=', 'hemas_app')
                    ->orderBy('pc_appointments.date_entered', 'DESC')
                    ->select('pc_appointments.*','pc_appointments_cstm.*')
                    ->paginate(20);
        }
        return View::make('appointments')
                        ->with('data', $orders)
                        ->with('title', 'Appointments');
    }

}
