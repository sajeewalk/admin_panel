<?php

namespace App\Repositories;

use App\Interfaces\IndexInterface;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;
use DirectoryIterator;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Questionnaire;
use App\Questionas;
use Webpatser\Uuid\Uuid;
use App\Models\Program;
use App\Models\ProgramSequenceMaster;
use Illuminate\Support\Facades\Redirect;
use Auth;
use App\Models\Tags;
use App\Models\ProgramTags;
use Illuminate\Support\Facades\Request;
use App\Models\PolicyUserMaster;
use App\Models\ProgramGroup;
use App\Models\ProgramGroupMapping;
use App\Repositories\ProgramRepository;
use App\Libs\SugarApi;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class IndexRepository implements IndexInterface {

//    public $dir_path = "C:/xampp7/htdocs/native_admin/admin_panel/public/uploads";
   //public $dir_path = "C:/xampp/htdocs/admin_panel/public/uploads";

 public $dir_path = "/var/www/html/admin_panel/public/uploads";

//    private $request;
//
//    public function __construct(ServiceRepository $request) {
//        $this->request = $request;
//    }


    public function dashboard($request) {

        return View::make('dashboard')
                        ->with('title', 'Dashboard');
    }

    public function all_programs($request) {
        $q = '';
        if (isset($request->q)) {
            $q = $request->q;
        }
        $programs = Program::where('name', 'like', "%" . $q . "%")->get();

        return View::make('all_programs')
                        ->with('programs', $programs)
                        ->with('q', $q)
                        ->with('title', 'All Programs');
    }

    public function create_program($request) {

        return View::make('create_program')
                        ->with('title', 'Create Program');
    }

    public function post_create_program($request) {
        $program_sequence = Program::firstOrNew(
                        [
                            'name' => $request->name,
                        ]
        );
        $program_sequence->decription = '';
        $program_sequence->type = $request->type;
        $program_sequence->hide = 1;
        $program_sequence->default_offset = 0;
        $program_sequence->dash_board_image = '';
        $program_sequence->program_image = $request->program_image;
        $program_sequence->tile_image = $request->tile_image;
        $program_sequence->intro_action = $request->intro_action;
        $program_sequence->action_id = $request->action_id;
        $program_sequence->created_by_id = Auth::user()->id;
        $program_sequence->save();
        if ($program_sequence) {
//            return redirect('create_program')->with('success', 'Program created.')
//                            ->with('title', 'Create Program');
            return redirect('program_page/' . $program_sequence->id);
        } else {
            return redirect()->back()->withErrors(['create_program' => 'Error creating program.']);
        }
    }

    public function program_page($id) {
        $program = Program::where('id', '=', $id)->first();
        $tags = Tags::get();
        $sequence = ProgramSequenceMaster::where('program_id', '=', $program->id)->orderBy('sequence')->get();
        return View::make('program_page')
                        ->with('tags', $tags)
                        ->with('program', $program)
                        ->with('sequence', $sequence)
                        ->with('title', 'Program Page');
    }

    public function create_folder($request) {
        $folder_name = $request->folder_name;
        $dir_path = $this->dir_path . '/' . $folder_name;
        if (!file_exists($dir_path)) {
            mkdir($dir_path, 0777, true);
        }
        return redirect('image_library/' . $folder_name);
    }

    public function program_page_post($id, $request) {
        $tags = Tags::get();
        $i = 0;
        foreach ($request->id as $s_id) {
            $program_sequence = ProgramSequenceMaster::firstOrNew(
                            [
                                'id' => $s_id,
                            ]
            );
//                $program_sequence = ProgramSequenceMaster::where('id', '=', $s_id)->first();
            $program_sequence->type = $request->type[$i];
            $program_sequence->program_id = $id;
            $program_sequence->sequence = $request->sequence[$i];
            $program_sequence->timeline_header = $request->timeline_header[$i];
            $program_sequence->timeline_text = $request->timeline_text[$i];
            $program_sequence->timeline_image_link = $request->timeline_image_link[$i];
            $program_sequence->timeline_video_link = $request->timeline_video_link[$i];
            $program_sequence->notification_header = $request->notification_header[$i];
            $program_sequence->notification_text = $request->notification_text[$i];
            $program_sequence->post_time = $request->post_time[$i];
            $program_sequence->relate_id = $request->relate_id[$i];
            $program_sequence->relate_text = $request->relate_text[$i];
            $program_sequence->tag_name_1 = $request->tag_name_1[$i];
            $program_sequence->tag_icon_1 = $request->tag_icon_1[$i];
            $program_sequence->tag_name_2 = $request->tag_name_2[$i];
            $program_sequence->tag_icon_2 = $request->tag_icon_2[$i];
            $program_sequence->tag_name_3 = $request->tag_name_3[$i];
            $program_sequence->points = $request->points[$i];
            $program_sequence->content_icon = $request->content_icon[$i];
            $program_sequence->sub_heading = $request->sub_heading[$i];
            $program_sequence->save();

            $i++;
        }
        $program = Program::where('id', '=', $id)->first();
        $sequence = ProgramSequenceMaster::where('program_id', '=', $program->id)->orderBy('sequence')->get();
        return View::make('program_page')
                        ->with('tags', $tags)
                        ->with('program', $program)
                        ->with('sequence', $sequence)
                        ->with('title', 'Program Page');
    }

    public function nativepost_table($request) {
        $native_post_json = DB::select("SELECT * FROM native_post_json ORDER BY updated_at DESC");
        return View::make('nativepost_table')
                        ->with('title', 'Native Post Table')
                        ->with('data', $native_post_json);
        ;
    }

    public function dynamic_template($request) {
//        print_r($request->all());
//               print_r($entity);
        //create native post json

        return View::make('dynamic_template')
                        ->with('title', 'Dynamic Question');
    }

    public function dynamic_template_submit($request) {
        $entity = $this->process_json($request);

        //save data
        if (isset($entity['name'])) {
            $values = array('name' => $entity['name'], 'json' => json_encode($entity), 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'));
            DB::table('native_post_json')->insert($values);

            return redirect('dynamic_template')->with('success', 'Native Post ' . $entity['name'] . ' saved. #' . DB::getPdo()->lastInsertId());
        } else {
            return redirect()->back()->withErrors(['template' => 'Please add atleast one template field(s).']);
        }
    }

    public function process_json($request) {
        $fields = $request->all();
        $post = [];
        $entity = [];
        //create native post json
        if (isset($fields['field'])) {
            foreach ($fields['field'] as $key => $field) {
//            print_r($field);
                if (isset($field['heading'])) {
                    $post[] = [
                        'type' => 'heading',
                        'text' => $field['heading'],
                    ];
                }
                if (isset($field['sub-heading'])) {
                    $post[] = [
                        'type' => 'sub-heading',
                        'text' => $field['sub-heading'],
                    ];
                }
                if (isset($field['paragraph'])) {
                    $post[] = [
                        'type' => 'paragraph',
                        'text' => $field['paragraph'],
                    ];
                }
                if (isset($field['image-full'])) {
                    $post[] = [
                        'type' => 'image-full',
                        'link' => $field['image-full'],
                        'thumb' => $field['image-full'],
                    ];
                }
                if (isset($field['button']) && !isset($field['terms2'])) {
                    $list = [];
                    foreach ($field['button'] as $but_key => $button) {
                        $stat_val = $field['button-status-text'][$but_key];
                        if ($stat_val == '') {
                            $stat_val = 'enabled';
                        }
                        $list[] = [
                            'label' => $button,
                            'action' => $field['button-action'][$but_key],
                            'meta' => $field['button-meta'][$but_key],
                            'type' => $field['button-type'][$but_key],
                            'status' => $stat_val,
                        ];
                    }
                    $post[] = [
                        'type' => 'button',
                        'list' => $list,
                    ];
                }
                if (isset($field['list'])) {
                    $blist = [];
                    foreach ($field['list'] as $list_key => $list) {
                        $blist[] = $list;
                    }
                    $post[] = [
                        'type' => $field['list-status'],
                        'list' => $blist,
                    ];
                }
                if (isset($field['video-full'])) {
                    $post[] = [
                        'type' => 'video-full',
                        'link' => $field['video-full'],
                        'thumb' => $field['video-full-thumb'],
                    ];
                }
                if (isset($field['price'])) {
                    $post[] = [
                        'type' => 'price',
                        'label' => 'Amount',
                        'amount' => $field['price'],
                        'currency' => $field['price-type'],
                        'symbol' => $field['price-type'],
                    ];
                }
                if (isset($field['authors'])) {
                    $alist = [];
                    $i = 0;
                    foreach ($field['authors'] as $list_key => $list) {
//                        $alist[] = [
//                            'image' => $field['authors-image'][$i],
//                            'main_label' => $list,
//                            'sub_label' => $field['authors-sub'][$i],
//                        ];
                        $alist = [
                            'image' => $field['authors-image'][$i],
                            'main_label' => $list,
                            'sub_label' => $field['authors-sub'][$i],
                        ];
                        $i++;
                    }
                    $post[] = [
                        'type' => 'authors',
                        'list' => $alist,
                    ];
                }
//                if (isset($field['terms'])) {
//                    $post[] = [
//                        'type' => 'terms',
//                        'link' => $field['terms'],
//                        'required' => 'true',
//                    ];
//                }
                if (isset($field['terms2'])) {
                    $list = [];
                    if (isset($field['button'])) {

                        foreach ($field['button'] as $but_key => $button) {
                            $stat_val = $field['button-status-text'][$but_key];
                            if ($stat_val == '') {
                                $stat_val = 'enabled';
                            }
                            $list[] = [
                                'label' => $button,
                                'action' => $field['button-action'][$but_key],
                                'meta' => $field['button-meta'][$but_key],
                                'type' => $field['button-type'][$but_key],
                                'status' => $stat_val,
                            ];
                        }
                    }
                    $post[] = [
                        'type' => 'termsnconditions',
                        'terms_action' => 'web',
                        'terms_meta' => $field['terms2'],
                        'required' => 'true',
                        'button_array' => $list,
                    ];
                }
                if (isset($field['table'])) {
                    $table_header = [];
                    $table_body = [];
                    foreach ($field['table'] as $table_key => $table_row) {
                        $tr = [];
                        foreach ($table_row as $table_data) {
                            if ($table_key == 0) {
                                array_push($table_header, $table_data);
                            } else {
                                array_push($tr, $table_data);
                            }
                        }
                        if (!empty($tr)) {
                            array_push($table_body, $tr);
                        }
                    }
                    $post[] = [
                        'type' => 'table',
                        'head' => $table_header,
                        'body' => $table_body,
                    ];
                }
            }
            $entity = [];
            $entity['type'] = 'native_post';
            $entity['id'] = $request->id;
            $entity['name'] = $request->name;
            ;
            $entity['template'] = $post;
        }
        return $entity;
    }

    public function image_library($request, $folder) {
        //get the file
        if (Input::file('file')) {
            $file = Input::file('file');

//create a file path
            $path = public_path() . '/uploads/';
            if (!empty($folder)) {
                $path = public_path() . '/uploads/' . $folder . '/';
            }
//get the file name
            $file_name = $request->file_type . '_' . $request->file_name . '_' . time() . '_' . $file->getClientOriginalName();
            $file_name = str_replace(' ', '_', $file_name);

//save the file to your path
            $file->move($path, $file_name); //( the file path , Name of the file)
//save that to your database
//            $new_file = new Uploads(); //your database model
//            echo $new_file->file_path = $path . $file_name;
//            $new_file->save();
//            die;
        }
        $files = [];
//        $dir_path = base_path() . '\public\uploades';
        $dir_path = $this->dir_path;
        $dirs = new DirectoryIterator($dir_path);
        if (!empty($folder)) {
            $dirs = new DirectoryIterator($dir_path . '/' . $folder);
        }

        if (isset($request->del)) {
            $path = public_path() . '/uploads/' . $request->del;

            if (File::exists($path)) {
                unlink($path);
            }
        }

        return View::make('image_library')
                        ->with('folder', $folder)
                        ->with('title', 'Image Library')
                        ->with('files', $dirs);
    }

    public function image_library_popup($request, $folder) {
        $files = [];
//        $dir_path = base_path() . '\public\uploades';
        $dir_path = $this->dir_path;
        $image_path = "uploads/";
        $back = '';
        if (!empty($folder)) {
            $dir_path = $dir_path . '/' . $folder;
            $image_path = $image_path . $folder . '/';
            $back = '<p onclick="load_gallery_images()">Back</p>';
        }
        $dirs = new DirectoryIterator($dir_path);
        $image_type = $request->image_type;

        $sfiles = array();
        foreach ($dirs as $fileinfo) {
            if (!$fileinfo->isDot()) {

                $sfiles[$fileinfo->getMTime()][] = $fileinfo->getFilename();
            }
        }
        krsort($sfiles);
//        $html = '<div><input type="search" id="myInput1" onkeyup="my_search(this)" name="myInput1" placeholder="Search"></div>';
//        $html .='<div id="myDIV" class="row">';
        $html = '';
        foreach ($sfiles as $sfile_name) {
            $file_name = $sfile_name[0];
            $file_explode = explode('.', $file_name);
            $extention = strtolower(end($file_explode));
            if ($extention != 'jpg' && $extention != 'gif' && $extention != 'png' && $extention != 'mp4') {
                $html .= '<div class="col-md-3 col-md-3 col-xs-12 img-tile">';
                $html .= '<div style="min-width:140px;overflow: hidden;text-overflow: ellipsis;">  ';
                $html .= '<img src="' . asset('images/folder.jpg') . '" alt="' . $sfile_name[0] . '" title="' . $sfile_name[0] . '" onclick=load_gallery_images("' . $sfile_name[0] . '")>';
                $html .= '<p class="font-italic small">' . $sfile_name[0] . '</p>';
                $html .= '</div>';
                $html .= '</div> ';
            }
            if ($image_type == 'video') {
                if ($extention != 'mp4') {
                    continue;
                }
            } else {
                if ($extention == 'mp4') {
                    continue;
                }
            }
            if ($extention == 'jpg' || $extention == 'gif' || $extention == 'png') {
                $html .= '<div class="col-md-3 col-md-3 col-xs-12 img-tile">';
                $html .= '<div style="min-width:140px;overflow: hidden;text-overflow: ellipsis;">  ';
                $html .= '<img src="' . asset($image_path . $sfile_name[0]) . '" alt="' . $sfile_name[0] . '" title="' . $sfile_name[0] . '" onclick=insert_url("' . asset($image_path) . '/' . $sfile_name[0] . '")>';
                $html .= '<p class="font-italic small">' . $sfile_name[0] . '</p>';
                $html .= '</div>';
                $html .= '</div> ';
            } elseif ($extention == 'mp4') {
                $html .= '<div class="col-md-3 col-md-3 col-xs-12 img-tile" onclick=insert_url("' . asset($image_path) . '/' . $sfile_name[0] . '")>';
                $html .= '<div style="min-width:140px;overflow: hidden;text-overflow: ellipsis;">';
                $html .= '<video width="200" controls>';
                $html .= '<source src="' . asset($image_path . $sfile_name[0]) . '" type="video/mp4">';
                $html .= 'Your browser does not support the video tag.';
                $html .= '</video> ';
                $html .= '<p class="font-italic small">' . $sfile_name[0] . '</p>';
                $html .= '</div>';
                $html .= '</div> ';
            }
        }
//         $html .= '</div> ';
        return $back . '<div class="row">' . $html . '</div>';
    }

    public function preview_template($request) {

        $entity = $this->process_json($request);
        $html = '';
        foreach ($entity['template'] as $template) {
            if ($template['type'] == 'heading') {
                $html .= '<div class="col-md-12 col-md-12 col-xs-12">';
                $html .= '<div>';
                $html .= '<h2>' . $template['text'] . '</h2>';
                $html .= '</div>';
                $html .= '</div>';
            }
            if ($template['type'] == 'sub-heading') {
                $html .= '<div class="col-md-12 col-md-12 col-xs-12">';
                $html .= '<div>';
                $html .= '<h4>' . $template['text'] . '</h4>';
                $html .= '</div>';
                $html .= '</div>';
            }
            if ($template['type'] == 'paragraph') {
                $html .= '<div class="col-md-12 col-md-12 col-xs-12">';
                $html .= '<div>';
                $html .= '<p>' . $template['text'] . '</p>';
                $html .= '</div>';
                $html .= '</div>';
            }
            if ($template['type'] == 'image-full' || $template['type'] == 'image-sub' || $template['type'] == 'gif') {
                $html .= '<div class="col-md-12 col-md-12 col-xs-12">';
                $html .= '<div>';
                $html .= '<p><img src="' . $template['thumb'] . '" width="100%" /></p>';
                $html .= '</div>';
                $html .= '</div>';
            }
            if ($template['type'] == 'bulleted-list') {
                $html .= '<div class="col-md-12 col-md-12 col-xs-12">';
                $html .= '<div>';
                $html .= '<p><ul>';
                foreach ($template['list'] as $l) {
                    $html .= '<li>"' . $l . '" </li>';
                }
                $html .= '</ul></p>';
                $html .= '</div>';
                $html .= '</div>';
            }
            if ($template['type'] == 'numbered-list') {
                $html .= '<div class="col-md-12 col-md-12 col-xs-12">';
                $html .= '<div>';
                $html .= '<p><ol>';
                foreach ($template['list'] as $l) {
                    $html .= '<li>"' . $l . '" </li>';
                }
                $html .= '</ol></p>';
                $html .= '</div>';
                $html .= '</div>';
            }
            if ($template['type'] == 'price') {
                $html .= '<div class="col-md-12 col-md-12 col-xs-12">';
                $html .= '<div>';
                $html .= '<h3>' . $template['label'] . ' ' . $template['currency'] . ' ' . $template['amount'] . '/=</h3>';
                $html .= '</div>';
                $html .= '</div>';
            }
            if ($template['type'] == 'authors') {
//                print_r($template);
//                foreach ($template['list'] as $l) {
                $html .= '<div class="col-md-12 col-md-12 col-xs-12">';
                $html .= '<div>';
                $html .= '<p><img src="' . $template['list']['image'] . '" width="100%"></p>';
                $html .= '<h4>' . $template['list']['main_label'] . '</h4>';
                $html .= '<p>' . $template['list']['sub_label'] . '</p>';
                $html .= '</div>';
                $html .= '</div>';
//                }
            }
            if ($template['type'] == 'terms') {
                if ($template['required'] == 'true') {
                    $html .= '<div class="col-md-12 col-md-12 col-xs-12">';
                    $html .= '<div>';
                    $html .= '<p>Terms & Conditions</p>';
                    $html .= '<span><input type="checkbox" checked readonly></span> ';
                    $html .= '<a href="' . $template['link'] . '" target="_blank">Click Here<a>';
                    $html .= '</div>';
                    $html .= '</div>';
                }
            }
            if ($template['type'] == 'video-full' || $template['type'] == 'video-sub') {
                $html .= '<div class="col-md-12 col-md-12 col-xs-12">';
                $html .= '<video width="100%" controls poster="' . $template['thumb'] . '">';
                $html .= '<source src="' . $template['link'] . '" type="video/mp4">';
                $html .= 'Your browser does not support the video tag.';
                $html .= '</video> ';
                $html .= '</div>';
            }
            if ($template['type'] == 'table') {
                $html .= '<div class="col-md-12 col-md-12 col-xs-12">';
                $html .= '<div>';
                $html .= '<table class="table-striped" width="100%">';
                $html .= '<tr>';
                foreach ($template['head'] as $head) {
                    $html .= '<td>' . $head . '</td>';
                }
                $html .= '</tr>';

                foreach ($template['body'] as $body) {
                    $html .= '<tr>';
                    foreach ($body as $b) {
                        $html .= '<td>' . $b . '</td>';
                    }
                    $html .= '</tr>';
                }
                $html .= '</table>';
                $html .= '</div>';
                $html .= '</div>';
            }
            if ($template['type'] == 'button' && !isset($template['termsnconditions'])) {
                $html .= '<div class="col-md-12 col-md-12 col-xs-12">';
                $html .= '<div>';

                foreach ($template['list'] as $l) {
                    $button_status = 'enabled';
                    if ($l['status'] == 'disabled') {
                        $button_status = 'disabled';
                    }
                    $html .= '<button class="btn btn-secondary" type="button" ' . $button_status . '>' . $l['label'] . '</button>';
                }

                $html .= '</div>';
                $html .= '</div>';
            }

            if ($template['type'] == 'termsnconditions') {
                $html .= '<div class="col-md-12 col-md-12 col-xs-12">';
                $html .= '<div>';
                $html .= '<p>Terms & Conditions</p>';
                $html .= '<span><input type="checkbox" checked readonly></span> ';
                $html .= '<a href="' . $template['terms_meta'] . '" target="_blank">Click Here<a>';
                foreach ($template['button_array'] as $l) {
                    $button_status = 'enabled';
                    if ($l['status'] == 'disabled') {
                        $button_status = 'disabled';
                    }
                    $html .= '<button class="btn btn-secondary" type="button" ' . $button_status . '>' . $l['label'] . '</button>';
                }

                $html .= '</div>';
                $html .= '</div>';
            }
        }
//        print_r($entity);
        echo $html;
        die;
    }

    function dynamic_template_edit($id) {
        $post = [];
        $name = '';
        $native_post = DB::select("SELECT * FROM native_post_json WHERE id=$id");
        foreach ($native_post as $np) {
            $name = $np->name;
            $post = json_decode($np->json, 1);
        }
        return View::make('dynamic_template_edit')
                        ->with('native_id', $id)
                        ->with('post', $post)
                        ->with('name', $name)
                        ->with('title', 'Dynamic Templae Edit');
        die;
    }

    public function dynamic_template_edit_save($request) {
        $entity = $this->process_json($request);
        //save data
        if (isset($entity['name'])) {
            $values = array('name' => $entity['name'], 'json' => json_encode($entity), 'updated_at' => date('Y-m-d H:i:s'));
            DB::table('native_post_json')
                    ->where('id', $request->native_id)
                    ->update($values);

            return redirect('dynamic_template/edit/' . $request->native_id)->with('success', 'Native Post ' . $entity['name'] . ' saved. #' . $request->native_id);
        } else {
            return redirect()->back()->withErrors(['template' => 'Please add atleast one template field(s).']);
        }
    }

    public function create_tile($request) {
        $user = User::where('user_name', '=', $request->mobile)->first();
        if (isset($user->id)) {
            $relate_id = $request->id;
            $user_id = $user->id;

            $tile_name = "Native post #" . $relate_id;
            $tile_image = 'https://livehappy.ayubo.life/custom/include/images/janashakthi_/zoom_level/janashakthi.png';
            $v5_bg_image = 'https://livehappy.ayubo.life/custom/include/images/janashakthi_/zoom_level/janashakthi2.png';
            $icon = 'https://livehappy.ayubo.life/custom/include/images/janashakthi_/zoom_level/today_janashakthi.png';
            $today = date('Y-m-d');
            $end_date = date('Y-m-d', strtotime($today . ' + 7 days'));
            DB::statement("INSERT INTO `today_tiles` (`related_id`, `related_type`, `assigned_user_id`, `title`, `sub_title`, `action_text`, `link`, `Image`, `visible_on`, `expire_on`, `status`, `section`, `image_v5`, `icon`, `tile_status`) VALUES ('$relate_id', 'native_post', '$user_id', '$tile_name', '', 'View', '$relate_id', '$tile_image', '$today', '$end_date', 'Active', '', '$v5_bg_image', '$icon', 'active')");
            echo 1;
        } else {
            echo 2;
        }

        die;
    }

    public function list_native_post() {
        $lists = DB::select("SELECT * FROM native_post_json ORDER BY name ASC");
        foreach ($lists as $list) {
            echo "<option value=" . $list->id . ">" . $list->name . "</option>";
        }
        die;
    }

    public function list_select_question() {
        $lists = DB::select("SELECT * FROM questionnaire ORDER BY type ASC");
        foreach ($lists as $list) {
            echo "<option value=" . $list->id . ">" . $list->type . "</option>";
        }
        die;
    }

    public function list_chat() {
        $lists = DB::select("SELECT * FROM contacts WHERE deleted=0 ORDER BY first_name ASC");
        foreach ($lists as $list) {
            echo "<option value=" . $list->id . ">" . $list->salutation . ' ' . $list->first_name . ' ' . $list->last_name . "</option>";
        }
        die;
    }

    public function add_empty_row($program_id) {
        $program_sequence = new ProgramSequenceMaster;
        $program_sequence->type = '';
        $program_sequence->program_id = $program_id;
        $program_sequence->sequence = 0;
        $program_sequence->timeline_header = '';
        $program_sequence->timeline_text = '';
        $program_sequence->timeline_image_link = '';
        $program_sequence->timeline_video_link = '';
        $program_sequence->notification_header = '';
        $program_sequence->notification_text = '';
        $program_sequence->post_time = '';
        $program_sequence->relate_id = '';
        $program_sequence->relate_text = '';
        $program_sequence->tag_name_1 = '';
        $program_sequence->tag_icon_1 = '';
        $program_sequence->tag_name_2 = '';
        $program_sequence->tag_icon_2 = '';
        $program_sequence->tag_name_3 = '';
        $program_sequence->points = '';
        $program_sequence->content_icon = '';
        $program_sequence->sub_heading = '';
        $program_sequence->save();
        return Redirect::to('program_page/' . $program_id);
    }

    public function del_row($program_id, $seq_id) {
        $program_sequence = ProgramSequenceMaster::where('id', '=', $seq_id)->first();
        $program_sequence->delete();
        return Redirect::to('program_page/' . $program_id);
    }

    public function tags_save($program_id, $request) {
        $program = Program::where('id', '=', $program_id)->first();
        foreach ($request->tags as $t) {
            $tags[] = ['tag_id' => $t, 'program_id' => $program_id];
        }
        $program->tags()->sync($tags);
        return Redirect::to('program_page/' . $program_id);
    }

    public function create_tile_program($request) {
        $user = User::where('user_name', '=', $request->mobile)->where('deleted', '=', 0)->first();
        if (isset($user->id)) {
            $relate_id = $request->id;
            $user_id = $user->id;

            //subscribe to program

            $my_program = Program::where('id', '=', $relate_id)->first();
            if (isset($my_program->name)) {
                $tile_name = $my_program->name;
            } else {
                echo 'program error';
                die;
            }
            $request1 = new \Illuminate\Http\Request();
            $request1->replace(['mobile' => $request->mobile, 'pro_ids' => array($relate_id), 'tile_name' => $tile_name]);
//            print_r($request1->all());
//            die;
            $res = self::postProgramsManual($request1);
//            return $res;
//            echo 'dddd';
//            die;
            //subscribe to program
//            $tile_image = 'https://livehappy.ayubo.life/custom/include/images/janashakthi_/zoom_level/janashakthi.png';
//            $v5_bg_image = 'https://livehappy.ayubo.life/custom/include/images/janashakthi_/zoom_level/janashakthi2.png';
//            $icon = 'https://livehappy.ayubo.life/custom/include/images/janashakthi_/zoom_level/today_janashakthi.png';
//            $today = date('Y-m-d');
//            $end_date = date('Y-m-d', strtotime($today . ' + 7 days'));
//            DB::statement("INSERT INTO `today_tiles` (`related_id`, `related_type`, `assigned_user_id`, `title`, `sub_title`, `action_text`, `link`, `Image`, `visible_on`, `expire_on`, `status`, `section`, `image_v5`, `icon`, `tile_status`) VALUES ('$relate_id', 'native_post', '$user_id', '$tile_name', '', 'View', '$relate_id', '$tile_image', '$today', '$end_date', 'Active', '', '$v5_bg_image', '$icon', 'active')");
            echo 1;
        } else {
            echo 2;
        }
//        return false;
//        die;
    }

    public function postProgramsManual($request) {
        $mobile = $request->mobile;
//        $user_id = '7922c247-7e63-e3c7-afbf-59b764342cbd';
        $user = User::where('user_name', 'like', '%' . $mobile . '%')->where('deleted', '=', 0)->first();
        $user_id = $user->id;

        $tile_name = "Live Life";
        if ($request->has("tile_name")) {
            $tile_name = $request->tile_name;
        }

        $max_id = PolicyUserMaster::max('id') + 1;
        $policy_user_master_id = $max_id;
        $relate_id = $policy_user_master_id;
        $policy_id = 'PROGRAM-GROUP';
        $pro_ids = $request->pro_ids;
//        $tile_name = "Live Life";
        $program_name = $tile_name;
        $testorder_id = 'al_' . time();

        $tile_icon1 = "https://livehappy.ayubo.life/custom/include/images/janashakthi_/zoom_level/janashakthi.png";
        $tile_icon2 = "https://livehappy.ayubo.life/custom/include/images/janashakthi_/zoom_level/janashakthi2.png";
        $tile_icon3 = "https://livehappy.ayubo.life/custom/include/images/janashakthi_/zoom_level/today_janashakthi.png";
        if (isset($pro_ids[0])) {
            if ($pro_ids[0] == '49') {
                $tile_icon1 = "https://livehappy.ayubo.life/custom/include/images/corona2/zoom_level/today_tile.png";
                $tile_icon2 = "https://livehappy.ayubo.life/custom/include/images/corona2/zoom_level/today_tile_old.png";
                $tile_icon3 = "https://livehappy.ayubo.life/custom/include/images/corona2/zoom_level/icon.png";
            }
        }
//remove tile if exists for the same program
//        $values = array('status' => 'Inactive');
//        DB::table('today_tiles')
//                ->where('related_type', 'janashakthi_policy')
//                ->where('assigned_user_id', $user_id)
//                ->where('title', $tile_name)
//                ->where('status', 'Active')
//                ->update($values);
        $lists = DB::select("SELECT * FROM today_tiles WHERE related_type='janashakthi_policy' AND assigned_user_id='$user_id' AND title='$tile_name' AND status='Active'");
        foreach ($lists as $list) {
            $request1 = new \Illuminate\Http\Request();
            $request1->replace(['policy_user_master_id' => $list->related_id, 'user_id' => $user_id]);
            ProgramRepository::program_exe_unsubscribe($request1);
        }

        DB::insert("INSERT INTO `ins_policy_user_master` (`id`, `policy_master_id`, `user_id`, `policy_id`, `policy_status`, `policy_create_date`, `policy_active_date`, `next_payment_date`, `policy_doc`, `user_declaration_json`, `created_at`, `updated_at`) VALUES "
                . "($policy_user_master_id, '6', '$user_id', '" . $policy_id . "', 'active', NULL, NULL, NULL, NULL, NULL, NULL, NULL);");

        DB::insert("INSERT INTO `today_tiles` (`related_id`, `related_type`, `assigned_user_id`, `title`, `sub_title`, `action_text`, `link`, `Image`, `visible_on`, `expire_on`, `status`, `section`, `image_v5`, `icon`, `tile_status`) "
                . "VALUES ('$relate_id', 'janashakthi_policy', '$user_id', '$tile_name', '', 'View', '', '$tile_icon1', '" . date('Y-m-d') . "', NULL, 'Active', '', '$tile_icon2', '$tile_icon3', 'active');");
        $ex1 = 'de542f25-617a-a3f8-3c84-589230c3b4b8';
        $ex2 = 'c3d3515d-a7e2-f51b-5c0e-5a8af7023a52';
        $ex3 = 'b2e43ff8-cf3a-a96f-0f34-5b234ffec8ab';

        if (in_array(49, $pro_ids)) {
            $ex2 = '5224088a-20c7-8ec1-1263-5a61cf03d166';
            $ex3 = 'dec850b5-bff6-48eb-7a16-5aa0b8d7855d';
        }
        DB::insert("INSERT INTO `policy_user_info` ( `policy_user_master_id`, `user_id`, `status`, `status_reason`, `reminder_date`, `reminder_count`, `questionier`, `hba1c`, `lipid`, `view_history`, `testorder_id`, `policy_user_report`, `policy_user_report_created_by`, `questionier_score`, `assigned_doctor_id`, `assigned_fitness_expert`, `assigned_nutrition_expert`, `proceed_with_questionier`, `created_at`, `updated_at`) VALUES "
                . "('$policy_user_master_id', '$user_id', 'pending_analysis', '', '', '', '', '', '', '', '$testorder_id', '', '', '','$ex1', '$ex2', '$ex3','',  '" . date('Y-m-d H:i:s') . "', '" . date('Y-m-d H:i:s') . "');");


        $program_group = ProgramGroup::firstOrNew(
                        [
                            'user_id' => $user_id,
                            'assigned_type' => 'program',
                            'assigned_id' => $user_id,
                            'policy_user_master_id' => $policy_user_master_id,
                        ]
        );
        $program_group->name = $program_name;
        $program_group->status = 'Active';
        $program_group->start_date = date('Y-m-d');
        $program_group->save();


        //insert group mapping
        foreach ($pro_ids as $program_id) {
            $offset = 0;
            $program_group_mapping = ProgramGroupMapping::firstOrNew(
                            [
                                'program_group_id' => $program_group->id,
                                'program_id' => $program_id,
                            ]
            );
            $program_group_mapping->offset = $offset;
            $program_group_mapping->save();
        }


        //send day 1 programs
        foreach ($pro_ids as $program_id) {
            $my_program = Program::where('id', '=', $program_id)->first();
            $program_sequences = ProgramSequenceMaster::where('program_id', '=', $my_program->id)->get();
            foreach ($program_sequences as $sequence) {
                $pgm = ProgramGroupMapping::where('program_group_id', '=', $program_group->id)->where('program_id', '=', $sequence->program_id)->first();
                $offset = $pgm->offset;
                $psm = ProgramSequenceMaster::where('program_id', '=', $program_id)->orderBy('sequence', 'desc')->first();
                $run_for_days = ($psm->sequence) + 1;
                //run 11 days only
                $run_for_days = 11;
                if ($sequence->sequence < $run_for_days) {
                    $today = date('Y-m-d');
                    $mod_date = strtotime($today . "+ " . ($sequence->sequence + $offset - 1) . " days");
                    $today = date("Y-m-d", $mod_date);
                    if ($sequence->sequence == 1)
                        ProgramRepository::timeline_post($user_id, $my_program->id, $sequence, 'yes', 'yes', $today, $policy_user_master_id);
                    else
                        ProgramRepository::timeline_post($user_id, $my_program->id, $sequence, 'no', 'yes', $today, $policy_user_master_id);
                }
            }
        }

        //send day 1 programs
        return $user_id;
        echo "done";
        die;
    }

    public function dynamic_template_welcome($request) {

        return View::make('dynamic_template_welcome')
                        ->with('title', 'Dynmic Dashboard');
    }

    public function get_farlin_form($request) {
        $company = $request->company;
        $today = date('Y-m-d');
        $promotion = DB::select("SELECT * FROM promotions WHERE promotion='$company' AND (expiry_date IS NULL OR expiry_date>='$today')  LIMIT 0,1");
        if (isset($promotion[0])) {

        } else {
            echo 'Invalid promotion or promotion expired.';
            die;
        }
        return View::make('farlin_form')
                        ->with('promotion', $promotion[0])
                        ->with('title', 'Farlin Data Capture Form');
    }

    public function post_farlin_form($request) {

        $company = $request->company;
        $today = date('Y-m-d');
        $promotion = DB::select("SELECT * FROM promotions WHERE promotion='$company' AND (expiry_date IS NULL OR expiry_date>='$today')  LIMIT 0,1");
        if (!isset($promotion[0])) {
            echo 'Invalid promotion or promotion expired.';
            die;
        }
        $data = $request->all();
        unset($data['_token']);
        unset($data['company']);
        foreach ($data as $k => $field) {
            $now = date('Y-m-d H:i:s');
            $values = array('promo_id' => $promotion[0]->id, 'mobile_number' => $request->mother_mobile, 'source' => $k, 'source_ref' => $field, 'created_at' => $now, 'updated_at' => $now);
            DB::table('promotion_entries')->insert($values);
        }

        //create user if not find
        $mobile = $request->mother_mobile;
        $user = User::where('user_name', '=', $mobile)->where('deleted', '=', 0)->exists();
        if (!$user) {
            //create the user
//            $sugar = new SugarApi('https://livehappy.ayubo.life/custom/service/v6/rest.php');
//            $rest_data = ['mobile_number' => $mobile, 'first_name' => 'Unknown', 'last_name' => 'Unknown', 'date_of_birth' => date('Y-m-d'), 'gender' => '2', 'email' => '', 'country_code' => '+94', 'nin' => ''];
//            $result = $sugar->send($rest_data, 'createAMobileUser');
//            sleep(2);
            //create the user
            //send sms to customer the app download link
            $promotion = DB::select("SELECT * FROM promotions WHERE id='{$promotion[0]->id}' LIMIT 0,1");
            foreach ($promotion as $promo) {
                if ($promo->app_id == 'ayubo.life') {
                    $mobile_number = $request->mother_mobile;
                    $message = 'Welcome to Farlin Mum, Please download https://bit.ly/2W2pTnr app and get access to your program.';
                    self::send_sms($mobile_number, $message);
                }
            }
        }

        //service subscription
        $msg = 'Data saved.';
        $service_types = DB::select("SELECT * FROM promotion_activations WHERE promo_id='{$promotion[0]->id}' AND related_type='program_subscription'");
        foreach ($service_types as $service_type) {
            $request1 = new \Illuminate\Http\Request();
            $request1->replace(['mobile' => $request->mother_mobile, 'id' => $service_type->related_id]);
            $this->create_tile_program($request1);
        }

        return redirect('promotion/form?company=' . $company)->with('success', $msg)
                        ->with('promotion', $promotion[0])
                        ->with('title', 'Farlin Data Capture Form');
    }

    static function send_sms($mobile_number, $message) {
        $sms_api_url = "https://cpsolutions.dialog.lk/index.php/cbs/sms/send";

        $client = new Client();
        $result = $client->post($sms_api_url, [
            'form_params' => [
                'destination' => $mobile_number,
                'q' => "15052020191860",
                'message' => $message,
            ]
        ]);
    }

}
