<?php

namespace App\Repositories;

use App\Interfaces\ProgramInterface;
use DB;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;
use Log;
use App\Models\Program;
use App\Models\ProgramTags;
use App\Models\ProgramGroup;
use App\Models\UserCSTM;
use App\Models\ProgramGroupMapping;
use App\Models\ProgramSequenceMaster;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Str;
use Webpatser\Uuid\Uuid;
use App\Repositories\NativeChatRepository;
use App\Models\Contact;
use App\User;
use App\Models\Post;
use App\Models\ProgramDashboardMaster;
use App\Models\PolicyUserInfo;
use App\Models\PolicyUserMaster;
use Illuminate\Support\Facades\Request;
use App\Repositories\DialogSubscriptionRepository;
use App\Libs\SugarApi;
use Illuminate\Support\Facades\View;

class ProgramRepository implements ProgramInterface {

//    public function __construct(NativeChatRepository $nativeChatRepository) {
//        $this->nativeChatRepository = $nativeChatRepository;
//    }

    public function programImport($request) {


        $string = config('janashakthi.program8');

        $data = json_decode($string, true);
        foreach ($data as $i => $v) {
            $sequesnce['program_id'] = '8';
            $sequesnce['type'] = strtolower($v['type']);
            $sequesnce['sequence'] = (int) filter_var($v['sequence'], FILTER_SANITIZE_NUMBER_INT);
            $sequesnce['timeline_header'] = $v['timeline_header'];
            $sequesnce['timeline_text'] = $v['timeline_text'];
            $sequesnce['timeline_image_link'] = $v['timeline_image_link'];
            $sequesnce['timeline_video_link'] = $v['timeline_video_link'];
            $sequesnce['inner_content_type'] = $v['inner_content_type'];
            $sequesnce['inner_content'] = $v['inner_content'];
            $sequesnce['tile'] = $v['tile'];
            $sequesnce['tile_image_link'] = $v['tile_image_link'];
            $sequesnce['tile_text'] = $v['tile_text'];
            $sequesnce['notification_header'] = $v['notification_header'];
            $sequesnce['notification_text'] = $v['notification_text'];
            $sequesnce['post_time'] = $v['post_time'];
            $sequesnce['relate_id'] = $v['relate_id'];
            $sequesnce['relate_text'] = $v['relate_text'];
            $sequesnce['sub_heading'] = $v['sub_heading'];
//            $sequesnce['category'] = $v['Category'];
//            $sequesnce['male'] = (strtolower($v['Male']) == 'yes') ? 1 : 0;
//            $sequesnce['female'] = (strtolower($v['Female']) == 'yes') ? 1 : 0;
//            $sequesnce['general'] = (strtolower($v['General']) == 'yes') ? 1 : 0;
//            $sequesnce['under_weight'] = (strtolower($v['Under weight']) == 'yes') ? 1 : 0;
//            $sequesnce['normal_weight'] = (strtolower($v['Normal weight']) == 'yes') ? 1 : 0;
//            $sequesnce['over_weight'] = (strtolower($v['Over weight']) == 'yes') ? 1 : 0;
//            $sequesnce['obese'] = (strtolower($v['Obese']) == 'yes') ? 1 : 0;
//            $sequesnce['low_bp'] = (strtolower($v['Low BP']) == 'yes') ? 1 : 0;
//            $sequesnce['ideal_blood_pressure'] = (strtolower($v['Ideal blood pressure']) == 'yes') ? 1 : 0;
//            $sequesnce['high_blood_pressure'] = (strtolower($v['High blood pressure']) == 'yes') ? 1 : 0;
//            $sequesnce['low_hdl'] = (strtolower($v['Low HDL']) == 'yes') ? 1 : 0;
//            $sequesnce['high_ldl'] = (strtolower($v['High LDL']) == 'yes') ? 1 : 0;
//            $sequesnce['high_tga'] = (strtolower($v['High TGA']) == 'yes') ? 1 : 0;
//            $sequesnce['low'] = (strtolower($v['low']) == 'yes') ? 1 : 0;
//            $sequesnce['normal'] = (strtolower($v['Normal']) == 'yes') ? 1 : 0;
//            $sequesnce['pre_diabatic'] = (strtolower($v['Pre diabatic']) == 'yes') ? 1 : 0;
//            $sequesnce['diabatic'] = (strtolower($v['diabatic']) == 'yes') ? 1 : 0;
//            $sequesnce['sedentary'] = (strtolower($v['Sedentary']) == 'yes') ? 1 : 0;
//            $sequesnce['moderately_active'] = (strtolower($v['Active']) == 'yes') ? 1 : 0;
//            $sequesnce['active'] = (strtolower($v['Active']) == 'yes') ? 1 : 0;
            $sequesnce['points'] = $v['points'];
            $sequesnce['content_icon'] = $v['content_icon'];
            $sequesnce['tag_name_1'] = $v['tag_name_1'];
            $sequesnce['tag_icon_1'] = $v['tag_icon_1'];
            $sequesnce['tag_name_2'] = $v['tag_name_2'];
            $sequesnce['tag_icon_2'] = $v['tag_icon_2'];
            $sequesnce['tag_name_3'] = $v['tag_name_3'];
            $sequesnce['tag_icon_3'] = $v['tag_icon_3'];
//            $sequesnce['do_not_take_part_in_challenge'] = (strtolower($v['Do not take part in challenge']) == 'yes') ? 1 : 0;
            print_r($sequesnce);

            if (!empty($sequesnce['type'])) {
                $record = ProgramSequenceMaster::create($sequesnce);
            }
//            return $record;
        }
    }

    public function createProgram($request) {
//        $user_id='b869c7b8-e244-93a5-0010-5da9599806c7';
//        $policy_user_master_id='2106';
//        JanashakthiRepository::createJanshakthiWelcomeTile($user_id, $policy_user_master_id);
        echo 'done'; 
        die;
        $program_group = ProgramGroup::where('status', '=', 'Active')->where('telco_app_id', '=', '6e4ffa97-b741-45e3-a90c-612e586a450e')->get();
        foreach ($program_group as $pg) {
            $date = $pg->start_date;
            $user_id = $pg->user_id;
//            echo "$date -- $user_id --".$pg->policy_user_master_id;
//            echo '<br>';
//            self::program_manual_post($date, $user_id, $pg->policy_user_master_id); 
        }
        die;
//        $mobile = '0715839815';
//        $sugar = new SugarApi('https://livehappy.ayubo.life/custom/service/v6/rest.php');
//        $rest_data = ['mobile_number' => $mobile, 'first_name' => 'Unknown', 'last_name' => 'Unknown', 'date_of_birth' => date('Y-m-d'), 'gender' => '0', 'email' => '', 'country_code' => '+94', 'nin' => ''];
//        $result = $sugar->send($rest_data, 'createAMobileUser');
//        print_r($result);
//        die; 
//        $request = new \Illuminate\Http\Request();
//        $request->replace(['user_id' => '5d8fe13f-fe6b-d4dc-5777-5bb44326b397', 'program_id' => '33','subscription_type'=>'sms','app_id'=>'6e4ffa97-b741-45e3-a90c-612e586a450e']);
//        self::subscribe($request);
//        die;        
//        DialogSubscriptionRepository::dialog_subscription_charge_daily_all();
        self::program_daily_cron_testing();
        die;
//        $app_id = '6e4ffa97-b741-45e3-a90c-612e586a450e';
//        $mobile = '0774150430';
//        $message = 'hi this is program sms';
//        $dialog = new DialogSubscriptionRepository();
//        $dialog->sendSMS($app_id, $mobile, $message);
//        die;
//        self::delete_tips_from_sldf();
//        die;
        /*
          $dashboard= ProgramDashboardMaster::get();
          foreach ($dashboard as $d) {
          $program_groups=ProgramGroup::where('user_id','=',$d->user_id)->orderBy('id', 'desc')->get();
          $pro_group_id='';
          foreach($program_groups as $program_group){
          $pro_group_id='';
          $program_group_mapping= ProgramGroupMapping::where('program_group_id','=',$program_group->id)->where('program_id','=',7)->orderBy('id', 'desc')->first();
          if(isset($program_group_mapping->id)){
          $pro_group_id=$program_group->id;
          break;
          }
          }
          if(!empty($pro_group_id)){
          echo $d->program_group_id=$pro_group_id;
          echo '---'.$d->id;
          echo '<br>';
          //            $d->save();
          }
          }
          echo 'done';
          die;
         * 
         */
//        DB::table('program_dashboard_master')
//                ->where('user_id', '5d8fe13f-fe6b-d4dc-5777-5bb44326b397')
//                ->where('action', 'dynamicquestion')
//                ->where('meta', 7)
//                ->limit(1)
//                ->update(array('disabled' => '1'));
//        $user_id='5d8fe13f-fe6b-d4dc-5777-5bb44326b397';
//                $pdm = ProgramDashboardMaster::where('user_id','=', $user_id)->where('action','=', 'dynamicquestion')->where('meta','=', '2')->where('disabled','=', 0)->first();
//                $data = json_decode($pdm->tags, 1);
//                foreach ($data as $d) {
//                    $total_points = (int) filter_var($d['text'], FILTER_SANITIZE_NUMBER_INT);
//                    $input_array = array();
//                    $input_array["assigned_user_id"] = $user_id;
//                    $input_array["mobile_number"] = '';
//                    $input_array["vendor_id"] = 1;
//                    $input_array["points"] = $total_points;
//                    $input_array["source"] = "livelife";
//                    $input_array["created_at"] = strtotime("now");
//                    $input_array["expire_date"] = DATE("Y", strtotime("+1 year")) . "-03-31";
//                    $input_array["point_date"] = DATE("Y-m-d", strtotime("now"));
//                    DB::table('loyality_points')->insert($input_array);
//                }        
//        $community_list = DB::select(DB::raw("SELECT GROUP_CONCAT(a.accounts_users_2accounts_ida) AS accounts, a.accounts_users_2users_idb FROM accounts_users_2_c AS a WHERE a.company_enrolled_c = 1  AND a.accounts_users_2users_idb = '1000c329-2c44-50b0-9071-5a96790f14ed'  AND a.deleted = 0  GROUP BY a.accounts_users_2users_idb"));
//        print_r($community_list[0]->accounts);
//        die;
//        self::program_daily_cron(); 
//        die;
        //manual run program
//        $date = '2019-07-31';
//        $user_id = 'bbb730f8-ebcc-972f-cd9f-5d4196782d96';
//        self::program_manual_post($date, $user_id, 512); 
//        die;
        //manuall program run
        //one program manual run-----------------------------------------------------------------
//        $program_id = 33;
//        $offset = 0;
//        $mam_subscription_program = DB::select("SELECT i.id FROM program_group pg INNER JOIN policy_user_info i ON pg.policy_user_master_id=i.policy_user_master_id where pg.status='Active' and telco_app_id='6e4ffa97-b741-45e3-a90c-612e586a450e' AND i.user_id!='5d8fe13f-fe6b-d4dc-5777-5bb44326b397' AND i.user_id!='7fa9082c-b5e8-d522-de15-5a314edac766'");
//        foreach ($mam_subscription_program as $msp) {
//            echo $info_id = $msp->id;
//            echo '<br>';
//
//            $policy_user_info = PolicyUserInfo::where('id', '=', $info_id)->orderBy('id', 'ASC')->get();
////        $policy_user_info = PolicyUserInfo::where('status', '=', 'active')->orderBy('id','ASC')->get(); 
//            foreach ($policy_user_info as $pui) {
//                $policy_user_master_id = $pui->policy_user_master_id;
//                $user_id = $pui->user_id;
//                $program_group_exists = ProgramGroup::where('user_id', '=', $user_id)->where('policy_user_master_id', '=', $policy_user_master_id)->exists();
//                if ($program_group_exists) {
//                    $program_group = ProgramGroup::where('user_id', '=', $user_id)->where('policy_user_master_id', '=', $policy_user_master_id)->first();
//                    $program_group_id = $program_group->id;
//                    echo $program_group_date = $program_group->created_at;
//                    $program_group_mapping = ProgramGroupMapping::firstOrNew(
//                                    [
//                                        'program_group_id' => $program_group_id,
//                                        'program_id' => $program_id,
//                                    ]
//                    );
//                    $program_group_mapping->offset = $offset;
//                    $program_group_mapping->created_at = $program_group_date;
//                    $program_group_mapping->save();
//                    $date = $program_group_date;
//                    $user_id = $user_id;
//                    $policy_user_master_id = $policy_user_master_id;
//                    $program_id = $program_id;
//                    self::program_manual_post_one_program($date, $user_id, $policy_user_master_id, $program_id);
//                }
//
//                Storage::append('tips_program_post.txt', date('Y-m-d H:i:s') . ' policy_user_master_id ' . $pui->id);
//            }
//        }
//        die;
        //one program manual run-----------------------------------------------------------------
        //sldf subscribe
//        $request = new \Illuminate\Http\Request();
//        $request->replace(['user_id' => 'bbb730f8-ebcc-972f-cd9f-5d4196782d96', 'program_id' => '7']);
//        self::subscribe($request);
        die;
        //sldf subscribe

        $key = '';
        $search = PolicyUserInfo::whereHas('user', function ($query) use ($key) {
                    $query->orWhere('first_name', 'like', '%' . $key . '%')
                    ->orWhere('last_name', 'like', '%' . $key . '%')
                    ->orWhere('user_name', 'like', '%' . $key . '%')
                    ;
                })
                ->orWhere('status', 'like', '%' . $key . '%')
                ->get();
        echo '<table border=1>';
        foreach ($search as $s) {
            $user_name = isset($s->user->user_name) ? $s->user->user_name : '';

            if (!empty($user_name)) {
                if (preg_match('/^\d{10}$/', $user_name)) {
//             echo $user_name;
                } else {
                    echo '<tr><td>' . $s->user->id . '</td><td>' . $user_name . '</td></tr>';
                }
            }
        }
        echo '</table>';
        die;
        $program_object['name'] = 'You Are What You Eat';
        $program_object['decription'] = 'Nutritional Assessment';
        $program_object['type'] = 'Mind';
        $tags_array = [];
        foreach ($tags_array as $val) {
            $tags[] = $val;
        }
        $program = Program::create($program_object);
        foreach ($tags as $tag) {
            $tag_object['tag_id'] = $tag;
            $tag_object['program_id'] = $program->id;
            ProgramTags::create($tag_object);
        }
    }

    /*
      public static function program_daily_cron_old() {
      //        $today = date('Y-m-d');
      $today = isset($_REQUEST['date']) ? $_REQUEST['date'] : date('Y-m-d');
      //        $today = '2019-07-23';
      $program_groups = ProgramGroup::where('status', '=', 'Active')->get();
      foreach ($program_groups as $program_group) {
      $user_id = $program_group->user_id;
      $program_group_mappings = ProgramGroupMapping::where('program_group_id', '=', $program_group->id)->get();
      foreach ($program_group_mappings as $program_group_mapping) {
      $programs = Program::where('id', '=', $program_group_mapping->program_id)->get();
      foreach ($programs as $program) {
      $subscription_date = $program_group->start_date;
      $subscription_date = date('Y-m-d', strtotime($subscription_date . ' + ' . $program_group_mapping->offset . ' days'));
      //                    echo "program : " . $program->id . " user : " . $user_id . "offset : " . $program_group_mapping->offset . " subscription date : " . $subscription_date;
      //                    echo '<br>';
      $pogram_id = $program->id;
      $program_sequences = ProgramSequenceMaster::where('program_id', '=', $program->id)->get();
      foreach ($program_sequences as $program_sequence) {
      if (strtotime($today) >= strtotime($subscription_date)) {
      $diff = abs(strtotime($today) - strtotime($subscription_date));
      $years = floor($diff / (365 * 60 * 60 * 24));
      $months = floor(($diff - $years * 365 * 60 * 60 * 24) / (30 * 60 * 60 * 24));
      $days = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24) / (60 * 60 * 24));
      //                            echo 'program sequence : ' . $program_sequence->sequence . ' days : ' . $days;
      //                            echo '<br>';

      if ($program_sequence->sequence == $days) {
      echo $program_sequence->id . ' ##### program posted ######## program : ' . $program->name . '## user : ' . $user_id . ' ## sequence id : ' . $program_sequence->id . ' ## day : ' . $days . ' ## subscribed date : ' . $program_group->start_date . ' ## offset : ' . $program_group_mapping->offset;
      echo '<br>';
      //                                if($user_id=='5d8fe13f-fe6b-d4dc-5777-5bb44326b397')
      self::timeline_post($user_id, $pogram_id, $program_sequence, 'yes', 'no', $today);
      }
      }
      }
      }
      }
      }
      }
     */

    public static function program_daily_cron() {
        $today = isset($_REQUEST['date']) ? $_REQUEST['date'] : date('Y-m-d');

        $program_groups = ProgramGroup::where('status', '=', 'Active')->orderBy('user_id')->get();
        $policy_user_master_id = '';
        Storage::append('cron.txt', "###################################################### program daily cron started  ########################################################" . date('Y-m-d H:i:s'));
        foreach ($program_groups as $program_group) {
            $user_id = $program_group->user_id;
            $policy_user_master_id = $program_group->policy_user_master_id;
            $program_group_mappings = ProgramGroupMapping::where('program_group_id', '=', $program_group->id)->get();
            foreach ($program_group_mappings as $program_group_mapping) {
                $programs = Program::where('id', '=', $program_group_mapping->program_id)->get();
                foreach ($programs as $program) {
                    $subscription_date = date('Y-m-d', strtotime($program_group_mapping->created_at));
                    $offset = $program_group_mapping->offset;
                    $pogram_id = $program->id;
                    $program_send_dates = self::program_send_dates($program->id, $subscription_date, $offset);

                    if (isset($program_send_dates[$today])) {
                        foreach ($program_send_dates[$today] as $program_sequence) {
                            echo $program_sequence->id . ' posted to ' . $user_id . '<br>';
                            //self::timeline_post($user_id, $pogram_id, $program_sequence, 'no', 'yes', $today);
                            self::timeline_post($user_id, $pogram_id, $program_sequence, 'yes', 'no', $today, $policy_user_master_id);
                        }
                    }
                }
            }
        }
        Storage::append('cron.txt', "###################################################### program daily cron ended  ########################################################" . date('Y-m-d H:i:s'));
    }

//    public static function program_future_cron_old() {
//        $future_days = 10;
//        $today = isset($_REQUEST['date']) ? $_REQUEST['date'] : date('Y-m-d');
////        $today = '2019-07-23';
//        $mod_date = strtotime($today . "+ $future_days days");
//        $today = date("Y-m-d", $mod_date);
//
////        $today = '2019-07-12';
//        $program_groups = ProgramGroup::where('status', '=', 'Active')->get();
//        foreach ($program_groups as $program_group) {
//            $user_id = $program_group->user_id;
//            $program_group_mappings = ProgramGroupMapping::where('program_group_id', '=', $program_group->id)->get();
//            foreach ($program_group_mappings as $program_group_mapping) {
//                $programs = Program::where('id', '=', $program_group_mapping->program_id)->get();
//                foreach ($programs as $program) {
//                    $subscription_date = $program_group->start_date;
//                    $subscription_date = date('Y-m-d', strtotime($subscription_date . ' + ' . $program_group_mapping->offset . ' days'));
////                    echo "program : " . $program->id . " user : " . $user_id . "offset : " . $program_group_mapping->offset . " subscription date : " . $subscription_date;
////                    echo '<br>';
//                    $pogram_id = $program->id;
//                    $program_sequences = ProgramSequenceMaster::where('program_id', '=', $program->id)->get();
//                    foreach ($program_sequences as $program_sequence) {
//                        if (strtotime($today) >= strtotime($subscription_date)) {
//                            $diff = abs(strtotime($today) - strtotime($subscription_date));
//                            $years = floor($diff / (365 * 60 * 60 * 24));
//                            $months = floor(($diff - $years * 365 * 60 * 60 * 24) / (30 * 60 * 60 * 24));
//                            $days = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24) / (60 * 60 * 24));
////                            echo 'program sequence : ' . $program_sequence->sequence . ' days : ' . $days;
////                            echo '<br>';
//                            if ($program_sequence->sequence == $days) {
//                                echo '##### program posted ######## program : ' . $program->name . '## user : ' . $user_id . ' ## sequence id : ' . $program_sequence->id . ' ## day : ' . $days . ' ## subscribed date : ' . $program_group->start_date . ' ## offset : ' . $program_group_mapping->offset;
//                                echo '<br>';
////                                if($user_id=='5d8fe13f-fe6b-d4dc-5777-5bb44326b397')
//                                self::timeline_post($user_id, $pogram_id, $program_sequence, 'no', 'yes', $today);
//                            }
//                        }
//                    }
//                }
//            }
//        }
//    }

    public static function program_future_cron() {
        $future_days = 10;
        $today = isset($_REQUEST['date']) ? $_REQUEST['date'] : date('Y-m-d');
//        $today = '2019-07-23';
        $mod_date = strtotime($today . "+ $future_days days");
        $today = date("Y-m-d", $mod_date);
        $policy_user_master_id = '';

        $program_groups = ProgramGroup::where('status', '=', 'Active')->get();
        foreach ($program_groups as $program_group) {
            $user_id = $program_group->user_id;
            $policy_user_master_id = $program_group->policy_user_master_id;
            $program_group_mappings = ProgramGroupMapping::where('program_group_id', '=', $program_group->id)->get();
            foreach ($program_group_mappings as $program_group_mapping) {
                $programs = Program::where('id', '=', $program_group_mapping->program_id)->get();
                foreach ($programs as $program) {
                    $subscription_date = date('Y-m-d', strtotime($program_group_mapping->created_at));
                    $offset = $program_group_mapping->offset;
                    $pogram_id = $program->id;
                    $program_send_dates = self::program_send_dates($program->id, $subscription_date, $offset);

                    if (isset($program_send_dates[$today])) {
                        foreach ($program_send_dates[$today] as $program_sequence) {
                            echo $program_sequence->id . ' posted to ' . $user_id . '<br>';
                            self::timeline_post($user_id, $pogram_id, $program_sequence, 'no', 'yes', $today, $policy_user_master_id);
                        }
                    }
                }
            }
        }
    }

    public static function timeline_post($user_id, $pogram_id, $program_sequence, $send_chat = 'yes', $send_post = 'yes', $date, $policy_user_master_id) {
        Storage::append('cron.txt', "###################################################### inside timeline post  ######################################################## user : $user_id program : $pogram_id program_sequence id : " . $program_sequence->id . " program_sequence type : " . $program_sequence->type);
        $sex = 0;
        $program_sex_male = $program_sequence->male;
        $program_sex_female = $program_sequence->female;
        $ucstm = UserCSTM::where('id_c', '=', $user_id)->first();
        if ($ucstm) {
            $sex = $ucstm[0]['user_gender_c'];
        }
        if ($program_sex_male == 1 && $sex == 2) {
            return;
        } elseif ($program_sex_female == 1 && $sex == 1) {
            return;
        }
        //send sms   

        if ($send_chat == 'yes' && $program_sequence->type == "sms") {
            $program_group = ProgramGroup::where('assigned_id', '=', $user_id)->where('policy_user_master_id', '=', $policy_user_master_id)->first();
            if (isset($program_group->subscription_type)) {
                if ($program_group->subscription_type == 'sms') {

                    $mobile = '';
                    $user = User::where('id', '=', $user_id)->where('deleted', '=', 0)->first();
                    if (isset($user->id)) {
                        $dialog = new DialogSubscriptionRepository();
                        $dash_board_url = "http://connect.ayubo.life/program_dashboard/web?userid=" . $user_id . "&pumid=" . $policy_user_master_id;
                        if (strpos($program_sequence->notification_text, '##web_dashboard_url##') !== false) {
                            $dash_board_url = urlencode($dash_board_url);
                            $dash_board_url_obj = \App\Libs\BitlyUrl::get_bitly_url($dash_board_url);
                            if (isset($dash_board_url_obj->data->url)) {
                                $dash_board_url = $dash_board_url_obj->data->url;
                            }
                            Storage::append('dialog_subscripiton.txt', 'bitly url' . print_r($dash_board_url_obj, 1));
                        }
                        $sms_notification_text = str_replace('##user_id##', $user_id, $program_sequence->notification_text);
                        $sms_notification_text = str_replace('##pum_id##', $policy_user_master_id, $sms_notification_text);
                        $sms_notification_text = str_replace('##web_dashboard_url##', $dash_board_url, $sms_notification_text);
//                        echo $program_sequence->relate_id . '-----' . $user->user_name . ' bitly ' . $dash_board_url . "<br>";
                        $dialog->sendSMS($program_sequence->relate_id, $user->user_name, $sms_notification_text);
                        sleep(1);
                        Storage::append('dialog_subscripiton.txt', '---- inside timeline_post send sms--------#######################  ----------## ');
                    }
                }
            }
        }
        //send sms
//////////////////////////////////////////////timeline post///////////////////////////////////////////////////
        $GLOBALS["sugar_config"]["site_url"] = 'https://livehappy.ayubo.life';
        $GLOBALS['sugar_config']['upload_dir'] = '/datadrive/wellness_upload/upload/';
        $GLOBALS['sugar_config']['api_upload_dir'] = '/var/www/html/wellness.hemas.life/api.ayubo.life/public/images/';

        $interaction = '0';
        $created_by = '9f4aa4a4-0c80-8bfd-ba41-5894069edb38';
        if ($program_sequence->type == 'challenge') {
            $redirect_type = 'challenge';
        } else {
            $redirect_type = 'common';
        }
        $community_ids = '';
        $assigned_user_ids = $user_id;


        $type = strtolower($program_sequence->type);
        $tile = strtolower($program_sequence->tile);
        $heading = html_entity_decode($program_sequence->timeline_header, ENT_QUOTES);
        $time = $program_sequence->post_time;
        $related_secondary_id = $program_sequence->id;
        $related_primary_id = $pogram_id;
        $meta = $program_sequence->relate_id;

        $notification_header = html_entity_decode($program_sequence->notification_header, ENT_QUOTES);
        $notification_text = html_entity_decode($program_sequence->notification_text, ENT_QUOTES);
        $content_icon = $program_sequence->content_icon;

        $created_at = $date . ' ' . $time;

        $paragraph = html_entity_decode($program_sequence->timeline_text, ENT_QUOTES);
        $sub_heading = html_entity_decode($program_sequence->sub_heading, ENT_QUOTES);

        $image_url = '';
        if (!empty($program_sequence->timeline_image_link)) {
            if (substr(trim($program_sequence->timeline_image_link), 0, 4) != 'http' && $type != 'video') {
                $image_url = $GLOBALS["sugar_config"]["site_url"] . trim($program_sequence->timeline_image_link);
            } else {
                $image_url = trim($program_sequence->timeline_image_link);
            }
        }
        $redirect_url = $program_sequence->inner_content;

        $button_text = 'View';
        $post_id = '';
        $pdmtag = [];
        if (!empty($program_sequence->tag_name_1)) {
            $pdmtag[] = ['text' => $program_sequence->tag_name_1, 'icon' => $program_sequence->tag_icon_1];
        }
        if (!empty($program_sequence->tag_name_2)) {
            $pdmtag[] = ['text' => $program_sequence->tag_name_2, 'icon' => $program_sequence->tag_icon_2];
        }
        if (!empty($program_sequence->tag_name_3)) {
            $pdmtag[] = ['text' => $program_sequence->tag_name_3, 'icon' => $program_sequence->tag_icon_3];
        }

        $program_dashboard_tags = json_encode($pdmtag);

        if ($type == 'system' && $send_post == 'yes') {
            if (!empty($heading) || !empty($paragraph) || !empty($image_url) || !empty($redirect_url)) {
                $post_id = self::native_timeline_post($heading, $paragraph, "SYSTEM_POST", $image_url, $created_by, $redirect_type, $redirect_url, $interaction, $created_at, $assigned_user_ids, $community_ids, '', 'program', $related_primary_id, $related_secondary_id, $program_dashboard_tags, $content_icon, $sub_heading, '', $policy_user_master_id);
            }
        } elseif ($type == 'program' && $send_post == 'yes') {
            if (!empty($heading) || !empty($paragraph) || !empty($image_url) || !empty($redirect_url)) {
                $post_id = self::native_timeline_post($heading, $paragraph, "PROGRAM_POST", $image_url, $created_by, $redirect_type, $redirect_url, $interaction, $created_at, $assigned_user_ids, $community_ids, $button_text, 'program', $related_primary_id, $related_secondary_id, $program_dashboard_tags, $content_icon, $sub_heading, '', $policy_user_master_id);
            }
        } elseif ($type == 'video' && $send_post == 'yes') {
            $redirect_url = $program_sequence->timeline_video_link;
            if (!empty($heading) || !empty($paragraph) || !empty($image_url) || !empty($redirect_url)) {
                $post_id = self::native_timeline_post($heading, $paragraph, "VIDEO_POST", $image_url, $created_by, $redirect_type, $redirect_url, $interaction, $created_at, $assigned_user_ids, $community_ids, $button_text, 'program', $related_primary_id, $related_secondary_id, $program_dashboard_tags, $content_icon, $sub_heading, '', $policy_user_master_id);
            }
        } elseif ($type == 'text' && $send_post == 'yes') {
            if (!empty($heading) || !empty($paragraph) || !empty($image_url) || !empty($redirect_url)) {
                $post_id = self::native_timeline_post($heading, $paragraph, "SYSTEM_POST", $image_url, $created_by, $redirect_type, $redirect_url, $interaction, $created_at, $assigned_user_ids, $community_ids, $button_text, 'program', $related_primary_id, $related_secondary_id, $program_dashboard_tags, $content_icon, $sub_heading, '', $policy_user_master_id);
            }
        } elseif ($type == 'image' && $send_post == 'yes') {
            if (!empty($heading) || !empty($paragraph) || !empty($image_url) || !empty($redirect_url)) {
                $post_id = self::native_timeline_post($heading, $paragraph, "SYSTEM_POST", $image_url, $created_by, $redirect_type, $redirect_url, $interaction, $created_at, $assigned_user_ids, $community_ids, $button_text, 'program', $related_primary_id, $related_secondary_id, $program_dashboard_tags, $content_icon, $sub_heading, '', $policy_user_master_id);
            }
        } elseif ($type == 'web' && $send_post == 'yes') {
            if (!empty($heading) || !empty($paragraph) || !empty($image_url) || !empty($redirect_url)) {
                $redirect_type = 'web';
                $post_id = self::native_timeline_post($heading, $paragraph, "SYSTEM_POST", $image_url, $created_by, $redirect_type, $redirect_url, $interaction, $created_at, $assigned_user_ids, $community_ids, $button_text, 'program', $related_primary_id, $related_secondary_id, $program_dashboard_tags, $content_icon, $sub_heading, $meta, $policy_user_master_id);
            }
        } elseif ($type == 'question' && $send_post == 'yes') {
            if (!empty($heading)) {
                $redirect_type = 'dynamicquestion';
                $post_id = self::native_timeline_post($heading, $paragraph, "SYSTEM_POST", $image_url, $created_by, $redirect_type, $redirect_url, $interaction, $created_at, $assigned_user_ids, $community_ids, $button_text, 'program', $related_primary_id, $related_secondary_id, $program_dashboard_tags, $content_icon, $sub_heading, $meta, $policy_user_master_id);
            }
        } elseif ($type == 'chat' && $send_chat == 'yes') {
            $chat_relate_id = $program_sequence->relate_id;
            $chat_relate_text = $program_sequence->relate_text;
            $dr_name = 'Doctor';
            $user_first_name = '';
            if (!empty($chat_relate_id)) {
                $contact = Contact::where('id', '=', $chat_relate_id)->first();
                if ($contact) {
                    $dr_name = $contact->salutation . ' ' . $contact->first_name . ' ' . $contact->last_name;
                }
                $user_info = User::where('id', '=', $user_id)->first();
                if ($user_info) {
                    $user_first_name = $user_info->first_name . ' ' . $user_info->last_name;
                }
            }
            $chat_relate_text = str_replace('<insert relevant doctor name>', $dr_name, $chat_relate_text);
            $chat_relate_text = str_replace('<insert user name>', $user_first_name, $chat_relate_text);

            if (!empty($chat_relate_id) && !empty($chat_relate_text)) {
                $post = array('type' => 'chat_with_doctor',
                    'user_id' => $user_id,
                    'chat_relate_id' => $chat_relate_id,
                    'chat_relate_text' => $chat_relate_text,
                );
                $service_data = JanashakthiRepository::sugar_service($post);
            }
        } elseif ($type == 'chat_quiz' && $send_chat == 'yes') {
            $chat_quiz_relate_id = $program_sequence->relate_id;
            $chat_quiz_relate_text = $program_sequence->relate_text;
            if (!empty($chat_quiz_relate_id) && !empty($chat_quiz_relate_text)) {
//                $nativeChatRepository = new NativeChatRepository;
//                $nativeChatRepository->button_pressed(null,['user_id'=>$user_id,'button_id'=>$chat_quiz_relate_id]);
            }
        } elseif ($type == 'challenge' && $send_chat == 'yes' && $send_post == 'no') {
            $redirect_url = $program_sequence->relate_id;
            if (!empty($heading) || !empty($paragraph) || !empty($image_url) || !empty($redirect_url)) {
                $post_id = self::native_timeline_post($heading, $paragraph, "SYSTEM_POST", $image_url, $created_by, $redirect_type, $redirect_url, $interaction, $created_at, $assigned_user_ids, $community_ids, '', 'program', $related_primary_id, $related_secondary_id, $program_dashboard_tags, $content_icon, $sub_heading, '', $policy_user_master_id);
            }
        } elseif ($type == 'native_post' && $send_post == 'yes') {
            $redirect_type = $type;
            $meta = $program_sequence->relate_id;
            $redirect_url = $meta;
            if (!empty($heading) || !empty($paragraph)) {
                $post_id = self::native_timeline_post($heading, $paragraph, "SYSTEM_POST", $image_url, $created_by, $redirect_type, $redirect_url, $interaction, $created_at, $assigned_user_ids, $community_ids, '', 'program', $related_primary_id, $related_secondary_id, $program_dashboard_tags, $content_icon, $sub_heading, $meta, $policy_user_master_id);
            }
        }

        $relate_type = 'JANASHAKTHI_POLICY';
//        $relate_id = $program_sequence->relate_id;
        $relate_id = self::get_relate_id($user_id);
        $notification_header = str_replace("'", "\'", $notification_header);
        $notification_text = str_replace("'", "\'", html_entity_decode($notification_text, ENT_QUOTES));
        $time = $program_sequence->post_time;
        $assigned_user_id = $user_id;
//        $image_url = $GLOBALS["sugar_config"]["site_url"] . '/resize.php?img=' . $GLOBALS['sugar_config']['upload_dir'] . $relate_id . '_picture';
        //notification post
        if ((!empty($notification_header) || !empty($notification_text)) && $send_chat == 'yes' && !empty($relate_id)) {
            $dr_name = 'Doctor';
            $user_first_name = '';
            if (!empty($relate_id)) {
                $contact = Contact::where('id', '=', $relate_id)->first();
                if ($contact) {
                    $dr_name = $contact->salutation . ' ' . $contact->first_name . ' ' . $contact->last_name;
                }
                $user_info = User::where('id', '=', $user_id)->first();
                if ($user_info) {
                    $user_first_name = $user_info->first_name . ' ' . $user_info->last_name;
                }
            }
            $notification_header = str_replace('<insert relevant doctor name>', $dr_name, $notification_header);
            $notification_header = str_replace('<insert user name>', $user_first_name, $notification_header);
            $notification_text = str_replace('<insert relevant doctor name>', $dr_name, $notification_text);
            $notification_text = str_replace('<insert user name>', $user_first_name, $notification_text);

            $notification_data = array();
            $notification_data["relate_type"] = $relate_type;
            $notification_data["relate_id"] = $relate_id;
            $notification_data["heading"] = stripslashes($notification_header);
            $notification_data["text"] = stripslashes($notification_text);
            $notification_data["assigned_user_id"] = $assigned_user_id;
            $notification_data["image_url"] = $image_url;
            $notification_data["created_at"] = $created_at;
//            $notification_insert_id = DB::table('notification_cron')->insert($notification_data);
        }
        //send notification to user        
        if ((!empty($notification_text)) && !empty($notification_header) && $send_post == 'yes') {
            self::createNotification($notification_header, $notification_text, $user_id, 'general', '', $created_at, $relate_id, $post_id, $meta,$policy_user_master_id);
        }
        //tile post
        if ($tile == 'yes' && $send_chat == 'yes') {
            $name = $program_sequence->tile_text;
            $tile_image = $program_sequence->tile_image_link;

            $tile_data = array();
            $tile_data["related_id"] = $related_primary_id;
            $tile_data["related_type"] = 'post';
            $tile_data["assigned_user_id"] = $user_id;
            $tile_data["title"] = $name;
            $tile_data["sub_title"] = '';
            $tile_data["action_text"] = 'View';
            $tile_data["link"] = $post_id;
            $tile_data["Image"] = $tile_image;
            $tile_data["visible_on"] = $created_at;
            $tile_data["expire_on"] = $created_at;
            $tile_data["status"] = 'Active';
            $tile_data["section"] = '';
            $tile_data_insert_id = DB::table('today_tiles')->insert($tile_data);
        }
        //tile post
//////////////////////////////////////////////timeline post///////////////////////////////////////////////////        
    }

    public static function native_timeline_post($heading, $body, $type = "SYSTEM_POST", $embed_url, $created_by, $redirect_type, $url, $interaction = 1, $created_at, $assigned_users, $community_list, $extra, $related_module, $related_primary_id, $related_secondary_id, $program_dashboard_tags = '', $content_icon, $sub_heading, $meta = '', $policy_user_master_id) {
        //delete same post posted again
        self::delete_old_post($heading, $body, $related_primary_id, $related_secondary_id, $assigned_users);
        //delete same post posted again

        $GLOBALS["sugar_config"]["site_url"] = 'https://livehappy.ayubo.life';
        $GLOBALS['sugar_config']['upload_dir'] = '/datadrive/wellness_upload/upload/';
        $GLOBALS['sugar_config']['api_upload_dir'] = '/var/www/html/wellness.hemas.life/api.ayubo.life/public/images/';
        $post_insert_id = "";

        $body = html_entity_decode($body);
        $post_data = array();
        $post_data["heading"] = $heading;
        $post_data["body"] = strip_tags($body);
        $post_data["type"] = $type;
        $post_data["embed_url"] = $embed_url;
        $post_data["interaction"] = $interaction;
        $post_data["created_by"] = $created_by;
        $post_data["redirect_type"] = $redirect_type;
        $post_data["url"] = $url;
        $post_data["created_at"] = strtotime($created_at);
        $post_data["updated_at"] = strtotime($created_at);

        if (empty($post_data["created_at"])) {
            $post_data["created_at"] = strtotime("now");
        }
        if (empty($post_data["updated_at"])) {
            $post_data["updated_at"] = strtotime("now");
        }

        if (!empty($related_module))
            $post_data["related_module"] = $related_module;

        if (!empty($related_primary_id))
            $post_data["related_primary_id"] = $related_primary_id;

        if (!empty($related_secondary_id))
            $post_data["related_secondary_id"] = $related_secondary_id;

        if (!empty($extra))
            $post_data["extra"] = $extra;

        if ($type == "VIDEO_POST") {
            $post_data["embed_url"] = "";
            $post_data["url"] = "";
        }
        $post_insert_id = 777;
        if ((!empty($post_data["body"]) || !empty($post_data["heading"]) || !empty($post_data["embed_url"]) || ($type == "VIDEO_POST" && !empty($embed_url) && !empty($url))) && (!empty($assigned_users) || !empty($community_list))) {
//            $post_insert_id = insert_posts("posts", $post_data);
            if ($redirect_type != 'dynamicquestion' && $redirect_type != 'challenge') {
                $post_insert_id = DB::table('posts')->insert($post_data);
                $post_insert_id = DB::getPdo()->lastInsertId();
            }
        }

        if (!empty($post_insert_id)) {

            $assigned_users_array = explode(",", $assigned_users);
            foreach ($assigned_users_array as $assigned_users_row) {
                if ($redirect_type != 'dynamicquestion' && $redirect_type != 'challenge') {
                    DB::insert("INSERT INTO post_tagged_users (post_id, user_id, created_at, updated_at) VALUES ($post_insert_id, '$assigned_users_row', '{$post_data["created_at"]}', '{$post_data["updated_at"]}')");
                }
                //insert post program dashboard
                $dashborad_type = ($type == "VIDEO_POST") ? 'video' : 'image';
                $action = 'post';
//                $sub_heading = 'View';
                if ($redirect_type == 'dynamicquestion') {
                    $dashborad_type = 'question';
                    $post_insert_id = $meta;
                    $action = 'dynamicquestion';
//                    $sub_heading = 'Assessment';
                }

                if ($redirect_type == 'native_post') {
                    $dashborad_type = 'native_post';
                    $post_insert_id = $meta;
                    $action = 'native_post';
                }
                if ($redirect_type == 'web') {
                    $dashborad_type = 'image';
                    $post_insert_id = $meta;
                    $action = 'web';
                }

                if ($redirect_type == 'challenge') {
                    $action = 'challenge';
                    $dashborad_type = 'challenge';
                    $post_insert_id = $url;
//                    $sub_heading = 'Start now';
                    //call the join challenge service
                    $sugar = new SugarApi("https://livehappy.ayubo.life/custom/service/v5/rest.php");
                    $rest_data = ["user_id" => $assigned_users_row, "challenge_id" => $url];
                    $result = $sugar->send($rest_data, 'join_adventure_challenge');
                    echo 'challege service called';
                    //call the join challenge service
                }

                $program_group = ProgramGroup::where('user_id', '=', $assigned_users_row)->where('policy_user_master_id', '=', $policy_user_master_id)->orderBy('id', 'desc')->first();
                $pro_group_id = $program_group->id;
                self::delete_old_post_program_dashboard($assigned_users_row, $heading, $sub_heading, $action, $created_at, $pro_group_id);

                $program_dashboard["user_id"] = $assigned_users_row;
                $program_dashboard["heading"] = $heading;
                $program_dashboard["sub_heading"] = $sub_heading;
                $program_dashboard["text"] = $post_data["body"];
                $program_dashboard["icon"] = $embed_url;
                $program_dashboard["is_read"] = 0;
                $program_dashboard["action"] = $action;
                $program_dashboard["meta"] = $post_insert_id;
                $program_dashboard["type"] = $dashborad_type;
                $program_dashboard["disabled"] = 0;
                $program_dashboard["progress_status"] = 'incomplete';
                $program_dashboard["progress_progress"] = 0;
                $program_dashboard["icon"] = $content_icon;
                $program_dashboard["tags"] = $program_dashboard_tags;
                $program_dashboard["timestamp"] = strtotime($created_at);
                $program_dashboard["program_group_id"] = $pro_group_id;
                $program_dashboard["created_at"] = $created_at;
                $program_dashboard["updated_at"] = $created_at;
                DB::table('program_dashboard_master')->insert($program_dashboard);
            }

            if (!empty($community_list)) {
                if ($community_list == "ALL") {
                    $community_list = "1";
                } else if ($community_list == "PUBLIC") {
//                    $community_list = $GLOBALS["db"]->getOne("SELECT GROUP_CONCAT(a.accounts_users_2accounts_ida) AS accounts FROM accounts_users_2_c AS a WHERE a.company_enrolled_c = 1  AND a.accounts_users_2users_idb = '{$post_data["created_by"]}'  AND a.deleted = 0  GROUP BY a.accounts_users_2users_idb");
                    $community_list = DB::select(DB::raw("SELECT GROUP_CONCAT(a.accounts_users_2accounts_ida) AS accounts FROM accounts_users_2_c AS a WHERE a.company_enrolled_c = 1  AND a.accounts_users_2users_idb = '{$post_data["created_by"]}'  AND a.deleted = 0  GROUP BY a.accounts_users_2users_idb"))->first();
                    if (isset($community_list[0])) {
                        $community_list = $community_list[0]->accounts;
                    }
                }

                $community_list_exploded = explode(",", $community_list);
                foreach ($community_list_exploded as $each_comunity) {
//                    $GLOBALS["db"]->query("INSERT INTO post_communities (post_id, community_id, created_at, updated_at) VALUES ($post_insert_id, '$each_comunity', '{$post_data["updated_at"]}', '{$post_data["updated_at"]}') ");
                    DB::insert("INSERT INTO post_communities (post_id, community_id, created_at, updated_at) VALUES ($post_insert_id, '$each_comunity', '{$post_data["updated_at"]}', '{$post_data["updated_at"]}')");
                }
            }

            if ($type == "VIDEO_POST") {
                $media_thumbnail = array();
                $media_thumbnail["post_id"] = $post_insert_id;
                $media_thumbnail["media_type"] = "image";
                $media_thumbnail["name"] = $embed_url;
                if (file_exists("{$GLOBALS["sugar_config"]["api_upload_dir"]}thumbnail/$embed_url")) {
                    $media_thumbnail["mime_type"] = mime_content_type("{$GLOBALS["sugar_config"]["api_upload_dir"]}thumbnail/$embed_url");
                }
                $media_thumbnail["created_at"] = strtotime($created_at);
                $media_thumbnail["updated_at"] = strtotime($created_at);
//                insert_posts("post_media", $media_thumbnail);
                DB::table('post_media')->insert($media_thumbnail);

                $media_video = array();
                $media_video["post_id"] = $post_insert_id;
                $media_video["media_type"] = "video";
                $media_video["name"] = $url;
                if (file_exists("{$GLOBALS["sugar_config"]["api_upload_dir"]}$url")) {
                    $media_video["mime_type"] = mime_content_type("{$GLOBALS["sugar_config"]["api_upload_dir"]}$url");
                }
                $media_video["created_at"] = strtotime($created_at);
                $media_video["updated_at"] = strtotime($created_at);
//                insert_posts("post_media", $media_video);
                DB::table('post_media')->insert($media_video);
            }

            return $post_insert_id;
        }
    }

    public static function createNotification($title, $description, $assigned_user, $type = "general", $frequency, $execute_on, $program_id = '', $post_id, $meta,$policy_user_master_id) {
//        echo "$description $execute_on ".date('Y-m-d H:i:s').'<br>';
        if ($execute_on < date('Y-m-d H:i:s')) {
            return;
        }
        $dr_name = 'Doctor';
        if (!empty($meta)) {
            $contact = Contact::where('id', '=', $meta)->first();
            if ($contact) {
                $dr_name = $contact->salutation . ' ' . $contact->first_name . ' ' . $contact->last_name;
            }
        }

        $title = stripslashes($title);
        $title = html_entity_decode($title, ENT_QUOTES);
        $description = stripslashes($description);
        $description = html_entity_decode($description, ENT_QUOTES);

        $description = str_replace('<insert relevant doctor name>', $dr_name, $description);

        //delete the same post already submitted
//        $title = html_entity_decode($title, ENT_QUOTES);
//        $title = str_replace("'", "\'", $title);
//        $description = html_entity_decode($description, ENT_QUOTES);
//        $description = str_replace("'", "\'", $description);
//        DB::statement("DELETE FROM `hal_notifications` WHERE name='".$title."' AND description='".$description."' AND assigned_user_id='$assigned_user'");
        DB::statement("UPDATE `hal_notifications` SET deleted=1  WHERE name='" . str_replace("'", "\'", $title) . "' AND description='" . str_replace("'", "\'", $description) . "' AND assigned_user_id='$assigned_user'");

        $input_array["id"] = Uuid::generate()->string;
        $input_array["name"] = $title;        
        $input_array["description"] = $description;
        $input_array["assigned_user_id"] = $assigned_user;
        $input_array["notification_type"] = $type;
        $input_array["push_flag"] = 1;
        $input_array["program_id"] = $program_id;
        $input_array["post_id"] = $post_id;
        $input_array["action"] = "programtimeline";
        $input_array["meta"] = $policy_user_master_id;
        if (!empty($frequency)) {
            $input_array["frequency"] = $frequency;
        }
        if (!empty($execute_on)) {
            $input_array["execute_on"] = $execute_on;
            $input_array["date_entered"] = $execute_on;
        }
        return DB::table('hal_notifications')->insert($input_array);
//        return insert_into_table($input_array,"hal_notifications");
    }

    public static function delete_old_post($heading, $body, $related_primary_id, $related_secondary_id, $assigned_users) {
        $posts = Post::select('posts.id', 'post_tagged_users.user_id')
                ->join('post_tagged_users', 'posts.id', '=', 'post_tagged_users.post_id')
                ->where('posts.heading', $heading)
                ->where('posts.body', $body)
                ->where('posts.related_primary_id', $related_primary_id)
                ->where('posts.related_secondary_id', $related_secondary_id)
                ->get();
        $assigned_users_array = explode(",", $assigned_users);
        foreach ($posts as $post) {
            if (in_array($post->user_id, $assigned_users_array)) {
                $post->delete();
            }
        }
    }

    public static function delete_old_post_program_dashboard($user_id, $heading, $sub_heading, $action, $date, $pro_group_id) {
        $programs = ProgramDashboardMaster::where('user_id', '=', $user_id)
                ->where('heading', '=', $heading)
                ->where('sub_heading', '=', $sub_heading)
                ->where('action', '=', $action)
                ->where('program_group_id', '=', $pro_group_id)
                ->where('created_at', 'like', '%' . substr($date, 0, 10) . '%')
                ->get();
        foreach ($programs as $program) {
            $program->delete();
        }
    }

    public static function program_send_dates($program_id, $subscription_date, $offset) {
        $send = [];
        $sequence = ProgramSequenceMaster::where('program_id', '=', $program_id)->get();
        foreach ($sequence as $seq) {
            $send_date = date('Y-m-d', strtotime($subscription_date . ' + ' . ($seq->sequence + $offset - 1) . ' days'));
            $send[$send_date][] = $seq;
        }
        return $send;
    }

    public static function get_relate_id($user_id) {
        $policy_user_master = PolicyUserMaster::where('user_id', '=', $user_id)->orderBy('id', 'desc')->first();
        if ($policy_user_master) {
            return $policy_user_master->id;
        }
        return '';
    }

    public static function program_manual_post($date, $user_id, $policy_user_master_id) {
        //delete all program data for the user 
//        $res = ProgramDashboardMaster::where('us er_id', $user_id)->delete();

        $date_from = $date;
        $date_from = strtotime($date_from);

        $Date = date('Y-m-d');
        $date_to = date('Y-m-d', strtotime($Date . ' + 20 days'));
        $date_to = strtotime($date_to);
        for ($i = $date_from; $i <= $date_to; $i += 86400) {
            $today = date("Y-m-d", $i);
//            echo '<br>';
//            continue;

            $program_groups = ProgramGroup::where('status', '=', 'Active')->where('user_id', '=', $user_id)->where('policy_user_master_id', '=', $policy_user_master_id)->get();
            foreach ($program_groups as $program_group) {
                $user_id = $program_group->user_id;
                $program_group_id = $program_group->id;
//                ProgramDashboardMaster::where('user_id', $user_id)->where('program_group_id', $program_group_id)->delete();
                $program_group_mappings = ProgramGroupMapping::where('program_group_id', '=', $program_group->id)->get();
//                $program_group_mappings = ProgramGroupMapping::where('program_group_id', '=', $program_group->id)->where('program_id', '=', 7)->get(); 
                foreach ($program_group_mappings as $program_group_mapping) {
                    $programs = Program::where('id', '=', $program_group_mapping->program_id)->get();
                    foreach ($programs as $program) {
                        $subscription_date = date('Y-m-d', strtotime($program_group_mapping->created_at));
                        $offset = $program_group_mapping->offset;
                        $pogram_id = $program->id;
                        $program_send_dates = self::program_send_dates($program->id, $subscription_date, $offset);

                        if (isset($program_send_dates[$today])) {
                            foreach ($program_send_dates[$today] as $program_sequence) {
//                                echo $program_sequence->id . ' posted to ' . $user_id . '<br>';
//                                if ($program_sequence->id == '1247') {
//                                    echo $today;
                                    self::timeline_post($user_id, $pogram_id, $program_sequence, 'no', 'yes', $today, $policy_user_master_id);
//                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public static function program_manual_post_one_program($date, $user_id, $policy_user_master_id, $program_id) {
        //delete all program data for the user
//        $res = ProgramDashboardMaster::where('user_id', $user_id)->delete();

        $date_from = $date;
        $date_from = strtotime($date_from);

        $Date = date('Y-m-d');
        $date_to = date('Y-m-d', strtotime($Date . ' + 60 days'));
        $date_to = strtotime($date_to);
        for ($i = $date_from; $i <= $date_to; $i += 86400) {
            echo $today = date("Y-m-d", $i);
            echo '<br>';
//            continue;
            $program_group = ProgramGroup::where('status', '=', 'Active')->where('user_id', '=', $user_id)->where('policy_user_master_id', '=', $policy_user_master_id)->first();
            $program_group_mappings = ProgramGroupMapping::where('program_group_id', '=', $program_group->id)->where('program_id', '=', $program_id)->get();
            foreach ($program_group_mappings as $program_group_mapping) {
                $programs = Program::where('id', '=', $program_group_mapping->program_id)->get();
                foreach ($programs as $program) {
                    $subscription_date = date('Y-m-d', strtotime($program_group_mapping->created_at));
                    $offset = $program_group_mapping->offset;
                    $pogram_id = $program->id;
                    $program_send_dates = self::program_send_dates($program->id, $subscription_date, $offset);

                    if (isset($program_send_dates[$today])) {
                        foreach ($program_send_dates[$today] as $program_sequence) {
                            echo $program_sequence->id . ' posted to ' . $user_id . '<br>';
                            self::timeline_post($user_id, $pogram_id, $program_sequence, 'no', 'yes', $today, $policy_user_master_id);
                        }
                    }
                }
            }
        }
    }

    //this is for individual program like sldf subscribe
    public static function subscribe($request) {
        $user_id = $request->user_id;
        $program_id = $request->program_id;
        $subscription_type = isset($request->subscription_type) ? $request->subscription_type : '';
        $app_id = isset($request->app_id) ? $request->app_id : '';

        $run_for_days = 11;
        $program_all = [1,2,3,4,5,9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 38, 39,40,41,42];  
        if (in_array($program_id, $program_all)) {
            $psm = ProgramSequenceMaster::where('program_id', '=', $program_id)->orderBy('sequence', 'desc')->first();
            ;
            $run_for_days = ($psm->sequence) + 1;
        }

        if (isset($request->subscription_type)) {
            $subscription_type = $request->subscription_type;
        }
        if (isset($request->app_id)) {
            $app_id = $request->app_id;
        }
        //program data
        $program_name = NULL;
        $program = Program::where('id', '=', $program_id)->first();
        if (isset($program->name)) {
            $program_name = $program->name;
        }
        //insert program group
        $policy_user_master = PolicyUserMaster::where('user_id', '=', $user_id)->orderBy('id', 'desc')->first();
        $program_group = ProgramGroup::create(
                        [
                            'user_id' => $user_id,
                            'name' => $program_name,
                            'assigned_type' => 'program',
                            'assigned_id' => $user_id,
                            'policy_user_master_id' => $policy_user_master->id,
                            'status' => 'Active',
                            'start_date' => date('Y-m-d'),
                            'subscription_type' => $subscription_type,
                            'telco_app_id' => $app_id
                        ]
        );
        $program_group->save();

        //insert group mapping
        $offset = 0;
        $program_group_mapping = ProgramGroupMapping::firstOrNew(
                        [
                            'program_group_id' => $program_group->id,
                            'program_id' => $program_id,
                        ]
        );
        $program_group_mapping->offset = $offset;
        $program_group_mapping->save();

        //first time charge for sms program
        if ($subscription_type == 'sms' && ($program_id == '33' || $program_id == '7')) {
            $amount = 2;
            DialogSubscriptionRepository::dialog_charge_registration_fee($program_group->id, $amount, $app_id);
        }
        //first time charge for sms program        
        //send day 1 programs
        $my_program = Program::where('id', '=', $program_id)->first();
        $program_sequences = ProgramSequenceMaster::where('program_id', '=', $my_program->id)->get();
        foreach ($program_sequences as $sequence) {
            $pgm = ProgramGroupMapping::where('program_group_id', '=', $program_group->id)->where('program_id', '=', $sequence->program_id)->first();
            $offset = $pgm->offset;
            if ($sequence->sequence < $run_for_days) {
                $today = date('Y-m-d');
                $mod_date = strtotime($today . "+ " . ($sequence->sequence + $offset - 1) . " days");
                $today = date("Y-m-d", $mod_date);
                if ($sequence->sequence == 1) {
                    self::timeline_post($user_id, $my_program->id, $sequence, 'yes', 'yes', $today, $policy_user_master->id);
                } else {
                    self::timeline_post($user_id, $my_program->id, $sequence, 'no', 'yes', $today, $policy_user_master->id);
                }
            }
        }
        //send day 1 programs 
        //send sms web link
        if ($subscription_type == 'sms') {
            $user = User::where('id', '=', $user_id)->where('deleted', '=', 0)->first();
            $dialog = new DialogSubscriptionRepository();
            $link = "http://connect.ayubo.life/program_dashboard/web?userid=$user_id&pumid=" . $policy_user_master->id;
//            $dialog->sendSMS($app_id, $user->user_name, $link);
        }
        //send sms web link
        if ($program_group_mapping) {
            return success_response(0, []);
        } else {
            return error_response(1, 'error');
        }
    }

    public static function unsubscribe($request) {
        $policy_user_master_id = $request->policy_user_master_id;

        $user_id = extract_user_id($request->header('authorization'));
        $user = User::find($user_id);
        if (!$user) {
            return error_response(1, 'error');
        }


        $policy_user_master = PolicyUserMaster::where('id', '=', $policy_user_master_id)->orderBy('id', 'desc')->first();

        if ($policy_user_master) {
            $program_group_id = '';
            $program_group = ProgramGroup::where('policy_user_master_id', '=', $policy_user_master->id)->first();
            if ($program_group) {
                $program_group_id = $program_group->id;
                $program_group->status = 'Inactive';
                $program_group->save();
            }
        }
        $program_ids = [];
        if ($program_group_id) {
            $program_group_mapping = ProgramGroupMapping::where('program_group_id', '=', $program_group_id)->get();
            foreach ($program_group_mapping as $pgm) {
                $program_ids[] = $pgm->program_id;
            }
        }
        //mam program id
        $mam_subscription_program_ids = [];
        foreach ($program_ids as $program_id) {
            $mam_subscription_program = DB::select("SELECT id FROM mam_subscription_programs AS p WHERE p.description = '$program_id'  AND p.deleted = 0 LIMIT 0,1");
            foreach ($mam_subscription_program as $msp) {
                $mam_subscription_program_ids[] = $msp->id;
            }
        }
        //al payments
        $subscribed_mobile = '';
        foreach ($mam_subscription_program_ids as $mam_subscription_program_id) {
            $al_payments = DB::select("SELECT * FROM al_payments WHERE mam_subscription_programs_id_c = '$mam_subscription_program_id' AND assigned_user_id = '$user_id' AND paymentstatus=2 AND deleted = 0 AND payment_type='dialog_subscription' ORDER BY date_entered DESC LIMIT 0,1");
            Storage::append('dialog_subscripiton.txt', "SELECT * FROM al_payments WHERE mam_subscription_programs_id_c = '$mam_subscription_program_id' AND assigned_user_id = '$user_id' AND paymentstatus=2 AND deleted = 0 AND payment_type='dialog_subscription' ORDER BY date_entered DESC LIMIT 0,1");
            foreach ($al_payments as $al_payment) {
                $subscribed_mobile_json_decode = json_decode($al_payment->params, 0);
                if (isset($subscribed_mobile_json_decode[0])) {
                    $subscribed_mobile = $subscribed_mobile_json_decode[0];
                    //unsubscribe dialog
                    $mobile = $subscribed_mobile;
                    $request = new \Illuminate\Http\Request();
                    $request->replace(['msisdn' => $mobile]);
                    $dialogRepository = new DialogSubscriptionRepository();
                    $response = $dialogRepository->unsubscribe($request);
                    Storage::append('dialog_subscripiton.txt', date('Y-m-d H:i:s') . ' unsubscribe : ' . $mobile);
                    Storage::append('dialog_subscripiton.txt', print_r($response, 1));
                    //update status to unsubscribed for dialog
                    DB::statement("UPDATE al_payments SET field3='dialog_unsubscribed' WHERE id = '{$al_payment->id}'");
                    DB::statement("UPDATE today_tiles SET status='Inactive' WHERE related_id = '{$policy_user_master_id}'");
                    //update status to unsubscribed for dialog
                }
            }
        }
        if ($policy_user_master) {
            return success_response(0, []);
        } else {
            return error_response(1, 'error');
        }
    }

    static function delete_program_sequence() {
        $policy_sequence_master = ProgramSequenceMaster::where('program_id', '=', 8)->where('notification_header', '!=', '')->where('notification_text', '!=', '')->get();
        foreach ($policy_sequence_master as $psm) {
//            DB::statement("DELETE FROM `hal_notifications` WHERE name='".$psm->notification_header."' AND description='".$psm->notification_text."'");
            echo $psm->notification_header . '////' . $psm->notification_text;
            echo '<br>';
        }

        echo 'done';
        die;
        $program_id = 8;

        $policy_sequence_master = ProgramSequenceMaster::where('program_id', '=', $program_id)->get();
        foreach ($policy_sequence_master as $psm) {
            echo $psm->timeline_header;
            echo "<br>";
            $programs = ProgramDashboardMaster::where('heading', '=', $psm->timeline_header)
                    ->where('sub_heading', '=', $psm->sub_heading)
//                ->where('text', '=', $psm->timeline_text )
                    ->get();
            foreach ($programs as $p) {
                echo $p->id . 'deleted<br>';
//                $p->forceDelete();
            }
        }
    }

    static function delete_tips_from_sldf() {
        $policy_user_master = PolicyUserMaster::where('policy_id', '=', 'PROGRAM')->orderBy('id', 'desc')->get();
        $i = 0;
        foreach ($policy_user_master as $pum) {
            $program_groups = ProgramGroup::where('status', '=', 'Active')->where('user_id', '=', $pum->user_id)->where('policy_user_master_id', '=', $pum->id)->get();
            foreach ($program_groups as $program_group) {
                $user_id = $program_group->user_id;
                $program_group_id = $program_group->id;
//                ProgramDashboardMaster::where('user_id', $user_id)->where('program_group_id', $program_group_id)->delete();
                $program_group_mappings = ProgramGroupMapping::where('program_group_id', '=', $program_group->id)->get();
//                $program_group_mappings = ProgramGroupMapping::where('program_group_id', '=', $program_group->id)->where('program_id', '=', 7)->get(); 
                foreach ($program_group_mappings as $program_group_mapping) {
                    $program_id = 8;

                    $program_group_id = $program_group_mapping->program_group_id;
                    $policy_sequence_master = ProgramSequenceMaster::where('program_id', '=', $program_id)->get();
                    foreach ($policy_sequence_master as $psm) {
//                        echo $psm->timeline_header;
//                        echo "<br>";
                        $programs = ProgramDashboardMaster::where('heading', '=', $psm->timeline_header)
                                ->where('sub_heading', '=', $psm->sub_heading)->where('program_group_id', '=', $program_group_id)
                                ->get();
                        foreach ($programs as $p) {
                            $i++;
                            echo $p->id . 'deleted ' . $i . '<br>';
//                $p->forceDelete();
                        }
                    }
                }
            }
        }
    }

    public static function program_exe_pause($request) {
        $policy_user_master_id = $request->policy_user_master_id;

        $user_id = extract_user_id($request->header('authorization'));
        $user = User::find($user_id);
        if (!$user) {
            return error_response(1, 'error');
        }
        $actual_user_id = self::get_user_id_from_policy_user_master_id($policy_user_master_id);
        if ($user_id != $actual_user_id) {
            return error_response(2, 'You are not autorized for the action');
        }

        $policy_user_master = PolicyUserMaster::where('id', '=', $policy_user_master_id)->orderBy('id', 'desc')->first();

        if ($policy_user_master) {
            $program_group_id = '';
            $program_group = ProgramGroup::where('policy_user_master_id', '=', $policy_user_master->id)->first();
            if ($program_group) {
                $program_group_id = $program_group->id;
                $program_group->status = 'Inactive';
                $program_group->pause_date = date('Y-m-d');
                $program_group->save();
            }
            return success_response(0, $policy_user_master->id);
        }
        return error_response(1, 'error');
    }

    public static function program_exe_resume($request) {
        $policy_user_master_id = $request->policy_user_master_id;

        $user_id = extract_user_id($request->header('authorization'));
        $user = User::find($user_id);
        if (!$user) {
            return error_response(1, 'error');
        }
        $actual_user_id = self::get_user_id_from_policy_user_master_id($policy_user_master_id);
        if ($user_id != $actual_user_id) {
            return error_response(2, 'You are not autorized for the action');
        }

        $policy_user_master = PolicyUserMaster::where('id', '=', $policy_user_master_id)->orderBy('id', 'desc')->first();

        if ($policy_user_master) {
            $program_group_id = '';
            $program_group = ProgramGroup::where('policy_user_master_id', '=', $policy_user_master->id)->first();
            if ($program_group) {
                $now = time();
                $pause_date = $program_group->pause_date;
                $datediff = $now - strtotime($pause_date);
                $num_days = round($datediff / (60 * 60 * 24)) - 1;

                $program_group_id = $program_group->id;
                $program_group->status = 'Active';
                $program_group->pause_date = date('Y-m-d');
                $program_group->save();

                //update group mapping
                if ($num_days > 0) {
                    $program_group_mapping = ProgramGroupMapping::where('program_group_id', '=', $program_group_id)->get();
                    foreach ($program_group_mapping as $pgm) {
                        $pgm->offset = ($pgm->offset + $num_days);
                        $pgm->save();
                    }
                }
            }
            return success_response(0, $policy_user_master->id);
        }
        return error_response(1, 'error');
    }

    public static function program_exe_unsubscribe($request) {
        $policy_user_master_id = $request->policy_user_master_id;

//        $user_id = extract_user_id($request->header('authorization'));
        $user_id=$request->user_id;
        $user = User::find($user_id);
        if (!$user) {
            return error_response(1, 'error');
        }

        $actual_user_id = self::get_user_id_from_policy_user_master_id($policy_user_master_id);
        if ($user_id != $actual_user_id) {
            return error_response(2, 'You are not autorized for the action');
        }


        $policy_user_master = PolicyUserMaster::where('id', '=', $policy_user_master_id)->orderBy('id', 'desc')->first();

        if ($policy_user_master) {
            $program_group_id = '';
            $program_group = ProgramGroup::where('policy_user_master_id', '=', $policy_user_master->id)->first();
            if ($program_group) {
                $program_group_id = $program_group->id;
                $program_group->status = 'Unsubscribed';
                $program_group->pause_date = date('Y-m-d');
                $program_group->save();
                //update program dashboard master
                DB::statement("UPDATE `program_dashboard_master` SET deleted_at='" . date('Y-m-d H:i:s') . "'  WHERE user_id='" . $user_id . "' AND program_group_id='" . $program_group->id . "'");
            }
            //update payment table
            $sql = "SELECT pgm.program_id FROM program_group pg INNER JOIN program_group_mapping pgm ON pg.id=pgm.program_group_id WHERE pg.id='$program_group_id'";
            $program_group_mapping = DB::select($sql);
            foreach ($program_group_mapping as $pgm) {
                $old_programs = DB::select("SELECT id FROM mam_subscription_programs WHERE description='{$pgm->program_id}'");
                foreach ($old_programs as $old_program) {
                    $old_program_id = $old_program->id;
                    DB::statement("UPDATE al_payments SET field3='dialog_unsubscribed'  WHERE mam_subscription_programs_id_c = '$old_program_id' AND assigned_user_id = '$user_id' AND paymentstatus=2 AND deleted = 0 AND payment_type='dialog_subscription'");
                }
            }
            //hide the tile
            DB::statement("UPDATE `today_tiles` SET status='Inactive'  WHERE assigned_user_id='" . $user_id . "' AND related_id='" . $policy_user_master_id . "'");

            return $policy_user_master->id;
        }
        return '';
    }

    public static function program_exe_restart($request) {
        $policy_user_master_id = $request->policy_user_master_id;

        $user_id = extract_user_id($request->header('authorization'));
        $user = User::find($user_id);
        if (!$user) {
            return error_response(1, 'error');
        }
        $actual_user_id = self::get_user_id_from_policy_user_master_id($policy_user_master_id);
        if ($user_id != $actual_user_id) {
            return error_response(2, 'You are not autorized for the action');
        }


        $policy_user_master = PolicyUserMaster::where('id', '=', $policy_user_master_id)->orderBy('id', 'desc')->first();

        if ($policy_user_master) {
            $date = date('Y-m-d');
            $user_id = $user_id;
            $program_group = ProgramGroup::where('policy_user_master_id', '=', $policy_user_master->id)->first();
            if ($program_group) {
                DB::statement("UPDATE `program_dashboard_master` SET deleted_at='" . date('Y-m-d H:i:s') . "'  WHERE user_id='" . $user_id . "' AND program_group_id='" . $program_group->id . "'");

                $program_group_id = $program_group->id;
                $program_group->start_date = date('Y-m-d');
                $program_group->status = 'Active';
                $program_group->save();
                $program_group_mapping = ProgramGroupMapping::where('program_group_id', '=', $program_group_id)->get();
                foreach ($program_group_mapping as $pgm) {
                    $pgm->created_at = date('Y-m-d H:i:s');
                    $pgm->save();
                }
            }
            DB::statement("UPDATE `today_tiles` SET status='Active',visible_on='$date'  WHERE assigned_user_id='" . $user_id . "' AND related_id='" . $policy_user_master_id . "'");
            self::program_manual_post($date, $user_id, $policy_user_master->id);
            return success_response(0, $policy_user_master->id);
        }
        return error_response(1, 'error');
    }

    static function get_user_id_from_policy_user_master_id($policy_user_master_id) {
        $policy_user_master = PolicyUserMaster::where('id', '=', $policy_user_master_id)->first();
        if (isset($policy_user_master->user_id)) {
            return $policy_user_master->user_id;
        } else {
            return '';
        }
    }

    public function program_sequence_test($id) {
        $programs = Program::get();
        $sequence = [];
        $program_sequence = ProgramSequenceMaster::where('program_id', '=', $id)->orderBy('sequence', 'asc')->get();
        return View::make('janashakthi.program_test')
                        ->with('id', $id)
                        ->with('programs', $programs)
                        ->with('sequence', $program_sequence)
        ;
    }

    public function program_sequence_test_post($id, $request) {
        if (isset($request->id) && $request->pass == '1121') {
            $i = 0;
            foreach ($request->id as $s_id) {
                $program_sequence = ProgramSequenceMaster::where('id', '=', $s_id)->first();
                $program_sequence->timeline_header = $request->timeline_header[$i];
                $program_sequence->timeline_text = $request->timeline_text[$i];
                $program_sequence->notification_header = $request->notification_header[$i];
                $program_sequence->notification_text = $request->notification_text[$i];
                $program_sequence->sub_heading = $request->sub_heading[$i];
                $program_sequence->post_time = $request->post_time[$i];
                $program_sequence->relate_id = $request->relate_id[$i];
                $program_sequence->save();
                $i++;
            }
        }

        $programs = Program::get();
        $sequence = [];
        $program_sequence = ProgramSequenceMaster::where('program_id', '=', $id)->orderBy('sequence', 'asc')->get();
        return View::make('janashakthi.program_test')
                        ->with('id', $id)
                        ->with('programs', $programs)
                        ->with('sequence', $program_sequence)
        ;
    }

    public function program_sequence_test_native_post($native_post_id) {
        $data = DB::select("SELECT * FROM native_post_json WHERE id = '$native_post_id' LIMIT 0,1");
        return View::make('janashakthi.native_post_test')
                        ->with('np', $data)
        ;
    }

    public function program_sequence_test_quiz($quiz_id) {
        $qq = DB::select("SELECT * FROM questionnaire WHERE id = '$quiz_id'");
        $q = DB::select("SELECT * FROM questions WHERE questionnaire_id = '$quiz_id' ORDER BY `order`");
        return View::make('janashakthi.quiz_test')
                        ->with('qq', $qq)
                        ->with('q', $q)
        ;
    }

    public static function program_daily_cron_testing() {
        $today = isset($_REQUEST['date']) ? $_REQUEST['date'] : date('Y-m-d');

        $program_groups = ProgramGroup::where('status', '=', 'Active')->orderBy('user_id')->get();
        $policy_user_master_id = '';

        foreach ($program_groups as $program_group) {
            $user_id = $program_group->user_id;
            if ($user_id != '5d8fe13f-fe6b-d4dc-5777-5bb44326b397') {
//            if ($user_id != '7922c247-7e63-e3c7-afbf-59b764342cbd') { 
//                continue; 
            }
            echo "<hr><h4>{$program_group->policy_user_master_id}</h4>";
            $policy_user_master_id = $program_group->policy_user_master_id;
            $program_group_mappings = ProgramGroupMapping::where('program_group_id', '=', $program_group->id)->get();
            foreach ($program_group_mappings as $program_group_mapping) {
                $programs = Program::where('id', '=', $program_group_mapping->program_id)->get();
                foreach ($programs as $program) {
                    $subscription_date = date('Y-m-d', strtotime($program_group_mapping->created_at));
                    $offset = $program_group_mapping->offset;
                    $pogram_id = $program->id;
                    $program_send_dates = self::program_send_dates($program->id, $subscription_date, $offset);

                    if (isset($program_send_dates[$today])) {
                        foreach ($program_send_dates[$today] as $program_sequence) {
                            $user = User::where('id', '=', $user_id)->first();
                            $program = Program::where('id', '=', $pogram_id)->first();
                            echo $program->name . ' ' . $program_sequence->type . ' ' . $program_sequence->id . ' posted to ' . $user->user_name . ' ' . $program_sequence->notification_text . '<br>';
//                            self::timeline_post($user_id, $pogram_id, $program_sequence, 'yes', 'no', $today, $policy_user_master_id);
                        }
                    }
                }
            }
        }
    }

}
