<?php

namespace App\Libs;


use Illuminate\Http\Request;
use App\User;

class SugarApi
{
    protected $url;
    protected $token;
    protected $app_id;
    //0778976404

    public function __construct($service_url = null)
    {
            if(is_null($service_url)):
                $this->url = "https://livehappy.ayubo.life/custom/service/v4_1_custom/rest.php";
            else:
                $this->url = $service_url;
            endif;
            // $this->url = "https://devo.ayubo.life/custom/service/v4_1_custom/rest.php";
            $this->token = '51514b30736a7370716b5057623156396e37795a7468664264624c63645235525954346c724839454542303d';
    }

    public function setAppId($app_id)
    {
        $this->app_id = $app_id;
    }

    public function send($restData = array(), $method = 'getErrorMessage')
    {
        
        $headers = [
            'api_key: 7a39524643316c41445a316b64484c68427570396579577a34514d74425a477558744751524f4e685459413d',
            'api_id: '.$this->app_id 
        ];

        ob_start();
        $curl_request = curl_init();

        curl_setopt($curl_request, CURLOPT_URL, $this->url);
        curl_setopt($curl_request, CURLOPT_POST, 1);
        curl_setopt($curl_request, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
        curl_setopt($curl_request, CURLOPT_HEADER, 1);
        curl_setopt($curl_request, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl_request, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl_request, CURLOPT_FOLLOWLOCATION, 0);
        curl_setopt($curl_request, CURLOPT_HTTPHEADER, $headers);

        $post = $this->set($method, $restData);

        curl_setopt($curl_request, CURLOPT_POSTFIELDS, $post);
        $result = curl_exec($curl_request);
        curl_close($curl_request);

        $result = explode("\r\n\r\n", $result, 2);

        $response = json_decode($result[1]);    
        ob_end_flush();

        return $response;

    }

    public function set($method, $restData = array())
    {
        $restData['token'] = $this->token; 
        $rest_data = json_encode($restData);

        $data = [
            "method" => $method,
            "input_type" => "JSON",
            "response_type" => "JSON",
            "rest_data" => $rest_data
        ];

        return $data;
    }


}
